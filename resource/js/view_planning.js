jQuery(function($){

	/*
	* Au click sur une case du planning -> permet de voir le détail, de supprimer une personne ou d'en rajouter une autre
	*/
	$('body').on('dblclick', '.boxPlanning', function(event){
		$('.popin').hide();
		$(this).find('.popin').show();
		event.stopPropagation();
	});

	/*
	* Permet de supprimer l'horaire d'une personne
	*/
	$('body').on('click', '.iconClose', function(event){
		var idTimeslot = $(this).attr('id').replace('delete_', '');

		$.ajax({
			type: "POST",
			url: "planning/removeTimeSlot",
			data : {"idTimeslot" : idTimeslot},
			success: function(result){
	        	var status = result.status;
                var message = result.message;
                Toast.show(message,status);

                setTimeout(function(){ document.location.reload(); }, 1000);
	    	},
	    	error: function(response) {
                console.log('error');
            }
		});

		event.stopPropagation();
	});

	/*
	* Permet d'ajouter une personne sur une horaire
	*/
	$('body').on('click', '.iconAdd', function(event){
		var date = $(this).parent().parent().find('.dateTimeslot').html();
		var hours = $(this).parent().parent().find('.hoursTimeslot').html();

		$('#dateTimeslot').html(date);
		$('#hoursTimeslot').html(hours);

		$('#addTimeslot').fadeIn();

		event.stopPropagation();
	});

	/*
	* Validation de l'ajout du timeslot
	*/
	$('body').on('click', '#validateAddTimeslot', function(){
		var date = $('#dateTimeslot').html();
		var hours = $('#hoursTimeslot').html();
		var id_monitor = $('#monitorTimeslot').val();
		var id_student = $('#studentTimeslot').val();

		if(date == '' || hours == '' || id_monitor == '' || id_student == ''){
			if(id_monitor == ''){
				$('.monitorTimeslotBlock > label').css('color', '#EB5757');
				$('.monitorTimeslotBlock > select').css('background-color', '#FFAEAE');
			}
			else if(id_student == ''){
				$('.studentTimeslotBlock > label').css('color', '#EB5757');
				$('.studentTimeslotBlock > select').css('background-color', '#FFAEAE');
			}
			else{
				$('#dateTimeslot').css('color', '#EB5757');
				$('#hoursTimeslot').css('color', '#EB5757');
			}
		}
		else{
			$.ajax({
				type: "POST",
				url: "planning/addtimeslot",
				data : {"date" : date, "hours" : hours, "id_monitor" : id_monitor, "id_student" : id_student},
				success: function(result){
		        	var status = result.status;
	                var message = result.message;
	                Toast.show(message,status);

	                setTimeout(function(){ document.location.reload(); }, 1000);
		    	},
		    	error: function(response) {
	                console.log('error');
	            }
			});
		}

		return false;
	});

	/*
	* Remettre le design de base au click sur le select
	*/
	$('body').on('click', '#monitorTimeslot', function(){
		$(this).prev().css('color', '#000');
		$(this).css('background-color', '#FFF');
	});
	$('body').on('click', '#studentTimeslot', function(){
		$(this).prev().css('color', '#000');
		$(this).css('background-color', '#FFF');
	});

	/*
	* Annuler de l'ajout du timeslot
	*/
	$('body').on('click', '#cancelAddTimeslot', function(){
		$('#addTimeslot').fadeOut();
		return false;
	});

	/*
	* Au click sur l'extérieur -> ferme la popin
	*/
	$('body').on('click', '#contentBack', function(){
		$('.popin').hide();
	});

	/*
	* Changement de semaine
	*/
	$('body').on('click', '.weekChange', function(){
		var week = ($(this).attr('id')).replace("week_", "");

		if($('.ownerPlanning').attr('id') == undefined){
			var idUser = null;
		}
		else{
			var idUser = ($('.ownerPlanning').attr('id')).replace("user_", "");
		}

		var uri = document.location.href;

		if(uri.indexOf("planning/") == '-1'){
			var url = "planning/navigation";
		}
		else{
			var url = "navigation";
		}

		$.ajax({
			type: "POST",
			url: url,
			data : {"week" : week, 'id' : idUser},
			success: function(result){
			   	$('.dataPlanning').children().remove();
	        	$('.dataPlanning').append(result);

	        	var weekCurrentlySplit = week.split("_");
	        	var weekCurrently = weekCurrentlySplit[0];
	        	var yearCurrently = weekCurrentlySplit[1];

	        	if(weekCurrently.length == 1){
	        		$('.weekCurrently').html("Semaine : 0" + weekCurrently);
	        	}
	        	else{
	        		$('.weekCurrently').html("Semaine : " + weekCurrently);
	        	}

	        	var idWeekBack = $('.weekBack').attr('id');
	        	var idWeekAfter = $('.weekAfter').attr('id');

	        	//La prochaine semaine -1 sera la dernière semaine de l'année d'avant
	        	if(weekCurrently == '1'){
	        		document.getElementById(idWeekBack).id = "week_52_" + (parseInt(yearCurrently)-1);
	        		document.getElementById(idWeekAfter).id = "week_" + (parseInt(weekCurrently)+1) + "_" + yearCurrently;
	        	}
	        	else if(weekCurrently == '52'){
	        		document.getElementById(idWeekBack).id = "week_" + (parseInt(weekCurrently)-1) + "_" + yearCurrently;
	        		document.getElementById(idWeekAfter).id = "week_1_" + (parseInt(yearCurrently)+1);
	        	}
	        	else{
	        		document.getElementById(idWeekBack).id = "week_" + (parseInt(weekCurrently)-1) + "_" + yearCurrently;
	        		document.getElementById(idWeekAfter).id = "week_" + (parseInt(weekCurrently)+1) + "_" + yearCurrently;
	        	}

	    	},
            error : function(response) {
                console.log('error');
            }
		});

	});


});