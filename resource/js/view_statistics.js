$(document).ready(function(){

	/*
	* Récupèration des données pour remplir les charts
	*/
	$.ajax({
		type: "GET",
		url: "statistics/chart",
		success: function(result){
	       	chartPie("canvasAll", result['all']['labels'], result['all']['data'], result['all']['backgroundcolor']);
	       	chartPie("canvasYear", result['year']['labels'], result['year']['data'], result['year']['backgroundcolor']);
	       	chartPie("canvasMonth", result['month']['labels'], result['month']['data'], result['month']['backgroundcolor']);
	       	chartPie("canvasToday", result['today']['labels'], result['today']['data'], result['today']['backgroundcolor']);
	    },
	    error: function(response) {
	        console.log('error');
	    }
	});

	/*
	* Afficher le TOUT de chaque card
	*/
	$('.allTab').addClass('activate');
	// $('.allTabcontent').show();
});


jQuery(function($){

	/*
	* Navigation dans les onglets des cards sur les statistiques
	*/
	$('body').on('click', '.tablinks', function(){
		var idParent = $(this).parent().parent().attr('id');

		$("#" + idParent + " .tablinks").removeClass('activate');
		$(this).addClass('activate');

		$("#" + idParent + " .tabcontent").hide();
		var className = $(this)[0].classList[1];
		$("#" + idParent +  ' .' + className + "Content").show();
	});

});

function chartPie(id, label, data, backgroundColor){
	var ctx = $("#"+id).get(0).getContext("2d");
 
	var data = {
	    labels: label,
	    datasets: [
	        {
	            data: data,
	            backgroundColor: backgroundColor,
	            label: 'My dataset'
	        }]
	};

	var myPieChart = new Chart(ctx,{
	    type: 'pie',
	    data: data,
	    options: {responsive: true}
	});
}