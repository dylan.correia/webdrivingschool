jQuery(function($){

	/*
	* Affichage de la première page par defaut
	*/
	$('#pageUsers1').fadeIn();

	/*
	* Affichage de la page souhaité
	*/
	$('body').on('click', '.buttonPageUsers', function(){
		var idNumber = $(this).attr('id').replace('buttonPageUsers', '');
		$('.pageUsers').hide();
		$('#pageUsers' + idNumber).fadeIn();
	});

	/*
	* Filtrage sur la recherche
	*/

	$('body').on('click', '#wen', function(){
		var value = $('#search').val();
		$('.dataUsers').html('');

		$.ajax({
			type: "POST",
			url: "users/search",
			data : {"search" : value},
			success: function(result){
	        	if(result.length == 0){
					$('.dataUsers').html('<div class="msgError"><p>Aucun utilisateur ne correpond à votre recherche !</p></div>');
				}
				else{
					$('.dataUsers').html(rewriteUsersData(result));
	        		$('#pageUsers1').fadeIn();
				}
	    	},
	    	error: function(response) {
	            console.log('error');
	        }
		});
	});



	/*
	* Udpate d'un utilisateur
	*/
	$('body').on('click', '#updateUsers', function(){
		if(!dataCheck()){
			var data = dataForm();

			files = {};

			if($('input[type=file]').val() != ''){
				if($('input[type=file]').data('base64')){
		            var image  = {
		                base64 :"",
		                type:""
		            };
		            image.base64 =  $('[name="'+$('input[type=file]').attr('name')+'"]').data('base64');
		            image.type = $('[name="'+$('input[type=file]').attr('name')+'"]').data('type');
		            files[$('input[type=file]').attr('name')] = image;
		        }
			}

			$.ajax({
				type: "PUT",
				url: $('form').attr('action'),
				data : {form : data, files : files},
				success: function(response){
					console.log(response);
                	var status = response.status;
                	var message = response.message;
                	Toast.show(message,status);
		    	},
		    	error: function(response) {
	                console.log('error');
	            }
			});

			return false;
		}
	});

	/*
	* Modification de l'image de profil
	*/
	$('body').on('change', 'input[type=file]', function(event){
		if($(this).val() != ''){
			var fileName =  event.target.name;
	        var fileType = event.target.files[0].type;

	        if(![ "image/png", "image/gif ", "image/jpeg","image/svg+xml"].includes(fileType)){
	            Toast.show('Le format de l\'image est incorrect','error');
	            $(this).val("");
	        }
	        else{
	        	var oFileReader = new FileReader();
		        oFileReader.readAsDataURL(event.target.files[0]);

		        oFileReader.onload = function() {
		            $('[name="'+fileName+'"]').data('base64',oFileReader.result.replace(/^data:.*?;base64,/, ""));
		            $('[name="'+fileName+'"]').data('type',fileType);
		        }
	        }
		}
	});


	/*
	* Annuler
	*/
	$('body').on('click', '#buttonCancel', function(){
		document.location.href = $('#urlUser').val();
		return false;
	});

});

function dataCheck(){
	var error = false;
	$('form [name]').each(function() {
		if(($(this)[0].value == "" && $(this)[0].required == true) && ($(this)[0].name != "pwd" && $(this)[0].name != "pwdConfirm")){
			error = true;
		}
	});
	return error;
}

function dataForm(){
	var dataForm = {};
	$('form [name]').each(function() {
		dataForm[$(this)[0].name] = $(this)[0].value;
	});
	return dataForm;
}

function rewriteUsersData(array){
	var dirName = $('#dirName').val();
	var dirImages = $('#dirImages').val();

	var numberPerPage = 9;
	var buttonForPage = [];

	var html = '';

	for (var i = 0; i < array.length; i++) {
		if(i == 0){
			buttonForPage[i/numberPerPage] = (i/numberPerPage)+1;
			html += '<div id="pageUsers' + ((i/numberPerPage)+1) + '" class="pageUsers">';
		}
		html += '<div class="row dataUsers">';
			html += '<div class="col-sm-3">';
				html += '<div class="pictureUsers">';
					if(array[i]['picture'] != "" && imageExists(dirImages + 'picture_users/' + array[i]['picture'])){
						html += '<img src="' + dirImages + 'picture_users/' + array[i]['picture'] + '">';
					}
					else{
						html += '<img src="' + dirImages + 'picture_users/unknown.png">';
					}
				html += '</div>';
			html += '</div>';

			html += '<div class="col-sm-3">';
				html += '<p>' + array[i]['lastname'] + '</p>';
			html += '</div>';

			html += '<div class="col-sm-3">';
				html += '<p>' + array[i]['firstname'] + '</p>';
			html += '</div>';



			html += '<div class="col-sm-3">';
				html += '<div class="actionUsers">';
					html += '<a href="'+ dirName +'progress/'+ array[i]['id'] +'"><i class="showPlanning icon ion-ios-bookmarks-outline"></i></a>';
					html += '<a href="'+ dirName +'progress/'+ array[i]['id']+'/add' +'"><i class="modifyUser icon ion-ios-plus-outline"></i></a>';
				html += '</div>';
			html += '</div>';
		html += '</div>';

		if(i != 0 && i != (array.length)-1 && i % numberPerPage == 0){
			buttonForPage[i/numberPerPage] = (i/numberPerPage)+1;
			numberPerPage = 10;
			html += '</div><div id="pageUsers' + ((i/numberPerPage)+1) + '" class="pageUsers">';
		}
		else if(i == (array.length)-1){
			html += '</div>';
		}
	}

	html += '<div id="paginationUsers">';
		for (var i = 0; i < buttonForPage.length; i++) {
			html += '<button id="buttonPageUsers' + buttonForPage[i] + '" class="buttonPageUsers">' + buttonForPage[i] + '</button>';
		}
	html += '</div>';

	return html;
}


function imageExists(image_url){
    var http = new XMLHttpRequest();

    http.open('HEAD', image_url, false);
    http.send();

    return http.status != 404;
}