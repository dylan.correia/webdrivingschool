<?php
session_start();

function myAutoloader($class) {

    if(file_exists("core/".$class.".php"))
        include "core/".$class.".php";

	$class = $class.".class.php";
	if(file_exists("core/".$class)){
		include "core/".$class;
	} else if(file_exists("models/".$class)){
		include "models/".$class;
	}
}
spl_autoload_register('myAutoloader');

if(!file_exists('conf.inc.php')){


	$installation  =  New Installer();

	if(!empty($_POST)){

		$installation->connectDatabase();
	} else {

		$installation->init();
	}
	die();
}

require "conf.inc.php";
require "routes.php";

if(isset($_SERVER['REMOTE_ADDR'])){
	new Visitor($_SERVER['REMOTE_ADDR']);
}

$request = new Request( $_SERVER['REQUEST_URI'],$_SERVER['REQUEST_METHOD']);
Router::run($request);