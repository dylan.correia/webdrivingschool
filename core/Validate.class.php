<?php
class Validate{


	public static function checkForm($config, $data){
		$errorsMsg = [];

		foreach ($config["input"] as $name => $attributs) {

			if((isset($data[$name]) && $data[$name] != "") || isset($attributs["required"])){

				if(isset($attributs["confirm"]) && $data[$name] != $data[$attributs["confirm"]]){
					$errorsMsg[]= $attributs['label'] ." ne correspond pas à ".$config["input"][$attributs["confirm"]]["label"];
				}else if( !isset($attributs["confirm"]) ){

					if($attributs["type"]=="email" && !self::checkEmail($data[$name]) ){
						$errorsMsg[]= "Format de l'email incorrect";
					}
					if ($attributs["type"]=="password" && !self::checkPwd($data[$name]) ){
						$errorsMsg[]= "Mot de passe incorrect(Maj, Min, Chiffre, entre 6 et 32)";
					}
					if ($attributs["type"]=="number" && !self::checkNumber($data[$name]) ){
						$errorsMsg[]= $name ." n'est pas correct";
					}

				}

				if(isset($attributs["maxString"]) && !self::maxString($data[$name], $attributs["maxString"])){
						$errorsMsg[]= 'Le champs '.$attributs['label'] ." doit faire moins de ".$attributs["maxString"]." caractères" ;
				}

				if(isset($attributs["minString"]) && !self::minString($data[$name], $attributs["minString"])){
						$errorsMsg[]= 'Le champs '.$attributs['label'] ." doit faire plus de ".$attributs["minString"]." caractères" ;
				}

				if(isset($attributs["maxNum"]) && !self::maxNum($data[$name], $attributs["maxNum"])){
						$errorsMsg[]= 'Le champs '.$attributs['label'] ." doit être inférieur à ".$attributs["maxNum"];
				}

				if(isset($attributs["minNum"]) && !self::minNum($data[$name], $attributs["minNum"])){
						$errorsMsg[]= 'Le champs '.$attributs['label'] ." doit être supérieur à ".$attributs["minNum"];
				}
				
			}

		}
		return $errorsMsg;
	}

	public static function maxString($string, $length){
		return strlen(trim($string))<=$length;
	}

	public static function minString($string, $length){
		return strlen(trim($string))>=$length;
	}

	public function maxNum($num, $length){
		return strlen($num)<=$length;
	}

	public function minNum($num, $length){
		return strlen($num)>=$length;
	}

	public static function checkEmail($email){
		return filter_var($email, FILTER_VALIDATE_EMAIL);
	}

	public static function checkPwd($pwd){
		return strlen($pwd)>=6 && strlen($pwd)<=32 && 
		preg_match("/[a-z]/", $pwd) && 
		preg_match("/[A-Z]/", $pwd) && 
		preg_match("/[0-9]/", $pwd);
	}

	public static function checkNumber($number){
		return is_numeric(trim($number));
	}

}








