<?php

/**
 *
 */
class Auth
{
    /**
     * Initialise un session avec les informations de l'utilisateur
     *
     * @param  string $token
     * @return void
     */

    public static function init($token)
    {
        $_SESSION['token'] = $token;
        $bd = new BaseSql();
        $_SESSION['AUTH'] = $bd->query(
            'SELECT user.id,user.firstname,user.lastname,user.picture,role.name as roleName,user.id_role FROM user,role WHERE user.token = :token AND user.id_role = role.id ',
            ['token' => $token])[0];

        $permissions = $bd->query('SELECT id_permission FROM permission, role_permission , role WHERE role_permission.id_permission = permission.id AND id_role = role.id AND role.id = :idRole', ['idRole' => $_SESSION['AUTH']['id_role']]);

        $query = [];

        //On charge les permissions en session
        foreach ($permissions as $permission) {
            $_SESSION['AUTH']['PERMISSIONS'][] = $permission['id_permission'];
            $access[$permission['id_permission']] = $permission['id_permission'];
            $query[] = 'id_permission = :' . $permission['id_permission'] . '';
        }

        // On charge l'accessibilité des controller/action en session
        if (!empty($query)) {
            $_SESSION['AUTH']['ACCESS'] = $bd->query('SELECT controller,method FROM ability WHERE ' . implode(' OR ', $query), $access);
        }
        $_SESSION['AUTH']['ACCESS'][] = ["controller" => "User", "method" => "profile"];

    }

    /**
     * Retourne l'identifiant de l'utilisateur connecté
     *
     * @return int
     */

    public static function id()
    {
        return $_SESSION['AUTH']['id'];
    }

    /**
     * Retourne le nom de l'image de profile
     *
     * @return string
     */
    public static function picture()
    {
        return $_SESSION['AUTH']['picture'];
    }

    /**
     * Retourne le prénom de l'utilisateur connecté
     *
     * @return string
     */
    public static function firstname()
    {
        return $_SESSION['AUTH']['firstname'];
    }

    /**
     * Retourne le nom de l'utilisateur connecté
     *
     * @return string
     */
    public static function lastname()
    {
        return $_SESSION['AUTH']['lastname'];
    }

    /**
     * Retourne le rôle de l'utilisateur connecté
     *
     * @return string
     */
    public static function role()
    {
        return $_SESSION['AUTH']['roleName'];
    }

    /**
     * Retourne les permissions de l'utilisateur connecté
     *
     * @return array string
     */
    public static function hasPermission($permission)
    {
        return in_array($permission, $_SESSION['AUTH']['PERMISSIONS']);
    }

    /**
     * Supprime la session
     *
     * @return void
     */
    public static function destroy()
    {
        unset($_SESSION['token']); //suppression de la variable de session token
        unset($_SESSION['AUTH']); //suppression de la variable de session AUTH
        session_destroy(); //destruction de la session
    }


    /**
     * Vérifie si l'utilisateur connecté à les droits d'accès sur une route
     *
     * @param  string $controller
     * @param  string $method
     * @return boolean
     */
    public static function checkAccess($controller, $method)
    {
        if ($controller == 'IndexController'
            || ($controller == 'UserController' && $method == 'loginAction')
            || ($controller == 'UserController' && $method == 'logoutAction')
            || ($controller == 'UserController' && $method == 'resetpwdAction')
            || ($controller == 'UserController' && $method == 'lostpwdAction')) {
            return true;
        }
        $allAccess = isset($_SESSION['AUTH']['ACCESS']) ? $_SESSION['AUTH']['ACCESS'] : null;
        $tokenCheck = self::verifytoken();
        if (!empty($allAccess)) {
            foreach ($allAccess as $access) {
                if ($access['controller'] . 'Controller' == $controller && $access['method'] . 'Action' == $method && $tokenCheck)
                    return true;
            }
        }
        return false;
    }

    /**
     * @return bool
     */
    public static function verifytoken()
    {
        //si le token est attribué à la session
        if (isset($_SESSION['token'])) {
            $bd = new BaseSql();
            $query = 'SELECT * FROM user WHERE token = :token';

            //si le resultat de la requete est positif, donc si le token existe en bdd
            $result = $bd->query($query, ['token' => $_SESSION['token']], 'User');
            if ($result) {
                $user = $result[0];
                $newtoken = ($user->generateToken());
                $user->setToken($newtoken);
                $user->save();
                $_SESSION['token'] = $newtoken;
                return true;

        } else {
            return false;
        }
    } else {
return false;
}
}


}