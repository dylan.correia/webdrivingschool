<?php

class BaseSql {
	private $table;
	private $pdo;
	private $columns;

	public function __construct(){
		$this->table = strtolower(get_called_class());
		$this->pdo = self::getPdo();
	}

	 /**
     * Retire les colonnes inutiles
     *
     * @return void
     */
	public function setColumns(){
		$columnsExcluded = get_class_vars(get_class());
		$objVars = $this->camelCaseTosnake_case(get_object_vars($this));
		$this->columns = array_diff_key($objVars, $columnsExcluded);
	}

	/**
     * Récupérer un objet pdo
     *
     * @return pdo object
     */
	private static function getPdo() {
    	try {
			$pdo = new PDO("mysql:host=".DBHOST.";dbname=".DBNAME,DBUSER,DBPWD);
		} catch(Exception $e){
			die("Erreur SQL".$e->getMessage()."\n");
		}
		return $pdo;
	}

	/**
     * Créer ou met à jour un objet dans la base
     *
     * @return void
     */

	public function save(){
		$this->setColumns();

		if($this->id){
			//Update
			foreach ($this->columns as $key => $value) {
				if(isset($value)){
					$sqlSet[] =  $this->table.'.'.$key."=:".$key;
				}
				else{
					unset($this->columns[$key]);
				}
			}
			$query = $this->pdo->prepare("UPDATE ".$this->table." SET ".implode(",", $sqlSet)." WHERE ".$this->table.".id=:id");

			$query->execute($this->columns);

		} else {
			//Insert
			unset($this->columns["id"]);
			$keys  =  [];
			foreach (array_keys($this->columns) as $value) {
				$keys []= $this->table.'.'.$value;
			}
			$query = $this->pdo->prepare("INSERT INTO " . $this->table . " (".
            implode(",", $keys)
            .") VALUES (:".
            implode(",:", array_keys($this->columns))
            .")");

            $query->execute($this->columns);
		}
	}

	/**
     * Liste les objets d'une classe
     *
     * @param  boolean  $hidden
     * @param  string  $orderBy
     * @return Array of object
     */

	public static function all($hidden = null,$orderBy = null) {
        $pdo = self::getPdo();

        $queryRequest =  [];
        if($hidden == null){
        	$queryRequest[] = "SELECT * FROM " . strtolower(get_called_class());
        } else {
            $queryRequest[] = "SELECT * FROM " . strtolower(get_called_class()). " WHERE hidden='0'";
        }
        if($orderBy){
            $queryRequest[] = " ORDER BY " . $orderBy;
        }

        $query = $pdo->query(implode(' ',$queryRequest));
        $objects = [];

        while($t = $query->fetchObject(get_called_class())) {
            $objects[]=$t;
        }
        
        $query->closeCursor();
        foreach ($objects as $object){
            self::snake_caseToCamelCase($object);
        }

        return $objects;
    }


    /**
     * Récupère un objet grâce son id
     *
     * @param  int  $id
     * @param  boolean  $class
     * @return object
     */

	public static function findById($id,$class =true){
		$pdo = self::getPdo();

        $query = $pdo->prepare("SELECT * FROM " . strtolower(get_called_class()).' WHERE id = :id');
        $query->execute(['id' => $id]);

        if($class){
            $object = $query->fetchObject(get_called_class());

            if($object) {
                self::snake_caseToCamelCase($object);
            }
        } else {
            $object = $query->fetch();
        }

        return $object;
	}

	/**
     * Transforme les attributs qui sont en snake_case en CamelCase
     *
     * @param  object  $object
     * @return void
     */

	private static function snake_caseToCamelCase(&$object){
        foreach ($object as $key => $value) {
            if(preg_match('#_#',$key )){
                $camelKey = lcfirst(implode('', array_map('ucfirst', explode('_', $key))));
                $camelKey ='set'.ucfirst($camelKey);
                $object->$camelKey($object->$key);
                unset($object->$key);
            }
        }
	}

	/**
     * Supprime un objet dans la base
     *
     * @param  boolean  $fakeDelete
     * @return void
     */

	public function delete($fakeDelete = false){
	    if($fakeDelete) {
            $query = $this->pdo->prepare("UPDATE ".$this->table." SET ".$this->table.".deleted = 1 WHERE ".$this->table.".id = :id");
        }else {
            $query = $this->pdo->prepare("DELETE FROM " . strtolower(get_called_class()).' WHERE id = :id');
        }

        $query->execute(['id' => $this->id]);
	}

	/**
     * Cache un objet dans la base
     *
     * @return void
     */

	public function hidden(){
		$query = $this->pdo->prepare("UPDATE " . strtolower(get_called_class()) . " SET hidden='1' WHERE id = :id");
        $query->execute(['id' => $this->id]);
	}

	/**
     * Transforme les attributs qui sont en CamelCase en snake_case
     *
     * @param  object  $object
     * @return void
     */

	private function camelCaseTosnake_case($object){
		$result = [];
		foreach ($object as $key => $value) {
			$newKey = preg_replace('/(?<=\\w)(?=[A-Z])/',"_$1", $key);
	    	$newKey = strtolower($newKey);
			$result[$newKey] = $value;
		}

		return $result;
	}


	/**
     * Requête avec la base de donnée
     *
     * @param  string  $queryString
     * @param  array  $queryParams
     * @param  string  $class
     * @return Array of object
     */

	public function query($queryString ,$queryParams,$class =null) {
		$query = $this->pdo->prepare($queryString);
        $query->execute($queryParams);

        $result =[];
        if(!is_null($class)){
        	while($t = $query->fetchObject(ucfirst($class))) {
            	$result[]=$t;
        	}
        } else{
	        while($row = $query->fetch())
			{
				$result[] = $row;
			}
        }

		return $result;
	}
}