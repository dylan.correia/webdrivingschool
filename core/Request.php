<?php

/**
* 
*/


class Request
{
	private $uri;
	private $controller;
	private $action;
	private $methodRequest;
	private $regexRoute;
	private $regexRoutes = [];
	private $routeAction;

	function __construct($uri,$methodRequest)
	{
		$this->routes = Router::allRoutes();
		$this->regexRoutes = RouterParser::getRegexRoutes();
		$this->methodRequest = $methodRequest;
		$this->uri = $uri;
		$this->conform();
	}

	/**
     * Vérifie que la requete est conforme
     *
     * @return void
     */
	private function conform() {
		// on format l'uri
		$this->uri = explode("?",$this->uri);
		if(DIRNAME !="/"){
            $this->uri = str_ireplace((DIRNAME), "",urldecode($this->uri[0]));
            $this->uri = '/'.$this->uri;
		} else {
            $this->uri = urldecode($this->uri[0]);
        }
		$this->uri = strtolower($this->uri);


		$notFound = true;

		// on vérifie si la route éxiste
		foreach ($this->regexRoutes as $regex => $route) {

			if(preg_match($regex,$this->uri)){
				if(!array_key_exists($this->methodRequest,$route)) {
					die("La route $this->uri suivant la méthode $this->methodRequest n'est pas définie.");
				}
				$urlParameters = $route[$this->methodRequest];
				$notFound = false;
				$this->routeAction = $this->routes[$urlParameters][$this->methodRequest];

				$actionController = explode("@",$this->routeAction);
				$this->controller = $actionController[0].'Controller'; 
				$this->action = $actionController[1].'Action';
				$this->regexRoute = $regex;

				break;
			}
		}

		// Si la page est non trouvé ou que les droits ne correspondent pas , on affiche la page d'erreur
		if($notFound || !Auth::checkAccess($this->controller ,$this->action )) {
			$this->methodRequest = 'GET';
			$this->uri = DIRNAME.'error';
			return $this->conform();
		}

		$this->matchMethod();
	}

	/**
     * Vérifie que la méthode et le controller fonctionne bien
     *
     * @return void
     */
	private function matchMethod(){
			
		if( file_exists("controllers/".$this->controller.".class.php") ){
			include("controllers/".$this->controller.".class.php");
			
			if(class_exists($this->controller)){
				$c  = $this->controller;
				if(!method_exists(new $c(), $this->action)){
					die("L'action ".$this->action." n'existe pas");
				}
			}else{
				die("La classe ".$this->controller." n'existe pas");
			}
		}else{
			die("Le controller ".$this->controller." n'existe pas");
		}

	}

	/**
     * Retourne le nom du controller
     *
     * @return string $this->controller
     */
	public function getController(){
		return $this->controller;
	}

	/**
     * Retourne le nom de l'action
     *
     * @return string $this->action
     */
	public function getAction(){
		return $this->action;
	}

	/**
     * Retourne l'expression régulière de la route correspondante
     *
     * @return string $this->regexRoute
     */
	public function getRegexRoute(){
		return $this->regexRoute;
	}

	/**
     * Retourne la méthode HTTP
     *
     * @return string $this->uri
     */
	public function getUri(){
		return $this->uri;
	}

	/**
     * Retourne la méthode HTTP
     *
     * @return string $this->methodRequest
     */
	public function getMethodRequest(){
		return $this->methodRequest;
	}
}