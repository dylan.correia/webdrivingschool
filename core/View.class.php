<?php
class View{

	private $view;
	private $t;
	private $data = [];


	public function __construct($view="default", $template="front"){
		$this->view = $view.".view.php";
		$this->template = $template.".tpl.php";

		if( !file_exists("views/".$this->view) ){
			die("La vue :".$this->view." n'existe pas");
		}
		if( !file_exists("views/templates/".$this->template) ){
			die("Le template :".$this->template." n'existe pas");
		}			

	}

	public function addModal($modal,$config, $errors=[] ,$values = null){
		include "views/modals/".$modal.".mdl.php";
	}

	public function __destruct(){
		$this->settingConfig();

		extract($this->data); // crée autant de variable qu'il y'a de clé dans le tableau
		include "views/templates/".$this->template;
	}

	public function assign($key, $value)
    {
        $this->data[$key] = $value;
    }

    private function settingConfig(){
	    if(defined('DBHOST')){

            $settings = Setting::all();
            foreach ($settings as $setting){
                $this->assign($setting->getName(),$setting->getValue());
            }
        }
    }
}