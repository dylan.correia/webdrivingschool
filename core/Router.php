<?php

/**
* 
*/

include "RouterParser.php";

class Router extends RouterParser
{
	private static $routes;
	
	//Appelle la méthode et le controller souhaité
	public static function run($r){
		
		$paramsIndex = parent::getParamsRoutes(); //Récupère la position des paramètres dans la route
		$regex = $r->getRegexRoute();
		$params=self::getParams($r->getUri(),$regex,$paramsIndex,$r->getMethodRequest()); // Récupère les paramètres dans la route
		$controller = $r->getController();
		$action = $r->getAction();
		//Instancie un controller et appelle la méthode
		self::call($controller,$action,$params);
	}

	//Retourne toutes les routes
	public static function allRoutes(){
		return self::$routes;
	}

	//Enregistre une route de type GET
	public static function get($url,$action) {
		self::$routes[$url]['GET'] = $action;
		parent::buildRegex($url,'GET');
	}

	//Enregistre une route de type POST
	public static function post($url,$action) {
		self::$routes[$url]['POST'] = $action;
		parent::buildRegex($url,'POST');
	}

	//Enregistre une route de type PUT
	public static function put($url,$action) {
		parent::buildRegex($url,'PUT');
		self::$routes[$url]['PUT'] = $action;
	}

	//Enregistre une route de type DELETE
	public static function delete($url,$action) {
		parent::buildRegex($url,'GET');
		self::$routes[$url]['GET'] = $action;
	}

	// Récupère les paramètres dans la route
	public static function getParams ($url,$regex,$params,$methodRequest){

		$params = $params[$regex][$methodRequest]; //récupère la postion des paramètres de la route souhaité

		//Remplace les paramètres dans le tableau
		if(preg_match($regex,$url)){
			$url =substr($url,1); 
			$urlExploded = explode('/', $url);
			foreach ($params as $key => $value) {
				$params[$key] = $urlExploded[$value];
			}
		}

		//Récupère les autres paramètres en fonction du type de méthode
		if($methodRequest == 'GET'){
			$params = array_merge($params,$_GET);
		}

		else if($methodRequest == 'POST'){
			if(empty($_POST)){
	 			$rest_json = file_get_contents("php://input");
	 			$_POST = json_decode($rest_json,TRUE);
	 		}
			$params = array_merge($params,$_POST);
		}
		else if($methodRequest == 'PUT'){
	 		parse_str(file_get_contents('php://input'), $_PUT);
			$params = array_merge($params,$_PUT);
		}

		return $params;
	}

	public static function call ($controller,$action,$params = null){
        //Instancie un controller et appelle la méthode
        $objC = new $controller();
        $objC->$action($params);
    }
}