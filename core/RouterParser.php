<?php

/**
* 
*/
class RouterParser
{
	private static $regexRoutes = [];
	private static $replacement = [
		'an' => '([a-zA-Z]|[0-9])+', // caractère alphabétique ou numérique
		'n'  => '[0-9]+', // caractère numérique
		'a'  => '[a-zA-Z]+', // caractère alphabétique
		'o'  => '.+'  // N'importe quel caractère
	];
	private static $paramsRoutes = [];

	public function buildRegex($url,$method) {
		$uri = $url;
		$url =substr($url,1); // On enlève le slash du début
		$url = explode('/', $url);
		$pregUrl = '/^';
		$params = [];
		$i=0;
		foreach ($url as $urlFormat) {
			//On regarde si la route comporte un paramètre
			if(preg_match('/{[a-zA-Z]+:(n|an|a|o)}/', $urlFormat)) {  
		 		$urlFormat = trim($urlFormat,'{'); 
		 		$urlFormat = trim($urlFormat,'}');
		 		$urlFormat = explode(':', $urlFormat); // On séprare le paramètre de son type
		 		$params[$urlFormat[0]] = $i; // On dique la position du paramètre dans l'URL
		 		$pregUrl .= "\/".self::$replacement[$urlFormat[1]]; 

		 	}	else {

				$pregUrl .= "\/".$urlFormat; 
		 	}
		 	$i++;
		}

		$pregUrl .= '$/';
		self::$regexRoutes[$pregUrl][$method] = $uri;
		self::$paramsRoutes[$pregUrl][$method] = $params;
	}

	//Retourne les routes version regex
	public static function getRegexRoutes(){
		return self::$regexRoutes;
	}

	//Retourne les paramètres des routes
	public  function getParamsRoutes(){
		return self::$paramsRoutes;
	}
}