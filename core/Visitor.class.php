<?php
class Visitor{

	private $ip_address;
	private static $tabIpAddress = [];

	public function __construct($ip_address){
		$this->ip_address = $ip_address;
		Visitor::allIpAddress();
		$this->saveVisitor();
	}

	public static function allIpAddress(){
		$baseSql = new BaseSql();

		//Récupère la liste des adresses ip 
		$ip =  "SELECT ip_address FROM visitor";
		$ipExec = $baseSql->query($ip ,null, null);

		foreach ($ipExec as $key => $value) {
			Visitor::$tabIpAddress[] = $value['ip_address'];
		}
	}

	public function saveVisitor(){
		if(!in_array($this->ip_address, Visitor::$tabIpAddress)){
			try {
				$pdo = new PDO("mysql:host=".DBHOST.";dbname=".DBNAME,DBUSER,DBPWD);
			} catch(Exception $e){
				die("Erreur SQL".$e->getMessage()."\n");
			}

			$req = $pdo->prepare("INSERT INTO visitor (ip_address) VALUES ('".$this->ip_address."')");
	        $req->execute(array(
	            "ip_address" => $this->ip_address
	        ));
		}
	}

}