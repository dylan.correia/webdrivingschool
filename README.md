# Webdrivingschool

## Presentation

Une solution simple et efficace pour créer un site web, gérer son contenu et profiter des fonctionnalités métiers spécialement pensées pour les gérants et clients d’auto-école.
Le client pourra mettre en avant les informations relatives à son entreprise, gérer les
commentaires, les différents tarifs qu’il proposera, se connecter sur le back-office
afin de gérer sa clientèle et les fonctionnalités proposées.


## Installation

Il suffit de cloner le projet , le placer dans à la racine du dossier qui permet d'éxécuter du code sur votre serveur web .
Ensuite suivre les étapes de l'installation