<?php
    define("DBUSER","secret");
    define("DBPWD","secret");
    define("DBHOST","localhost");
    define("DBNAME","secret");
    define("DBPORT","3306");

	// define("DS",DIRECTORY_SEPARATOR);
	define("DS","/");
	$scriptName = (dirname($_SERVER["SCRIPT_NAME"]) == "/")?"":dirname($_SERVER["SCRIPT_NAME"]);
	define("DIRNAME",$scriptName.DS);

	define("DIRIMAGES", DIRNAME."resource/images/");
	define("DIRCSS", DIRNAME."css/");
	define("DIRJS", DIRNAME."resource/js/");
