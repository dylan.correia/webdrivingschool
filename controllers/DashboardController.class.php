<?php

class DashboardController
{
	
	function indexAction()
	{
		$allUser = User::all("hidden");
        $allRole = Role::all();

        foreach ($allUser as $keyUser => $valueUser) {
            foreach ($allRole as $keyRole => $valueRole) {
                if ($valueUser->getIdRole() == $valueRole->getId()) {
                    $valueUser->nameRole = $valueRole->getName();
                }
            }
        }

        $baseSql = new BaseSql();

        $queryString = "SELECT t.day, t.hour, u.lastname, u.firstname FROM timeslot as t, user as u WHERE t.id_student = u.id and t.day >= DATE(NOW()) ORDER BY t.day";
        $allTimeslot = $baseSql->query($queryString, null, null);

		$v = new View("dashboard","backoffice_menu");
		$v->assign('allUser', $allUser);
		$v->assign('allTimeslot', $allTimeslot);
		$v->assign('menuPage', 'dashboard');
	}
}