<?php

class ProgressController
{
    //Liste les utilisateurs
    function indexAction()
    {

        $bd = new BaseSql();
        $query = "SELECT user.* FROM user INNER JOIN role ON role.id = user.id_role WHERE role.name = :name AND hidden = 0";
        $allUser = $bd->query($query, ["name" => "Eleve"],'User');


        $v = new View("progress", "backoffice_menu");
        $v->assign('allUser', $allUser);
        $v->assign('menuPage', 'progress');
    }

    //Affiches les progressions d'un utilisateur
    public function listAction($params)
    {
        $query = 'SELECT progress.*, user.lastname, user.firstname , timeslot.* FROM progress INNER JOIN timeslot ON progress.id_timeslot = timeslot.id INNER JOIN user ON progress.id_monitor = user.id WHERE timeslot.id_student = :userId AND progress.hidden = 0';
        $bd = new BaseSql();
        $user = User::findById($params['idUser']);
        $progress = $bd->query($query, ['userId' => $params["idUser"]]);

        $v = new View("progressList", "backoffice_menu");
        $v->assign('user', $user);
        $v->assign('params',$params);
        $v->assign('progress', $progress);
        $v->assign('menuPage', 'progress');
    }


    //Formulaire d'ajout d'une progression
    public function addAction($params)
    {

        $progress = new Progress();
        $user = User::findById($params['idUser']);
        $errors = [];
        $query = 'SELECT timeslot.hour, timeslot.day, timeslot.id FROM timeslot LEFT JOIN progress ON timeslot.id = progress.id_timeslot WHERE id_student = :userId AND progress.id_timeslot IS NULL';
        $bd = new BaseSql();
        $selectTimeslot= $bd->query($query, ['userId' =>$params["idUser"]]);


        $config = $progress->configFormProgress($selectTimeslot,$params['idUser']);
        $v = new View("progressForm", "backoffice_menu");
        $v->assign("config", $config);
        $v->assign("errors", $errors);
        $v->assign("timeslot", $selectTimeslot);
        $v->assign("form", "formProgress");
        $v->assign("title", "Ajout Progression"." ". $user->getFirstname()." ". $user->getLastname() );
        $v->assign('menuPage', 'progress');

    }

    //formulaire modification
    public function showAction($params)
    {

        $progress = Progress::findById($params['idProgress']);
        $timeslot = Timeslot::findById($progress->getIdTimeSlot());
        $errors = [];
        $values =[];
        $values['title'] = $progress->getTitle();
        $values['comment'] = $progress->getComment();
        $values['timeslot'] = $timeslot->getId();
        $values['id']= $params['idProgress'];


        $query = 'SELECT DISTINCT timeslot.hour, timeslot.day, timeslot.id FROM timeslot WHERE timeslot.id = :id';
        $bd = new BaseSql();
        $selectTimeslot= $bd->query($query, ['id' =>$progress->getIdTimeSlot()]);

        $config = $progress->configFormProgress($selectTimeslot, $params['idUser'],$params['idProgress'],true);
        $date=date_create($progress->getDateUpdated());

        $v = new View("progressForm", "backoffice_menu");
        $v->assign("config", $config);
        $v->assign("errors", $errors);
        $v->assign("values", $values);
        $v->assign('params',$params);
        $v->assign('selectTimeslot',$selectTimeslot);
        $v->assign("form", "formProgress");
        $v->assign("title", "Modification Progression (Dernire modification le : ".date_format($date,"d/m/Y").")");
        $v->assign('menuPage', 'progress');

    }
    //Logique d'ajout d'une progression
    public function createAction($params)
    {
        $progress = new Progress();
        $config = $progress->configFormProgress(null,$params['idUser']);
        $errors = [];


        if (!empty($params)) {

            //$errors = Validate::checkForm($config, $params);

            if (empty($errors)) {
                $progress->setTitle($params['form']["title"]);
                $progress->setComment($params['form']["comment"]);
                $progress->setIdTimeslot($params['form']["timeslot"]);
                $progress->setIdMonitor(Auth::id());
                $progress->setStudentShow('0');
                $progress->setHidden('0');
                $progress->save();

                $msg = array("status"=> 'success', "message"=>'Progression ajoutée!');
                print_r(json_encode($msg)) ;
            } else {
                $msg = array("status"=> 'error',"message"=>$errors);
                print_r(json_encode($msg)) ;
            }
                die();
            }
        }


    //logique de suppression d'un user
    public function deleteAction($params)
    {

        if (isset($params['idProgress'])) {
            $progress = new Progress();
            $progress->setId($params['idProgress']);
            $progress->hidden();

            header("Location: " . DIRNAME . "progress");
            die();
        }
    }
    //logique de modification d'un user
    public function updateAction($params)
    {
        $progress = new Progress();
        $config = $progress->configFormProgress();
        $errors = [];

        if (!empty($params)) {

            //$errors = Validate::checkForm($config, $params);

            if (empty($errors)) {
                $progress->setId($params["form"]["id"]);
                $progress->setIdTimeslot($params["form"]["timeslot"]);
                $progress->setTitle($params["form"]["title"]);
                $progress->setComment($params["form"]["comment"]);
                $progress->setStudentShow('0');
                $progress->setHidden('0');
                $progress->save();
                $msg = array("status" => 'success', "message" => 'Progression modifiée');
                print_r(json_encode($msg));
            } else {
                $msg = array("status" => 'error', "message" => $errors);
                print_r(json_encode($msg));
            }
        }
        die();
    }
    //Progression de l'élève uniquement
    public function myprogressAction($params){

        $user = User::findById(Auth::id());
        $query = 'SELECT progress.*, user.lastname, user.firstname , timeslot.* FROM progress INNER JOIN timeslot ON progress.id_timeslot = timeslot.id INNER JOIN user ON progress.id_monitor = user.id WHERE timeslot.id_student = :userId AND progress.hidden = 0';
        $bd = new BaseSql();
        $progress = $bd->query($query, ['userId' => $user->getId()]);

        $v = new View("progressList", "backoffice_menu");
        $v->assign('myprogress', true);
        $v->assign('user', $user);
        $v->assign('params',$params);
        $v->assign('progress', $progress);
        $v->assign('menuPage', 'myprogress');
    }

}