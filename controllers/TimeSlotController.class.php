<?php
class TimeSlotController
{
	
	function indexAction()
	{
        $horaires = Timeslot::dataTimeSlotUser();
		$v = new View("timeslot","backoffice_menu");
		$v->assign("horaires", $horaires);
		$v->assign('menuPage','timeslot');
	}

	public function addAction() {
		/*echo "Formulaire de création d'un horaire";*/
		$timeslot = new Timeslot();
        /*$timeslot->setFirstname("Dylan");
        $timeslot->setLastname("Correia lopes");
        $timeslot->setEmail("Correia@lopes");
        $timeslot->setPwd("mdp");
        $timeslot->save();*/
        $config = $timeslot->configFormAdd();
		$errors = [];
		
		// if(!empty($params["POST"])){
			
		// 	$errors = Validate::checkForm($config, $params["POST"]);

		// 	if(empty($errors)){
		// 		$timeslot->setHour($params["POST"]["firstname"]);
		// 		$timeslot->setDay($params["POST"]["lastname"]);
		// 		$timeslot->setIdStudent($params["POST"]["pwd"]);
		// 		$timeslot->save();
		// 	}
		// }
		
		$v = new View("timeslotAdd","backoffice_menu");
		$v->assign("config", $config);
		$v->assign("errors", $errors);
		$v->assign("value", null);
		$v->assign('menuPage','timeslot');
	}

	public function showAction($params) {
		$timeslot = TimeSlot::findById($params['idTimeSlot']);

		$config = $timeslot->configFormAdd($timeslot);
		$errors = [];
		
		$v = new View("timeslotAdd","backoffice_menu");
		$v->assign("config", $config);
		$v->assign("errors", $errors);
		$v->assign('menuPage','timeslot');
	}
	public function showAdminAction() {
		$horaires = TimeSlot::all();
		$v = new View("timeslot","backoffice_menu");
		$v->assign("horaires", $horaires);
	}

	public function createAction() {
		echo "Création d’un horaire";
		$timeslot = new Timeslot();

		$timeslot->setHour($_POST["hour"]);
		$timeslot->setDay($_POST["day"]);
		$timeslot->setIdStudent($_POST["idStudent"]);
		$timeslot->setIdMonitor($_POST["idMonitor"]);
		$timeslot->save();

		header("Location:".DIRNAME."timeslot");
	}

	public function deleteAction($params) {
		$timeslot = TimeSlot::findById($params['idTimeSlot']);
	
		if($timeslot) {

            $bd = new BaseSql();
            $timeslots = $bd->query('SELECT id FROM timeslot WHERE id_timeslot = :idTimeSlot ',$params);
            $deadline = new DateTime();
            $deadline->modify('+2 day');

            if($deadline < new DateTime($timeslot->getDay())){
                $timeslot->delete();
                $timeslots = $bd->query('SELECT id,name FROM timeslot',null);
                $msg = array("status"=> 'success',"message"=>'horaire supprime',"horaires"=>$timeslots);
                print_r(json_encode($msg)) ;

            } else {
                $msg = array("status"=> 'error',"message"=>'horaire non supprimable , des horaires ont deja ete effectues ou seront effectues dans moins de 48h');
                print_r(json_encode($msg)) ;
            }
            
        }

        header("Location: " . DIRNAME . "timeslot");
        die();
	}

	/*public function updateAction($params) {
		var_dump($params);
		echo "Modification d’un horaire";
		$timeslot = new Timeslot();
		$config = $timeslot->configFormAdd();
		$errors = [];

		if(!empty($params)){

			$errors = Validate::checkForm($config, $params['form']);

			if(empty($errors)){
				$timeslot->setId($params["id"]);
				$timeslot->setHour($_POST["hour"]);
				$timeslot->setDay($_POST["day"]);
				$timeslot->setIdStudent($_POST["idStudent"]);
				$timeslot->setIdMonitor($_POST["idMonitor"]);
				$timeslot->save();

				header("Location:".DIRNAME."timeslot");				
			} else{
                $msg = array("status"=> 'error',    "message"=>$errors);
            }
        }

        header( "Content-Type: application/json" );
        print_r(json_encode($msg));
        die();

    	}*/
	public function updateAction($params) {
		var_dump($params);
		echo "Modification d’un horaire";
		
        $bd = new BaseSql();
        $timeslot = new Timeslot();
        $config = $timeslot->configFormAdd();
        $errors = Validate::checkForm($config, $params);


        if(!$errors) {
            $idTimeslot = $params['id'];
            $timeslot->setId($idTimeslot);
            $timeslot->setHour($params['hour']);
            $timeslot->setDay($params['day']);
            $timeslot->setIdStudent($params['idStudent']);
            $timeslot->setIdMonitor($params['idMonitor']);
            $timeslot->save();

            unset($params['hour']);
            unset($params['day']);
            unset($params['idStudent']);
            unset($params['idMonitor']);

            print_r(json_encode($msg)) ;
        } else {
            $msg = array("status"=> 'error',"message"=>$errors);
            print_r(json_encode($msg)) ;
        }
    }

}