<?php

class PackController
{
	/**
     * Suppression d'un forfait
     *
     * @param  array  $params [
     *                           int idPack
     *                   ]
     * @return JSON $msg
     */
	public function deleteAction($params) {
		$pack = Pack::findById($params['idPack']);

        if($pack) {
                $pack->delete();
                $bd = new BaseSql();
                $msg = array("status"=> 'success',"message"=>'Forfait supprimé');
                print_r(json_encode($msg)) ;

        } else {
            $msg = array("status"=> 'error',"message"=>'Forfait non supprimable');
            print_r(json_encode($msg)) ;
        }
	} 	

    /**
     * Ajout d'un forfait
     *
     * @param  array  $params [
     *                           string title
     *                           string description
     *                           int price
     *                   ]
     * @return JSON $msg
     */
	public function createAction($params) {
		$pack =  new Pack();
        $config = $pack->configFormPack();
        $errors = Validate::checkForm($config, $params);
        
        if(!$errors) {
        	$pack->setTitle($params['title']);
        	$pack->setDescription($params['description']);
        	$pack->setPrice($params['price']);
        	$pack->save();
        	$bd = new BaseSql();
            $packs = $bd->query('SELECT id,title,price FROM pack',null);
        	$msg = array("status"=> 'success',"message"=>'Forfait crée',"packs" =>$packs);
            print_r(json_encode($msg)) ;
        } else {
        	$msg = array("status"=> 'error',"message"=>$errors);
            print_r(json_encode($msg)) ;
        }
	} 

    /**
     * Récupère une offre
     *
     * @param  object  $object
     * @return JSON    $msg
     */

	public function showAction($params){
		$bd = new BaseSql();
        $pack = $bd->query('SELECT * FROM pack WHERE id = :idPack',$params);

        if(!$pack){
            $msg = array("status"=> 'error',"message"=>'Le forfait n\'éxiste pas');
            print_r(json_encode($msg)) ;
        } else {
        	$msg = array("status"=> 'success',"pack" =>$pack);
            print_r(json_encode($msg)) ;
        }
	}

    /**
     * Mets à jour une offre
     *
     * @param  object  $object
     * @return JSON    $msg
     */

	public function updateAction($params){

		$pack =  new Pack();
        $config = $pack->configFormPack();
        $errors = Validate::checkForm($config, $params);

        if(!$errors) {
            $pack->setId($params['idPack']);
            $pack->setTitle($params['title']);
        	$pack->setDescription($params['description']);
        	$pack->setPrice($params['price']);
        	$pack->save();
        	$bd = new BaseSql();
            $packs = $bd->query('SELECT id,title,price FROM pack',null);
            $msg = array("status"=> 'success',"message"=>'Forfait modifié',"packs"=>$packs);
            print_r(json_encode($msg)) ;

        } else {
            $msg = array("status"=> 'error',"message"=>'Forfait non modifié');
            print_r(json_encode($msg)) ;
        }
	}
}