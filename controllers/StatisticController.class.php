<?php

class StatisticController
{
	
	function indexAction()
	{	
		$baseSql = new BaseSql();

		//Récupère la liste des inscriptions
		$signUp =  "SELECT u.date_inserted as dateStatistics FROM user as u, role as r WHERE u.id_role = r.id and r.name='Eleve'";
		$signUpExec = $baseSql->query($signUp ,null, null);
		$dataSignUp = $this->numberStatistics($signUpExec);

		//Récupère la liste des commentaires
		$feedback =  "SELECT date_inserted as dateStatistics FROM feedback";
		$feedbackExec = $baseSql->query($feedback ,null, null);
		$dataFeedback = $this->numberStatistics($feedbackExec);

		//Récupère la liste des visiteurs
		$visitor =  "SELECT date_inserted as dateStatistics FROM visitor";
		$visitorExec = $baseSql->query($visitor ,null, null);
		$dataVisitor = $this->numberStatistics($visitorExec);

		$v = new View("statistics","backoffice_menu");
		$v->assign('menuPage', 'statistics');
		$v->assign('dataSignUp', $dataSignUp);
		$v->assign('dataFeedback', $dataFeedback);
		$v->assign('dataVisitor', $dataVisitor);
	}

	function numberStatistics($array){
		$data = [];
		$all = count($array);
		$year = 0;
		$month = 0;
		$today = 0;

		$dateToday = new DateTime();
		foreach ($array as $key => $value) {
			$date = new DateTime($value['dateStatistics']);

			if($dateToday->format('Y') == $date->format('Y')){
				$year += 1;
			}

			if($dateToday->format('m') == $date->format('m')){
				$month += 1;
			}

			if(strtotime($date->format('Y-m-d')) == strtotime($dateToday->format('Y-m-d'))){
				$today += 1;
			}
		}

		$data['all'] = $all;
		$data['year'] = $year;
		$data['month'] = $month;
		$data['today'] = $today;

		return $data;
	}

	public function chartAction()
	{
		$baseSql = new BaseSql();
		$dataRegistrationLicense = [];
		$date = new DateTime();

		//Récupère le nombre d'utilisateur par type de permis depuis toujours
    	$registrationLicense = "SELECT count(lastname) as numberUsers, type_license FROM user WHERE type_license <> '' GROUP BY type_license ORDER BY type_license";
        $registrationLicenseExec = $baseSql->query($registrationLicense ,null, null);

        //Récupère le nombre d'utilisateur par type de permis depuis cette année
        $dateTodayYear = $date->format('Y') . "-01-01";
    	$registrationLicenseYear = "SELECT count(lastname) as numberUsers, type_license FROM user WHERE type_license <> '' and date_inserted > '" . $dateTodayYear . "' GROUP BY type_license ORDER BY type_license";
        $registrationLicenseYearExec = $baseSql->query($registrationLicenseYear ,null, null);

        //Récupère le nombre d'utilisateur par type de permis depuis ce mois
        $dateTodayMonth = $date->format('Y') . "-" . $date->format('m') . "-01";
    	$registrationLicenseMonth = "SELECT count(lastname) as numberUsers, type_license FROM user WHERE type_license <> '' and date_inserted > '" . $dateTodayMonth . "' GROUP BY type_license ORDER BY type_license";
        $registrationLicenseMonthExec = $baseSql->query($registrationLicenseMonth ,null, null);

        //Récupère le nombre d'utilisateur par type de permis depuis aujourd'hui
    	$registrationLicenseToday = "SELECT count(lastname) as numberUsers, type_license FROM user WHERE type_license <> '' and date_inserted >= DATE(NOW()) GROUP BY type_license ORDER BY type_license";
        $registrationLicenseTodayExec = $baseSql->query($registrationLicenseToday ,null, null);

        $dataRegistrationLicense['all'] = $this->registrationLicense($registrationLicenseExec);
        $dataRegistrationLicense['year'] = $this->registrationLicense($registrationLicenseYearExec);
        $dataRegistrationLicense['month'] = $this->registrationLicense($registrationLicenseMonthExec);
        $dataRegistrationLicense['today'] = $this->registrationLicense($registrationLicenseTodayExec);

		header("Content-Type: application/json");
        echo json_encode($dataRegistrationLicense);
    }

	function registrationLicense($array){
		$color = [
		    "AM" => "#7FC6BC",
		    "A1" => "#4A1A2C",
		    "A2" => "#FFCE56",
		    "A" => "#4BC0C0",
		    "B1" => "#E6E2AF",
		    "B" => "#FF6384",
		    "BE" => "#BD8D46",
		    "C1" => "#B5E655",
		    "C1E" => "#84815B",
		    "C" => "#046380",
		    "CE" => "#002F2F",
		    "D1" => "#C03000",
		    "D1E" => "#EDF7F2",
		    "D" => "#A7A37E",
		    "DE" => "#FF358B"
		];

		$dataRegistrationLicense = [];

        $labels = [];
        $data = [];
        $backgroundcolor = [];
        foreach ($array as $key => $value) {
        	$labels[] = $value['type_license'];
        	$data[] = $value['numberUsers'];
        	if(array_key_exists($value['type_license'], $color)){
        		$backgroundcolor[] = $color[$value['type_license']];
        	}
        }
        $dataRegistrationLicense['labels'] = $labels;
        $dataRegistrationLicense['data'] = $data;
        $dataRegistrationLicense['backgroundcolor'] = $backgroundcolor;

        return $dataRegistrationLicense;
	}

}