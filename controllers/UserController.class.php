<?php
class UserController
{
    function indexAction()
    {
        $allUser = User::all("hidden");
        $allRole = Role::all();

        foreach ($allUser as $keyUser => $valueUser) {
            foreach ($allRole as $keyRole => $valueRole) {
                if ($valueUser->getIdRole() == $valueRole->getId()) {
                    $valueUser->nameRole = $valueRole->getName();
                }
            }
        }

        $v = new View("user", "backoffice_menu");
        $v->assign('allUser', $allUser);
        $v->assign('allRole', $allRole);
        $v->assign('menuPage', 'users');
    }

    public function addAction()
    {
        $user = new User();
        $config = $user->configForm();
        $errors = [];

        $v = new View("userForm", "backoffice_menu");
        $v->assign("config", $config);
        $v->assign("errors", $errors);
        $v->assign("title", "CRÉATION UTILISATEUR");
        $v->assign('menuPage', 'users');
    }

    public function showAction($params)
    {
        $dataUser = User::dataUserRole($params['id']);

        $user = new User();
        $config = $user->configForm($dataUser[0], true);
        $errors = [];

        $v = new View("userForm", "backoffice_menu");
        $v->assign("config", $config);
        $v->assign("errors", $errors);
        $v->assign("title", "MODIFICATION UTILISATEUR");
        $v->assign('menuPage', 'users');
    }

    public function createAction($params)
    {
        $user = new User();
        $config = $user->configForm();
        $errors = [];
        $mails = [];

        if (!empty($params)) {

            foreach (User::all() as $keyUser => $valueUser) {
                $mails[] = $valueUser->getEmail();
            }

            $errors = Validate::checkForm($config, $params);

            if( in_array(strtolower(trim($params["email"])), $mails) ){
                $errors[] = "Mail déjà utilisé";
            }

            if (empty($errors)) {

                $password = $user->generatePwd(8);

                if(isset($_FILES['picture'])){
                    $dir = "resource/images/picture_users/";
                    $info = new SplFileInfo($_FILES['picture']['name']);
                    $ext = $info->getExtension();
                    $file = $params["firstname"] . "_" . $params["lastname"] . "_" . uniqid() . "." . $ext;

                    if(move_uploaded_file($_FILES['picture']['tmp_name'], $dir . $file)){
                        $user->setPicture($file);
                    }
                }

                $user->setFirstname($params["firstname"]);
                $user->setLastname($params["lastname"]);
                $user->setPassword($password);
                $user->setEmail($params["email"]);
                $user->setAddress($params["address"]);
                $user->setCity($params["city"]);
                $user->setPostcode($params["postcode"]);
                $user->setToken($user->generateToken());
                $user->setBirthDate($params["birthDate"]);
                $user->setTypeLicense($params["typeLicense"]);
                $user->setHoursDrivingLicense($params["hoursDrivingLicense"]);
                $user->setIdRole($params["role"]);
                $user->setHidden(0);
                $user->save();

                $this->newaccountAction($user, $password);


                header("Location: " . DIRNAME . "users");
                die();
            }

            $config = $user->configForm($params);
            $v = new View("userForm", "backoffice_menu");
            $v->assign("config", $config);
            $v->assign("errors", $errors);
            $v->assign("title", "CRÉATION UTILISATEUR");
            $v->assign('menuPage', 'users');
        }
    }

    public function updateAction($params)
    {   
        $user = new User();
        $config = $user->configForm(false, false, true);
        $errors = [];
        $mails = [];

        if (!empty($params)) {

            foreach (User::all() as $keyUser => $valueUser) {
                $mails[] = $valueUser->getEmail();
            }

            $dataUser = User::dataUserRole($params["idUser"]);

            $errors = Validate::checkForm($config, $params['form']);

            if( in_array(strtolower(trim($params['form']["email"])), $mails) ){
                if(strtolower(trim($params['form']["email"])) != $dataUser[0]['email']){
                    $errors[] = "Mail déjà utilisé";
                }
            }

            if (empty($errors)) {

                if(isset($params['files'])){
                    $targetDir = "resource/images/picture_users/";
                    $files = $params['files'];
                    foreach ($files as $name => $file){
                        $decoded_file = base64_decode($file['base64']);
                        $imageType = [  "image/png" => "png",
                                        "image/gif "=>  "gif",
                                        "image/jpeg" => "jpg",
                                        "image/svg+xml" => "svg"
                        ];
                        $ext =  $imageType[$file['type']];
                        $nameFile = $params['form']["firstname"] . "_" . $params['form']["lastname"] . "_" . uniqid() . "." . $ext;
                        $filePtah = $targetDir . $nameFile;
                        file_put_contents($filePtah, $decoded_file);
                        $user->setPicture($nameFile);
                    }
                }

                if(isset($params['form']['password']) && $params['form']['password'] != ""){
                    $user->setPassword($params['form']['password']);
                }

                $user->setId($params["idUser"]);
                $user->setFirstname($params['form']["firstname"]);
                $user->setLastname($params['form']["lastname"]);
                $user->setEmail($params['form']["email"]);
                $user->setAddress($params['form']["address"]);
                $user->setCity($params['form']["city"]);
                $user->setPostcode($params['form']["postcode"]);
                $user->setBirthDate($params['form']["birthDate"]);
                $user->setTypeLicense($params['form']["typeLicense"]);
                $user->setHoursDrivingLicense($params['form']["hoursDrivingLicense"]);
                $user->setIdRole($params['form']["role"]);
                $user->save();
                $msg = array("status" => 'success', "message" => 'Utilisateur modifié');
            } else {
                $msg = array("status" => 'error', "message" => $errors);
            }
        }

        header("Content-Type: application/json");
        print_r(json_encode($msg));
        die();
    }

    public function deleteAction($params)
    {
        if (isset($params['id'])) {
            $user = new User();
            $user->setId($params['id']);

            $user->hidden();

            header("Location: " . DIRNAME . "users");
            die();
        }
    }

    public function searchAction($params)
    {
        if (isset($params['role'])) {
            $filterUser = User::filterRoleUsers($params['role']);
        } elseif (isset($params['search'])) {
            $filterUser = User::filterSearchUsers($params['search']);
        }

        header("Content-Type: application/json");
        echo json_encode($filterUser);
    }

    public function profileAction(){
        $dataUser = User::dataUserRole(Auth::id());

        $user = new User();
        $config = $user->configForm($dataUser[0], true, true);
        $errors = [];

        $v = new View("userForm", "backoffice_menu");
        $v->assign("config", $config);
        $v->assign("errors", $errors);
        $v->assign("profile", "profile");
        $v->assign("title", "MODIFICATION UTILISATEUR");
        $v->assign('menuPage', 'users');

    }

    public function loginAction($params)
    {
        $user = new User();
        $config = $user->configFormLogin();
        $errors = [];
        $alert = null;
        $_SESSION['token'] = null;
        $_SESSION['errormsg'] = null;



        if (!empty($params)) {
            //verification du formulaire
            $errors = Validate::checkForm($config, $params);


            if (empty($errors)) {
                //requete de recupération des données user grace à son mdp

                $query = 'SELECT email, password, token FROM user WHERE email = :email AND hidden = 0';

                if (empty($user = $user->query($query, ['email' => $params["email"]], 'User'))) {

                    $error = 'Identifiant ou mot de passe incorrect';
                    $_SESSION['errormsg'] = $error;
                    header("location:".DIRNAME);
                    die;

                } else {

                    if (password_verify($params["pwd"], $user[0]->getPassword())) {
                        //attribution à la variable de session token
                        $_SESSION['token'] = $user[0]->getToken();

                        $error = 'Vous êtes connecté !';
                        Auth::init($user[0]->getToken()); // Initialise les informations de l'utilisateur

                        if(Auth::hasPermission('dashboardRight')){
                            header("location:" . DIRNAME . "dashboard"); //redirige vers la page index
                        } else if (Auth::hasPermission('planning')){
                            header("location:" . DIRNAME . "planning");
                        } else if (Auth::hasPermission('myPlanningRight')){
                            header("location:" . DIRNAME . "planning/myplanning");
                        } else if (Auth::hasPermission('myFeedbackRight')){
                            header("location:" . DIRNAME . "myfeedback");
                        }
                        return true;

                    } else {
                        $error = 'Identifiant ou mot de passe incorrect';
                        $_SESSION['errormsg'] = $error;
                        header("location:".DIRNAME);
                        die;
                    }
                } 
            } else {
                $error = 'Identifiant ou mot de passe incorrect';
                $_SESSION['errormsg'] = $error;
                header("location:".DIRNAME);
                die;
            }

        } else {
            $error = 'Identifiant ou mot de passe incorrect';
            $_SESSION['errormsg'] = $error;
            header("location:".DIRNAME);
            die;
        }

    }

    public function logoutAction($params)
    {
        Auth::destroy();
        header("location:" . DIRNAME); //redirige vers la page index
        exit();
    }

    //mot de passe oublié
    public function lostpwdAction($params)
    {
        //Mot de passe perdu
        $user = new User;
        $config = $user->configFormLostpwd();
        $_SESSION['errorpwd'] = null;

        //Recupération du formulaire
        if (!empty($params)) {

            //vérification des champs
            $errors = Validate::checkForm($config, $params);

            if (empty($errors)) {

                //Requete qui récupère le token de l'email fourni s'il existe en bdd
                $query = 'SELECT token FROM user WHERE email = :email';
                $user = $user->query($query, ['email' => $params["email"]], 'User');
                if (!empty($user)) {
                    $token = $user[0]->getToken();
                    function requireToVar($file, $token)
                    {
                        ob_start();
                        require($file);
                        return ob_get_clean();
                    }

                    $objet = utf8_decode('WDS Réinitialisation mot de passe');
                    $corps = requireToVar('./resource/mail/passwordlostMail.php', $token);
                    $mail = new Mailer();
                    $mail->mailSend(MAILFROMLOGIN, 'WDS', MAILFROMPASSWORD, $params['email'], $objet, $corps);

                    $error =  "Mail de récupération de mot de passe envoyé";
                    $_SESSION['errorpwd'] = $error;
                    header("location:".DIRNAME);
                } else {
                    $error =  "L'adresse mail fournie n'existe pas";
                    $_SESSION['errorpwd'] = $error;
                    header("location:".DIRNAME);
                }
            } else {
                $error = "L'adresse mail fournie n'existe pas";
                $_SESSION['errorpwd'] = $error;
                header("location:".DIRNAME);
            }
        }
    }

    // Réinitialisation de mdp
    public function resetpwdAction($params)
    {
        $user = new User();
        //$config = $user->configFormResetpwd();

        if (!empty($params)) {

            $query = 'SELECT * FROM user WHERE token = :token';
            $result1 = $user->query($query, ['token' => $params["token"]], 'User');

            if (!empty($result1)) {

                $password = $user->generatePwd(8);
                $user->setPassword($password);
                $crypted_password = $user->getPassword();
                $bd = new BaseSql();
                $query = 'UPDATE user SET password = :password WHERE token = :token';
                $bd->query($query, ['token' => $params['token'], 'password' => $crypted_password]);
                $result = $bd->query('SELECT * FROM user WHERE password = :password',['password' => $crypted_password],'User');

                if ($result) {

                    $objet = utf8_decode('WDS Nouveau mot de passe');

                    //concatene le nouveau mdp avec le corps du mail
                    function requireToVar($file, $password)
                    {
                        ob_start();
                        require($file);
                        return ob_get_clean();
                    }
                    $corps = requireToVar('./resource/mail/passwordresetMail.php', $password);
                    $email = $result1[0]->getEmail();
                    $mail = new Mailer();
                    $mail->mailSend(MAILFROMLOGIN, 'WDS', MAILFROMPASSWORD, $email, $objet, $corps);
                    header('Location:' . DIRNAME . '');

                }
            }
        }
    }


    //envoi d'un mail contenant le mdp à la création d'un user
    private function newaccountAction(User $ObjectUser, $pass)
    {
        //concatene le nouveau mdp avec le body du mail
        function requireToVar($file, $pass)
        {
            ob_start();
            require($file);
            return ob_get_clean();
        }
        $object = utf8_decode('Nouveau compte WDS');
        $body = requireToVar('./resource/mail/newaccountMail.php', $pass);
        Mailer::mailSend(MAILFROMLOGIN, 'WDS', MAILFROMPASSWORD, $ObjectUser->getEmail(), $object, $body);
        //header('Location:' . DIRNAME . '');
    }


}
