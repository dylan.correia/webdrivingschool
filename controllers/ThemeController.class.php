<?php

class ThemeController {

    /**
     * Affichage des thèmes
     *
     * @return void
     */
	public function indexAction() {

		$v = new View("themes","backoffice_menu");
        $v->assign('themes',Theme::all());
        $v->assign('menuPage','themes');
	}

    /**
     * Affichage d'un thème
     *
     * @param  array  $params [
     *                           int idTheme
     *                   ]
     * @return JSON $object
     */
	public function showAction($params) {
        $theme = Theme::findById($params['idTheme']);

        $object = [];

        if($theme){
            $object['id'] = $theme->getId();
            $object['name'] = $theme->getName();
            $object['description'] = $theme->getDescription();
            $object['image'] = $theme->getImage();
        }

        print_r(json_encode($object)) ;

	}

    /**
     * Mis à jour d'un thème
     *
     * @param  array  $params [
     *                           int idTheme
     *                   ]
     * @return JSON $msg
     */
	public function updateAction($params) {

        $theme = Theme::findById($params['idTheme']);
        if($theme) {
            $bd = new BaseSql();
            $oldTheme = $bd->query('SELECT * FROM theme WHERE choosen = 1 ',null,'Theme')[0];
            $oldTheme->setChoosen('0');
            $oldTheme->save();

            $theme->setChoosen('1');
            $theme->save();

            $msg = array("status"=> 'success',"message"=>'Thème mis à jour ');
            print_r(json_encode($msg)) ;

        } else {
            $msg = array("status"=> 'error',"message"=>'Thème non modifiable');
            print_r(json_encode($msg)) ;
        }

    }

}