<?php

class RoleController {

    /**
     * Affiche la liste des rôles
     *
     * @return void
     */
    function indexAction()
    {
        $roles =  Role::all();
        $v = new View("roles","backoffice_menu");
        $v->assign('roles',$roles);
        $v->assign('menuPage','roles');
    }

    /**
     * Affiche le formulaire de création
     *
     * @param  array  $errors
     * @return JSON $msg
     */
	public function addAction($errors = null) {
        $role = new Role();
        $v = new View("rolesRight","backoffice_menu");
        $values = [];
        $values['permissions']= Permission::all();

        $v->assign('menuPage','roles');
        $v->assign('form','formRole');
        $v->assign('config',$role->configFormRole());
        $v->assign('values',null);
        $v->assign('errors',$errors);
        $v->assign('values',$values);
        $v->assign('title','Ajout d\'un rôle');

    }

    /**
     * Affichage d'un rôle
     *
     * @param  array  $params [
     *                           int idRole
     *                   ]
     * @return void
     */
	public function showAction($params) {
        $role = Role::findById($params['idRole']);

        if($role) {
            $permissions = Permission::all();
            $bd = new BaseSql();
            $rolePermissions = $bd->query('SELECT * FROM role_permission WHERE id_role =:idRole',$params);

            // On récupère toutes les permissions  qui doivent etre coché à l'écran
            foreach ($permissions as $permission){
                foreach ($rolePermissions as $rolePermission){
                    if($permission->getId()===$rolePermission['id_permission']){
                        $permission->checked = true;
                    }
                }
            }

            $allChecked = (count($rolePermissions)===count($permissions))?true:null; // On regarde si toutes les permissions doivent être cochées
            $values =[];
            $values['permissions'] = $permissions;
            $values['roleName'] = $role->getName();
            $values['roleDescription'] = $role->getDescription();
            $values['allChecked'] = $allChecked;
            $config = $role->configFormRole();

            // On met à jour la config pour une modification
            $config['config']['action']= DIRNAME."roles/".$params['idRole'];
            $config['config']['method']= 'PUT';
            $config['config']['type']= 'button';

            $v = new View("rolesRight","backoffice_menu");
            $v->assign('menuPage','roles');
            $v->assign('values',$values);
            $v->assign('config',$config);
            $v->assign('form','formRole');
            $v->assign('title','MODIFICATION D\'UN RÔLE');


        } else {
            header('Location: '.DIRNAME.'roles');
        }

    }
    /**
     * Ajout d'un forfait
     *
     * @param  array  $params [
     *                           string roleName
     *                   ]
     * @return JSON $msg
     */
	public function createAction($params) {

        $bd = new BaseSql();
        $role = new Role();
        $config = $role->configFormRole();
        $errors = Validate::checkForm($config, $params);

        $result = $bd->query('SELECT * FROM role WHERE name=:name',['name'=>$params['roleName']]); // on recherche le rôle grâce à son nom

        if(!empty($result)){
            $errors[] = "La rôle existe déja";
        }

        if(!$errors) {
            $role->setName($params['roleName']);
            $role->setDescription($params['roleDescription']);
            $role->save();
            $role = $bd->query('SELECT id FROM role WHERE name = :name',['name'=>$params['roleName']],'Role')[0];
            unset($params['roleName']);
            unset($params['roleDescription']);
            $queryString = [];

            foreach ($params as $ability => $state){
                $queryString[] = 'INSERT INTO role_permission (id_role, id_permission) VALUES ('.$role->getId().',:'.$ability.')';
                $params[$ability] =$ability ;
            }

            $bd->query(implode(';',$queryString),$params);
            header('Location: '.DIRNAME.'roles');
        } else {
            $this->addAction($errors);
        }


	}

    /**
     * Suppression d'un rôle
     *
     * @param  array  $params [
     *                           int idRole
     *                   ]
     * @return JSON $msg
     */
	public function deleteAction($params) {
        $role = Role::findById($params['idRole']);

        if($role) {

            $bd = new BaseSql();
            $users = $bd->query('SELECT id FROM user WHERE id_role = :idRole ',$params); // on récupère les users ayant ce rôle

            if(empty($users)){
                $role->delete();
                $roles = $bd->query('SELECT id,name FROM role',null);
                $msg = array("status"=> 'success',"message"=>'Rôle supprimé',"roles"=>$roles);
                print_r(json_encode($msg)) ;

            } else {
                $msg = array("status"=> 'error',"message"=>'Rôle non supprimable , des utilisateurs possèdent ce rôle');
                print_r(json_encode($msg)) ;
            }
        }
	}

    /**
     * Mis à jour d'un rôle
     *
     * @param  array  $params [
     *                           int idRole
     *                           string roleName
     *                           string roleDescription
     *                   ]
     * @return void
     */
    public function updateAction($params) {

        $bd = new BaseSql();
        $role = new Role();
        $config = $role->configFormRole();
        $errors = Validate::checkForm($config, $params);


        if(!$errors) {
            $idRole = $params['idRole'];
            $role->setId($idRole);
            $role->setName($params['roleName']);
            $role->setDescription($params['roleDescription']);
            $role->save();

            unset($params['roleName']);
            unset($params['roleDescription']);
            unset($params['idRole']);
            $queryString = [];

            //On crée les requêtes mutiples d'insertion
            foreach ($params as $ability){
                $queryString[] = 'INSERT INTO role_permission (id_role, id_permission) VALUES ('.$idRole.',:'.$ability.')';
                $params[$ability] =$ability ;
            }

            // On supprime les précedentes permissions du rôle
            $bd->query('DELETE  FROM role_permission WHERE id_role =:idRole',['idRole' => $idRole]);
            // On ajoute les nouvelles permissions du rôle
            $bd->query(implode(';',$queryString),$params);
            $msg = array("status"=> 'success', "message"=>'Rôle mis à jour .');
            print_r(json_encode($msg)) ;
        } else {
            $msg = array("status"=> 'error',"message"=>$errors);
            print_r(json_encode($msg)) ;
        }
    }
}