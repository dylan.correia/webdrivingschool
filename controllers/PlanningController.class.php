<?php

class PlanningController {

    function indexAction(){
        $planning = new Planning();
        $numberMonitor = User::numberUserRole("Moniteur");
        $planningDraw = $planning->draw();

        //Récupèration des données nécessaire
        $days = $planningDraw['days'];
        $data = $planningDraw['data'];
        $drawPlanning = $planningDraw['drawPlanning'];

        $role = Role::all();
        $id_role_monitor = '';
        $id_role_student = '';
        foreach ($role as $key => $value) {
            if($value->getName() == "Moniteur"){
                $id_role_monitor = $value->getId();
            }
            elseif($value->getName() == "Eleve"){
                $id_role_student = $value->getId();
            }
        }

    	$v = new View("planning","backoffice_menu");
        $v->assign('menuPage', 'planning');
        $v->assign('days', $days);
        $v->assign('data', $data);
        $v->assign('numberMonitor', $numberMonitor);
        $v->assign('planning', $drawPlanning);
        $v->assign('year', $planning->getYear());
        $v->assign('week', $planning->getWeek());
        $v->assign('allMonitor', User::filterRoleUsers($id_role_monitor));
        $v->assign('allStudent', User::filterRoleUsers($id_role_student));
    }

    function showAction($params){
        $planning = new Planning();
        $userRole = User::dataUserRole($params['idUser']);

        if(count($userRole) != 0 && $userRole[0]['name'] == "Moniteur"){
            $planningDraw = $planning->draw(null, $params['idUser']);
        }
        elseif(count($userRole) != 0 && $userRole[0]['name'] == "Eleve"){
            $planningDraw = $planning->draw($params['idUser'], null);
        }
        else{
            $planningDraw = $planning->draw();
        }

        //Récupèration des données nécessaire
        $days = $planningDraw['days'];
        $data = $planningDraw['data'];
        $drawPlanning = $planningDraw['drawPlanning'];

        $v = new View("planning","backoffice_menu");
        $v->assign('menuPage', 'planning');
        $v->assign('days', $days);
        $v->assign('data', $data);
        $v->assign('planning', $drawPlanning);
        $v->assign('year', $planning->getYear());
        $v->assign('week', $planning->getWeek());

        if(count($userRole) != 0 && ($userRole[0]['name'] == "Moniteur" || $userRole[0]['name'] == "Eleve")){
            $v->assign('role', $userRole[0]['name']);
            $v->assign('firstname', $userRole[0]['firstname']);
            $v->assign('lastname', $userRole[0]['lastname']);
            $v->assign('idUser', $params['idUser']);
        }
        else{
            $numberMonitor = User::numberUserRole("Moniteur");
            $v->assign('numberMonitor', $numberMonitor);
        }
    }

    function navigationAction($params){
        $weekExploded = explode("_", $params['week']);

        if(strlen($weekExploded[0]) == 1){
            $week = "0".$weekExploded[0];
        }
        else{
            $week = $weekExploded[0];
        }
        $year = $weekExploded[1];

        $id = $params['id'];

        $planning = new Planning($year, $week);

        if($id == null){
            $numberMonitor = User::numberUserRole("Moniteur");
            $planningDraw = $planning->draw();
        }
        else{
            $userRole = User::dataUserRole($id);
            if(count($userRole) != 0 && $userRole[0]['name'] == "Moniteur"){
                $planningDraw = $planning->draw(null, $id);
            }
            elseif(count($userRole) != 0 && $userRole[0]['name'] == "Eleve"){
                $planningDraw = $planning->draw($id, null);
            }
            else{
                $planningDraw = $planning->draw();
            }
        }

        $days = $planningDraw['days'];
        $data = $planningDraw['data'];
        $planning = $planningDraw['drawPlanning'];

        $html = "<div class='planningOwn'>";
        if(isset($userRole[0]['name']) && isset($userRole[0]['firstname']) && isset($userRole[0]['lastname'])){
            $html .= "<p id='user_" . $id . "' class='ownerPlanning'>" . $userRole[0]['lastname'] . " " . $userRole[0]['firstname'] . " (" . $userRole[0]['name'] . ")</p>";
        }
        else{
            $html .= "<p class='ownerPlanning'>Planning général</p>";
        }
        $html .= "</div>";

        $html .= "<div id='planning'>";
            $html .= "<table>";
                $html .= "<tr>";
                    $html .= "<td class='menuPlanning'>Horaire</td>";

                        $dateFirst = new DateTime($data->start);
                        $dateToday = new DateTime($data->today);
                        $dateEnd = new DateTime($data->end);
                        $iterator = 0;

                        while($dateFirst <= $dateEnd){

                            if($dateFirst == $dateToday){
                                $html .= "<td class='menuPlanning today'>" . $days[$iterator] . " " . $dateFirst->format('d/m/y') . "</td>";
                            }
                            else{
                                $html .= "<td class='menuPlanning'>" . $days[$iterator] . " " . $dateFirst->format('d/m/y') . "</td>";
                            }

                            $dateFirst->modify('+1 day');
                            $iterator++;
                        }

                $html .= "</tr>";

                if(isset($userRole[0]['name']) && isset($userRole[0]['firstname']) && isset($userRole[0]['lastname'])){
                    foreach($planning as $key => $value){
                        $html .= "<tr id='plannigDay" . $key . "'>";
                        $html .= "<td class='menuPlanning'>" . $key ."</td>";

                        foreach ($value as $keyDay => $valueDay) {
                            $html .= "<td id='plannigDay" . $keyDay . "' class='boxPlanning'>";

                            foreach ($valueDay as $keyData => $valueData) {
                                $html .= '<p title="' . $valueData->lastname . ' ' . $valueData->firstname . '">' . substr($valueData->lastname, 0, 1) . '. ' . $valueData->firstname . '</p>';
                            }
                            $html .= "</td>";
                        }
                        $html .= "</tr>";
                    }
                }
                else{
                    $numberMonitor = (isset($numberMonitor))? $numberMonitor[0]['numberUser'] : 0;

                    foreach ($planning as $key => $value) {
                        $html .= "<tr id='plannigDay" . $key . "'>";
                        $html .= "<td class='menuPlanning'>" . $key . "</td>";

                        foreach ($value as $keyDay => $valueDay) {
                            $monitorSingle = [];
                            foreach($valueDay as $keyData => $valueData){
                                if(!in_array($valueData->id_monitor ,$monitorSingle)){
                                    $monitorSingle[] = $valueData->id_monitor;
                                }
                            }
                            if(count($monitorSingle) >= $numberMonitor || $numberMonitor == 0){
                                $class = 'planningRed';
                            }
                            elseif(count($monitorSingle) == 0 && $numberMonitor > 0){
                                $class = 'planningWhite';
                            }
                            else{
                                $class = 'planningGreen';
                            }

                            $html .= "<td class='" . $class . " boxPlanning' id='plannigDay" . $keyDay . "'>";
                                $html .= "<div class='popin' id='popin_" . $key . "_" . $keyDay . "'>";

                                $monitorLastname = ''; 
                                $monitorFirstname = '';
                                foreach ($valueDay as $keyData => $valueData) {
                                    if($monitorLastname != $valueData->lastname_monitor && $monitorFirstname != $valueData->firstname_monitor){
                                        $html .= "<strong><p>" . $valueData->lastname_monitor . " " . $valueData->firstname_monitor . " (Moniteur)</p></strong>";
                                        $monitorLastname = $valueData->lastname_monitor;
                                        $monitorFirstname = $valueData->firstname_monitor;
                                    }
                                                    
                                    $html .= "<p>" . $valueData->lastname . " " . $valueData->firstname . "<i id='delete_". $valueData->id . "' class='iconClose ion-close'></i></p>";
                                }
                                $html .= "<p class='addTimeslot'><i class='iconAdd ion-ios-plus-outline'></i></p>";
                            $html .= "</div>";
                        $html .= "</td>";
                        }
                    $html .= "</tr>";
                    }
                }

            $html .= "</table>";
        $html .= "</div>";

        header( "Content-Type: application/json" );
        echo json_encode( $html );
    }

    function myPlanningAction(){
        $planning = new Planning();
        $userRole = User::dataUserRole(Auth::id());

        if(count($userRole) != 0 && $userRole[0]['name'] == "Moniteur"){
            $planningDraw = $planning->draw(null, Auth::id());
        }
        elseif(count($userRole) != 0 && $userRole[0]['name'] == "Eleve"){
            $planningDraw = $planning->draw(Auth::id(), null);
        }
        else{
            $planningDraw = $planning->draw();
        }

        //Récupèration des données nécessaire
        $days = $planningDraw['days'];
        $data = $planningDraw['data'];
        $drawPlanning = $planningDraw['drawPlanning'];

        $v = new View("planning","backoffice_menu");
        $v->assign('menuPage', 'myPlanning');
        $v->assign('days', $days);
        $v->assign('data', $data);
        $v->assign('planning', $drawPlanning);
        $v->assign('year', $planning->getYear());
        $v->assign('week', $planning->getWeek());

        if(count($userRole) != 0){
            $v->assign('role', $userRole[0]['name']);
            $v->assign('firstname', $userRole[0]['firstname']);
            $v->assign('lastname', $userRole[0]['lastname']);
            $v->assign('idUser', Auth::id());
        }
        else{
            $numberMonitor = User::numberUserRole("Moniteur");
            $v->assign('numberMonitor', $numberMonitor);
        }
    }

    function removetimeslotAction($params){

        if(!empty($params)){
            $timeslot = new Timeslot();
            $timeslot->setId($params['idTimeslot']);
            $timeslot->delete();

            $msg = array("status"=> 'success', "message"=>'Horaire supprimée');
        }
        else{
            $msg = array("status"=> 'error',    "message"=>"Erreur");
        }

        header( "Content-Type: application/json" );
        echo json_encode( $msg );
    }

    function addtimeslotAction($params){

        if(!empty($params)){
            $dateParams = DateTime::createFromFormat('d/m/Y',$params["date"]);
            $date = $dateParams->format("Y-m-d");

            $timeslot = new Timeslot();

            $timeslot->setHour($params["hours"]);
            $timeslot->setDay($date);
            $timeslot->setIdMonitor($params["id_monitor"]);
            $timeslot->setIdStudent($params["id_student"]);

            $timeslot->save();

            $msg = array("status"=> 'success', "message"=>'Horaire ajoutée');
        }
        else{
            $msg = array("status"=> 'error',    "message"=>"Erreur");
        }

        header( "Content-Type: application/json" );
        echo json_encode( $msg );
    }

}