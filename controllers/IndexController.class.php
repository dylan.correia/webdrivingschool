<?php

class IndexController {
	
    /**
     * Traitement et affichage de la page d'accueil
     *
     * @return void
     */

	public function indexAction($errors = null) {

		$bd = new BaseSql();

		// On recherche tout le contenu dans le page d'accueil
		$contents = $bd->query('SELECT c.key,c.value,s.id as idSection FROM section s, content c WHERE id_page=:idPage AND s.id=c.id_section',['idPage'=>1]);

        $pages = Page::all(true,'position ASC'); // On récupère toutes pages visible depuis le frontoffice et trié selon l'ordre d'affichage
        $theme = $bd->query('SELECT name FROM theme WHERE choosen = 1 ',null)[0]['name']; // On récupère le thème choisi

        // On récupère les 3 derniers commentaires validés
		$comments = $bd->query('SELECT f.description,f.date_inserted ,u.lastname , u.firstname,u.picture ,f.date_inserted FROM feedback f,user u  WHERE f.id_student = u.id AND  f.status =:status ORDER BY f.date_inserted DESC LIMIT 3',['status' => 'VALID']);

		$v = new View("homeFront","frontoffice");
		$v->assign('pages',$pages);
		$v->assign('errors',$errors);
		$v->assign('comments',$comments);
		$v->assign('active',1);
		$v->assign('theme',$theme);
		$v->assign('title','ACCUEIL');

		// On assigne à la vue tout le contenu
		foreach ($contents as $content) {
			$v->assign($content['key'],$content['value']);
		}

		// On récupère toutes les sections de la vue
		$sections = $bd->query('SELECT title,hidden FROM section WHERE id_page= :idPage',['idPage'=>1]);

		// On assigne toutes à la vue toutes les sections
		foreach ($sections as $section) {
			$v->assign($section['title'].'Hidden',$section['hidden']);
		}
	}

    /**
     * Affichage de la page de maintenance
     *
     * @return void
     */
	public function maintenanceAction() {
        $v = new View("default","front");
    }

    /**
     * Affichage de la page d'erreur
     *
     * @return void
     */
    public function notFoundAction(){
    	header("HTTP/1.0 404 Not Found"); // On rajoute l'en-tête HTTP 404
    	$v = new View("notfound","front");
    }

    /**
     * Traitement et affichage de la page souhaité
     *
     * @param  array  $params [
     *                           string pageName
     *                   ]
     * @return void
     */
    public function getFrontPageAction($params){

    	$bd = new BaseSql();
    	$page = $bd->query('SELECT * FROM page where slug =:pageName',$params,'Page'); // On récupère la page demandée

         if(($page && !$page[0]->getHidden() )|| ($page && $page[0]->getId() == 5)){
    		$page = $page[0];

    		//Page d'accueil
    		if($page->getId() == 1){
    			header('Location: '.DIRNAME);
	            die();
    		}  
    		
            //Page des offres
    		else if($page->getId() == 2){

                    $packs = Pack::all();
    			$v = new View("packsFront","frontoffice");
    			if($packs){
                    $v->assign('packs',$packs);
                } else {
                    $v->assign('noPacks',true);

                }
    		}

    		//Page des commentaires
    		else if($page->getId() == 3){
    			$v = new View("commentsFront","frontoffice");
    			$nbComments = $bd->query('SELECT count(*) as nbComments FROM feedback f ,user u WHERE  f.status =:status AND u.id = f.id_student ',['status' => 'VALID']);
    			$commentsPerPage=10;
    			$nbComments = $nbComments[0]['nbComments'];
    			
    			if($nbComments != 0){
                    $nbPages = ceil($nbComments/$commentsPerPage);
                    $tmpComments = $bd->query('SELECT f.description,f.date_inserted,u.firstname,u.lastname,u.picture FROM feedback f ,user u WHERE  f.status =:status AND u.id = f.id_student'.' ORDER BY f.id DESC LIMIT '.$commentsPerPage,['status' => 'VALID']);
                    $comments = [];

                    // On crée le tableau de commentaire
                    foreach ($tmpComments as $comment) {
                        $tmp['description'] = $comment['description'];
                        $tmp['firstname'] = $comment['firstname'];
                        $tmp['lastname'] = $comment['lastname'];
                        $tmp['picture'] = $comment['picture'];
                        $date = new DateTime($comment['date_inserted']);
                        $tmp['date_inserted'] = $date->format('d/m/Y');
                        $comments[]=$tmp;
                    }
                    $v->assign('comments',$comments);
                    $v->assign('nbPages',$nbPages);
                    $v->assign('commentsPerPage',$commentsPerPage);
                } else {
                    $v->assign('noComments',true);
                }

            }

    		//Page de contact
    		else if($page->getId() == 4){
    			$v = new View("contactFront","frontoffice");
    			$config = $page->configFormContact();
    			$v->assign('config',$config);

    		}

    		// Page créée depuis le backoffice
    		else {
                //On récupère les contenus
				$content = $bd->query('SELECT c.value FROM section s, content c WHERE id_page=:idPage AND s.id=c.id_section',['idPage'=>$page->getId()])[0];
			
				$v = new View("pageFront","frontoffice");
				$v->assign('content',$content['value']);
    		}
				$pages = Page::all(true,'position ASC');  // On récupère toutes pages visible depuis le frontoffice et trié selon l'ordre d'affichage
    		    $bannerLogo = $bd->query('SELECT content.value FROM content WHERE content.key = "bannerLogo"',null)[0]['value'];

            $theme = $bd->query('SELECT name FROM theme WHERE choosen = 1 ',null)[0]['name']; // On récupère le thème

            $v->assign('pages',$pages);
            $v->assign('active',$page->getId());
            $v->assign('theme',$theme);
            $v->assign('bannerLogo',$bannerLogo);
            $v->assign('title',$page->getTitle());
    	} else {
    		$this->notFoundAction();
    	}
    }

    /**
     * Traitement et affichage des commentaires
     *
     * @param  array  $params [
     *                           int pageNumber
     *                   ]
     * @return void
     */
    public  function commentsAction($params){
    	$bd = new BaseSql();
    	$offset = ($params['pageNumber']-1)*10;
 
    	$tmpComments = $bd->query('SELECT f.description,f.date_inserted,u.firstname,u.lastname,u.picture FROM feedback f ,user u WHERE  f.status =:status AND u.id = f.id_student'.' ORDER BY f.id DESC LIMIT 10 OFFSET '.$offset,['status' => 'VALID']);

        $comments = [];

        // On crée le tableau de commentaire
        foreach ($tmpComments as $comment) {
            $tmp['description'] = $comment['description'];
            $tmp['firstname'] = $comment['firstname'];
            $tmp['lastname'] = $comment['lastname'];
            $tmp['picture'] = $comment['picture'];
            $date = new DateTime($comment['date_inserted']);
            $tmp['date_inserted'] = $date->format('d/m/Y');
            $comments[]=$tmp;
        }
        print_r(json_encode($comments)) ;
    }
    /**
     * Envoie de mails
     *
     * @param  array
     * @return void
     */
    public  function sendMessageAction($params){

        $objet = utf8_decode('CONTACT FORMULAIRE');

        //concatene le nouveau mdp avec le corps du mail
        function requireToVar($file, $params)
        {
            ob_start();
            require($file);
            return ob_get_clean();
        }
        $corps = requireToVar('./resource/mail/contactMail.php', $params);
        $mail = new Mailer();
        $mail->mailSend(MAILFROMLOGIN, 'Contact WDS', MAILFROMPASSWORD, MAILFROMLOGIN, $objet, $corps);
        $msg = ["message" =>"Message evoyé" ];
        print_r(json_encode($msg)) ;
    }

}
