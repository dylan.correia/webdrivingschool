<?php

/**
* 
*/

class PageController
{
	function indexAction()
	{
	    $pages =  Page::all();
		$v = new View("pages","backoffice_menu");
		$v->assign('pages',$pages);
        $v->assign('menuPage','pages');
	}

    function showAction($params)
    {

        $page = Page::findById($params['idPage']);
      
        if(!$page){
            header('Location: '.DIRNAME.'pages');
            die();
        } else {
            $bd = new BaseSql();

            $result = $bd->query('SELECT COUNT(*) as nbPages FROM page ',null)[0];
            $nbPages = [];
            for($i=1;$i<=$result['nbPages']+1;$i++){
                $nbPages[] = ['value' =>$i , 'label' => $i ];
            }

            if($params['idPage']==1){
                $values = [];
                $keys =[];
                $queryParams =[];

                $values['pageTitle'] = $page->getTitle();
                $sections= $bd->query('SELECT * FROM section s WHERE id_page =:idPage ',['idPage' => $params['idPage']],'Section');

                
                foreach ($sections as $section) {
                    $values[$section->getTitle()] = $section->getHidden();
                    $keys[] = 'id_section = :'.$section->getTitle(); 
                    $queryParams[$section->getTitle()] = $section->getId(); 
                }
                $contents= $bd->query('SELECT c.key,c.value FROM content c WHERE '.implode(' OR ',$keys) ,$queryParams,'Content');

                foreach ($contents as $content) {
                    $values[$content->getKey()]=$content->getValue();
                }

                $config = $page->configFormHomeModification();
                $errors = [];
                $v = new View("pageModificationForm","backoffice_menu");
                $v->assign('page',$page);
                $v->assign('values',$values);
                $v->assign('menuPage','pages');
                $v->assign('config',$config);
                $v->assign('errors',$errors);
                $v->assign('form','homeForm');

            } else if($params['idPage']== 2 ) {

                $values = [
                            'pageTitle'=> $page->getTitle(),
                            'pageHidden'=>$page->getHidden(),
                            'pagePosition' => $page->getPosition()
                ];


                $errors = [];
                $config = $page->configFormPage($nbPages);
                $packs = Pack::all();
                $config['config']['action']= DIRNAME."pages/".$params['idPage'];

                $pack =  new Pack();
                $configMdl = $pack->configFormPack();

                $v = new View("packs","backoffice_menu");
                $v->assign('packs',$packs);
                $v->assign('config',$config);
                $v->assign('configMdl',$configMdl);
                $v->assign('values',$values);
                $v->assign('errors',$errors);
                $v->assign('page',$page);
                $v->assign('form','pageFormAdd');
                $v->assign('formMdl','packForm');

                $v->assign('menuPage','pages');
            }

            else if($params['idPage']==3 || $params['idPage']==4){

                $values = [
                    'idPage' => $params['idPage'],
                    'pageTitle'=> $page->getTitle(),
                    'pageHidden'=>$page->getHidden(),
                    'pagePosition' => $page->getPosition()
                ];

                $config = $page->configFormPage($nbPages);
                $config['config']['action']= DIRNAME."pages/".$values['idPage'];

                $v = new View("pageModificationForm","backoffice_menu");

                $errors = [];

                $v->assign('menuPage','pages');
                $v->assign('config',$config);
                $v->assign('errors',$errors);
                $v->assign('form','pageForm');
                $v->assign('values',$values);

            } else if($params['idPage']== 5){

                $v = new View("pageModificationForm","backoffice_menu");
                $content = $bd->query('SELECT c.id,c.key,c.value FROM section s ,content c WHERE s.id_page = :idPage AND s.id = c.id_section',$params,'Content')[0];

                $values = [
                    'idPage' => $params['idPage'],
                    'pageTitle'=> $page->getTitle(),
                    'contentPage'=> $content->getValue()
                ];
                $config = $page->configFormMentionslegales($nbPages);
                $errors = null;
                $config['config']['action']= DIRNAME."pages/".$params['idPage'];
                $config['config']['method']= 'PUT';
                $config['config']['type']= 'button';

                $v->assign('menuPage','pages');
                $v->assign('config',$config);
                $v->assign('errors',$errors);
                $v->assign('form','pageFormAdd');
                $v->assign('values',$values);
            }else {

                $content = $bd->query('SELECT c.id,c.key,c.value FROM section s ,content c WHERE s.id_page = :idPage AND s.id = c.id_section',$params,'Content')[0];

                $values = [
                            'pageTitle'=> $page->getTitle(),
                            'pageHidden'=>$page->getHidden(),
                            'contentPage'=> $content->getValue(),
                            'pagePosition' => $page->getPosition()
                ];

                $v = new View("pageModificationForm","backoffice_menu");

                $errors = [];
                $config = $page->configFormPageAdd($nbPages);
                $config['config']['action']= DIRNAME."pages/".$params['idPage'];
                $config['config']['method']= 'PUT';
                $config['config']['type']= 'button';
                $v->assign('menuPage','pages');
                $v->assign('config',$config);
                $v->assign('errors',$errors);
                $v->assign('form','pageFormAdd');
                $v->assign('values',$values);
            }
        }
    }

    function addAction($errors =null)
    {
        $page =  new Page();
        $bd = new BaseSql();
        $result = $bd->query('SELECT COUNT(*) as nbPages FROM page ',null)[0];
        $nbPages = [];
        for($i=1;$i<=$result['nbPages']+1;$i++){
            $nbPages[] = ['value' =>$i , 'label' => $i ];
        }

        $config = $page->configFormPageAdd($nbPages);
        $v = new View("pageAdd","backoffice_menu");
        $v->assign('menuPage','pages');
        $v->assign('config',$config);
        $v->assign('errors',$errors);
        $v->assign('form','pageFormAdd');
        $v->assign('menuPage','pages');
    }

    function createAction($params)  
    {
        $page =  new Page();
        $config = $page->configFormPageAdd();
        $errors = Validate::checkForm($config, $params);

        $page->setTitle($params['pageTitle']);
        $page->setSlug($params['pageTitle']);
        $bd = new BaseSql();
        $result = $bd->query('SELECT * FROM page WHERE title=:title OR slug = :slug',['title'=>$page->getTitle() , 'slug'=> $page->getSlug()]);

        if(!empty($result)){
            $errors[] = "La page est déja éxistante, veuillez changer le titre";
        }

        $nbPages = $bd->query('SELECT COUNT(*) as nbPages FROM page ',null)[0];

        if($nbPages['nbPages']+1 < $params['pagePostion'] || $params['pagePostion'] < 0){
            $errors[] = "L'ordre d'affichage de la page est incorrecte";
        }

        if(!$errors) {

            $hidden = isset($params["pageHidden"])?"1":"0";

            $page->setPosition($params['pagePosition']);
            $page->setHidden($hidden);
            $page->setStatus('DELETABLE');
            $page->save();

            $page = $bd->query('SELECT id FROM page WHERE slug = :slug',['slug'=> $page->getSlug()],'Page')[0];

            $section =  new Section();
            $section->setTitle('templatePage');
            $section->setHidden('0');
            $section->setIdPage($page->getId());
            $section->save();
            $section = $bd->query('SELECT id FROM section WHERE title = :title AND id_page = :idPage',['title'=> $section->getTitle(),'idPage'=> $page->getId()],'Section')[0];

            $content = new Content();
            $content->setKey('tinymce');
            $content->setValue($params["contentPage"]);
            $content->setidSection($section->getId());
            $content->save();
            $this->generateSitemap();
            header('Location: '.DIRNAME.'pages');
            die();

        } else {
            $this->addAction($errors);
        }

    }

    function updateAction($params){

        if($params['idPage'] ==1){
            $this->updateHome($params);
        } 
        else if($params['idPage'] == 2 || $params['idPage'] == 3 || $params['idPage'] == 4) { 
            $page = new Page();
            $config = $page->configFormPage();
            $queryParams['pageTitle'] =$params['pageDscp']['page']['pageTitle'];
            $page = new Page();
            $errors = Validate::checkForm($config, $queryParams);

            if(!empty($errors)){
                $msg = array("status"=> 'error',    "message"=>$errors);
                print_r(json_encode($msg)) ;
            } else {

                $page->setId($params['idPage']);
                $page->setTitle($params['pageDscp']['page']['pageTitle']);
                $page->setSlug($params['pageDscp']['page']['pageTitle']);
                $page->setHidden($params['pageDscp']['page']['pageHidden']);
                $page->setPosition($params['pageDscp']['page']['pagePosition']);

                $page->save();
                $this->generateSitemap();

                $msg = array("status"=> 'success', "message"=>'Page mise à jour .');
                print_r(json_encode($msg)) ;
            }
        } else if($params['idPage'] == 5){

            $page = new Page();
            $config = $page->configFormMentionslegales();
            $queryParams['pageTitle'] =$params['pageDscp']['page']['pageTitle'];
            $errors = Validate::checkForm($config, $queryParams);

            if(!empty($errors)){
                $msg = array("status"=> 'error',"message"=>$errors);
                print_r(json_encode($msg)) ;
            } else {

                $page->setId($params['idPage']);
                $page->setTitle($params['pageDscp']['page']['pageTitle']);

                $page->save();
                $this->generateSitemap();

                $bd = new BaseSql();

                $content = $bd->query('SELECT c.id,c.key,c.value FROM section s ,content c WHERE s.id_page = :idPage AND s.id = c.id_section',['idPage' =>$params['idPage']],'Content')[0];
                $value = isset($params['pageDscp']['content']['contentPage'])?$params['pageDscp']['content']['contentPage']:"";
                $content->setValue($value);
                $content->save();

                $msg = array("status"=> 'success', "message"=>'Page mise à jour .');
                print_r(json_encode($msg)) ;
            }
        }else {
            $page = new Page();
            $config = $page->configFormPageAdd();
            $queryParams['pageTitle'] =$params['pageDscp']['page']['pageTitle'];
            $page = new Page();
            $errors = Validate::checkForm($config, $queryParams);

            if(!empty($errors)){
                $msg = array("status"=> 'error',"message"=>$errors);
                print_r(json_encode($msg)) ;
            } else {

                $page->setId($params['idPage']);
                $page->setTitle($params['pageDscp']['page']['pageTitle']);
                $page->setSlug($params['pageDscp']['page']['pageTitle']);
                $page->setHidden($params['pageDscp']['page']['pageHidden']);
                $page->setPosition($params['pageDscp']['page']['pagePosition']);

                $page->save();
                $this->generateSitemap();
                $bd = new BaseSql();
                
                $content = $bd->query('SELECT c.id,c.key,c.value FROM section s ,content c WHERE s.id_page = :idPage AND s.id = c.id_section',['idPage' =>$params['idPage']],'Content')[0];
                $value = isset($params['pageDscp']['content']['contentPage'])?$params['pageDscp']['content']['contentPage']:"";
                $content->setValue($value);
                $content->save();

                $msg = array("status"=> 'success', "message"=>'Page mise à jour .');
                print_r(json_encode($msg)) ;
            }
        }
    }

    private function updateHome($params) {

        $queryParams = $params['pageDscp']['section'];
        $queryParams = array_merge($queryParams,$params['pageDscp']['content']);
        $queryParams['pageTitle'] =$params['pageDscp']['page']['pageTitle'];
        $page = new Page();

        $formConfig = $page->configFormHomeModification();
        $config = [];
        foreach ($formConfig['sections'] as $section) {
            $config= array_merge($config,$section['input']);
        }

        $config['input'] = $config;
        $errors = Validate::checkForm($config, $queryParams);

        if(empty($errors)){
            $page->setId($params['idPage']);
            $page->setTitle($params['pageDscp']['page']['pageTitle']);
            $page->setSlug($params['pageDscp']['page']['pageTitle']);
            $page->save();

            $query =  [];
            $bd = new BaseSql();

            if(isset($params['pageDscp']['files'])){
                $files= $params['pageDscp']['files'];
                foreach ($files as $name => $file){
                    $value = $this->upload_file($name,$file);
                    $query[] = "UPDATE content SET content.value = :".$name." WHERE content.key = '$name'";
                    $queryParams[$name] = $value;
                }
            }

            foreach ($params['pageDscp']['content'] as $key => $value) {
                $query[] = "UPDATE content SET content.value = :".$key." WHERE content.key = '$key'"; 
            }

            foreach ($params['pageDscp']['section'] as $key => $value) {
                $query[] = "UPDATE section SET section.hidden = :".$key." WHERE section.title = '$key'  AND id_page = :idPage"; 
            }
            $queryParams['idPage'] =$params['idPage'];

            $bd->query(implode(';',$query),$queryParams); 
            $msg = array("status"=> 'success', "message"=>'Page mise à jour .');
            print_r(json_encode($msg)) ;
        } else {
            $msg = array("status"=> 'error',"message"=>$errors);
            print_r(json_encode($msg)) ;
        }
    }

    function deleteAction($params){

        $page = Page::findById($params['idPage']);

        if($page) {
            if($page->getStatus()=='DELETABLE'){

                $page->delete();
                $bd = new BaseSql();
                $pages = $bd->query('SELECT id,title,status FROM page',null);
                $this->generateSitemap();

                $msg = array("status"=> 'success',"message"=>'Page supprimée',"pages"=>$pages);
                print_r(json_encode($msg)) ;

            } else {
                $msg = array("status"=> 'error',"message"=>'Page non supprimable');
                print_r(json_encode($msg)) ;
            }
        }
    }


    function upload_file($name,$file){
        $targetDir ='./resource/images/home/';
        $decoded_file = base64_decode($file['base64']);

        $imageType = [  "image/png" => "png",
                        "image/gif "=>  "gif",
                        "image/jpeg" => "jpg",
                        "image/svg+xml" => "svg"
        ];

        $extension =  $imageType[$file['type']] ;
        $file = $name.'.'. $extension;
        $filePtah = $targetDir . $file;
        file_put_contents($filePtah, $decoded_file);
        return $file;

    }

    public function generateSitemap(){
	    $xmlDoc = new DOMDocument('1.0', 'utf-8');
        $urlset = $xmlDoc->createElement('urlset');
        $urlset->setAttribute('xmlns', "http://www.google.com/schemas/sitemap/0.9");
        $urlset->setAttribute('xmlns:xsi', "http://www.w3.org/2001/XMLSchema-instance");
        $urlset->setAttribute('xsi:schemaLocation', "http://www.sitemaps.org/schemas/sitemap/0.9
        http://www.sitemaps.org/schemas/sitemap/0.9/sitemap.xsd");

        $pages = Page::all(true);

        foreach ($pages as $page){
            $url = $xmlDoc->createElement('url');

            if($page->getId()==1){
                $loc = $xmlDoc->createElement('loc', 'https://webdrivingschool.fr/');
            } else {
                $loc = $xmlDoc->createElement('loc', 'https://webdrivingschool.fr/'.$page->getSlug());
            }

            if($page->getId()==1){
                $priority = $xmlDoc->createElement('priority', "1");
            } else {
                $priority = $xmlDoc->createElement('priority', "0.8");
            }

            $lastmod = $xmlDoc->createElement('lastmod', date(DATE_ATOM));
            $url->appendChild($loc);
            $url->appendChild($priority);
            $url->appendChild($lastmod);
            $urlset->appendChild($url);
        }

        //Ajout des mentions légales
        $url = $xmlDoc->createElement('url');
        $loc = $xmlDoc->createElement('loc', 'https://webdrivingschool.fr/mententionslegales');
        $priority = $xmlDoc->createElement('priority', "0.5");
        $lastmod = $xmlDoc->createElement('lastmod', date(DATE_ATOM));

        $url->appendChild($loc);
        $url->appendChild($priority);
        $url->appendChild($lastmod);
        $urlset->appendChild($url);

        $xmlDoc->appendChild($urlset);
        $xmlDoc->save('sitemap.xml');

    }
}