<?php

class SettingController {

    /**
     * Affichage des paramètres
     *
     * @return void
     */
	public function indexAction() {
		$v = new View("settings","backoffice_menu");
		$setting =  new Setting();
		$config = $setting->configFormSetting();
		$settings = Setting::all();
		$values = [];
		foreach ($settings as $setting){
		    $values[$setting->getName()] =$setting->getValue();
        }

        $v->assign('config',$config);
        $v->assign('values',$values);
        $v->assign('errors',null);
        $v->assign('form','form');
		$v->assign('menuPage','settings');
	}

    /**
     * Mis à jour des paramètres
     *
     * @param  array  $params [
     *                           string sitename
     *                           string mailcompany
     *                           int phone
     *                           string mentions-legales
     *                   ]
     * @return JSON $msg
     */
	public function updateAction($params) {

        $setting =  new Setting();
        $bd = new BaseSql();

        $config = $setting->configFormSetting();
        $errors = Validate::checkForm($config, $params);
        if(empty($errors)){
            $query = [];
            // On crée les rêquetes multiple de mis à jour
            foreach ($params as $name => $value){
                $query[] = 'UPDATE setting SET setting.value = "'.$value .'" WHERE setting.name = "'.$name.'"';
            }
            $bd->query(implode(';',$query),$params);
            $msg = array("status"=> 'success', "message"=>'Paramètre mis à jour ');
        } else {
            $msg = array("status"=> 'error', "message"=>$errors);
        }
        print_r(json_encode($msg)) ;

    }

}