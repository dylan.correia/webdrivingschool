<?php

class FeedbackController {

    /**
     * Liste des commentaires en attente de validation
     *
     * @return void
     */

    function indexAction(){
        $v = new View("feedback","backoffice_menu");
        $bd = new BaseSql();
        $feedbacks = $bd->query('SELECT feedback.* , user.firstname , user.lastname FROM feedback, user WHERE feedback.status = "WAITING" AND user.id = feedback.id_student',null);

        $v->assign('feedbacks',$feedbacks);
        $v->assign('menuPage','feedback');
    }

    /**
     * Récupère un commentaire
     *
     * @param  array  $errors
     * @return void
     */

    public function addAction($errors = null) {
        $feedback = new Feedback();
        $v = new View("feedbackAdd","backoffice_menu");
        $v->assign('config',$feedback->configFormFeedback());
        $v->assign('errors',null);
        $v->assign('values',null);
        $v->assign('title','Ajout d\'un commentaire');
        $v->assign('form','form');
        $v->assign('menuPage','feedback');
        $v->assign('errors',$errors);
	}

    /**
     * Récupère un commentaire
     *
     * @param  array  $params [
     *                          int idFeedback
     *                          ]
     * @return JSON    $msg
     */

    public function showAction($params) {

        $bd = new BaseSql();

        $feedback = $bd->query('SELECT feedback.* , user.firstname , user.lastname FROM feedback, user WHERE feedback.id = :idFeedback AND user.id = feedback.id_student',$params);

        if($feedback){
            $msg = array("status"=> 'success',"feedback" =>$feedback[0]);
            print_r(json_encode($msg)) ;
        } else {
            $msg = array("status"=> 'error',"message"=>'Le commentaire n\'éxiste pas');
            print_r(json_encode($msg)) ;
        }
	}

    /**
     * Crée un commentaire
     *
     * @param  array  $params [
     *                          string description
     *                          ]
     * @return JSON    $msg
     */
	public function createAction($params) {

        if(!empty($params['description'])){
            $feedback = new Feedback();
            $feedback->setDescription(htmlspecialchars($params['description']));
            $feedback->setStatus('WAITING');
            $feedback->setIdStudent(Auth::id());
            $feedback->save();
            header('Location: '.DIRNAME.'myfeedback');
        } else {
            $this->addAction(['Votre commentaire ne peut pas être vide']);
        }

	}

    /**
     * Mets à jour un commentaire
     *
     * @param  array  $params [
     *                          int idFeedback
     *                          string status
     *                          ]
     * @return JSON    $msg
     */
	public function updateAction($params){
        $feedback  = Feedback::findById($params['idFeedback']);
        if($feedback){
            $feedback->setStatus($params['status']);
            $feedback->save();

            $bd = new BaseSql();
            $feedbacks = $bd->query('SELECT feedback.* , user.firstname , user.lastname FROM feedback, user WHERE feedback.status = "WAITING" AND user.id = feedback.id_student',null);

            $msg = array("status"=> 'success',"message" =>"Commentaire mis à jour","feedbacks"=>$feedbacks);
        } else {
            $msg = array("status"=> 'error',"message"=>'Le commentaire n\'éxiste pas');
        }
        print_r(json_encode($msg)) ;

    }

    /**
     * Supprimer un commentaire
     *
     * @param  array  $params [
     *                          int idFeedback
     *                          ]
     * @return JSON    $msg
     */
	public function deleteAction($params) {

        $feedback  = Feedback::findById($params['idFeedback']);
        if($feedback){
            $feedback->delete(true);

            $bd = new BaseSql();
            $feedbacks = $bd->query('SELECT feedback.id,feedback.description, feedback.status,feedback.date_inserted  FROM feedback WHERE feedback.status = "WAITING" OR feedback.status = "VALID" AND feedback.id_student = :id AND feedback.deleted = 0 ORDER BY feedback.date_inserted DESC',['id' => Auth::id()]);

            $msg = array("status"=> 'success',"message" =>"Commentaire supprimée","feedbacks"=>$feedbacks);
        } else {
            $msg = array("status"=> 'error',"message"=>'Le commentaire n\'éxiste pas');
        }
        print_r(json_encode($msg)) ;
	}

    /**
     * Liste des commentaires de l'utilisateur
     *
     * @return void
     */

    public  function myfeedbackAction(){
        $v = new View("myfeedback","backoffice_menu");
        $bd = new BaseSql();
        $feedbacks = $bd->query('SELECT feedback.id,feedback.description, feedback.status,feedback.date_inserted  FROM feedback WHERE feedback.status = "WAITING" OR feedback.status = "VALID" AND feedback.id_student = :id AND feedback.deleted = 0 ORDER BY feedback.date_inserted DESC',['id' => Auth::id()]);

        $v->assign('feedbacks',$feedbacks);
        $v->assign('menuPage','myfeedback');
    }
}