<?php

class Feedback extends BaseSql
{

    protected $id = null;
    protected $description;
    protected $status;
    protected $idStudent;
    protected $dateInserted;
    protected $deleted = 0;


    public function __construct()
    {
        parent::__construct();
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int
     */
    public function setId($id)
    {
        $this->id = $id;
    }


    /**
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }


    /**
     * @param string $description
     */
    public function setDescription($description)
    {
        $this->description = trim($description);
    }

    /**
     * @return int
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * @param int $status
     */
    public function setStatus($status)
    {
        $this->status = trim($status);
    }

    /**
     * @return int
     */
    public function getIdStudent()
    {
        return $this->idStudent;
    }

    /**
     * @param int $idStudent
     */
    public function setIdStudent($idStudent)
    {
        $this->idStudent = $idStudent;
    }

    /**
     * @return date
     */
    public function getDateInserted()
    {
        return $this->dateInserted;
    }

    /**
     * @param int $dateInserted
     */
    public function setDateInserted($dateInserted)
    {
        $this->dateInserted = $dateInserted;
    }

    /**
     * @return boolean
     */
    public function getDeleted()
    {
        return $this->deleted;
    }

    /**
     * @param boolean $deleted
     */
    public function setDeleted($deleted)
    {
        $this->deleted = $deleted;
    }

    public function configFormFeedback($values = null){
        return [
            "config"=>["method"=>"POST",  "action"=>DIRNAME."feedback/add", "value"=>"Envoyer","type" => "submit","back" =>DIRNAME.'myfeedback'],
            "input"=>[
                "description" => [
                    "type" =>"textarea",
                    "placeholder"=>"Inserez votre commentaire",
                    "cols"=>50,
                    "rows"=>15,
                    "label"=>"Description"
                ]
            ]
        ];

    }



}