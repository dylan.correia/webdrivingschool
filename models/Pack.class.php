<?php
/**
 * Created by PhpStorm.
 * User: dylan
 * Date: 09/05/2018
 * Time: 14:18
 */

class Pack extends BaseSql
{
    protected $id;
    protected $title;
    protected $description;
    protected $price;

    /**
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param integer $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * @param string $title
     */
    public function setTitle($title)
    {
        $this->title = $title;
    }

    /**
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @param string $description
     */
    public function setDescription($description)
    {
        $this->description = $description;
    }

    /**
     * @return string
     */
    public function getPrice()
    {
        return $this->price;
    }

    /**
     * @param string $price
     */
    public function setPrice($price)
    {
        $this->price = $price;
    }

    public function configFormPack(){

    return [
            "input"=>[
                            "title" =>[
                                        "type" =>"text",
                                        "maxString"=>100,
                                        "placeholder"=>"Titre du forfait",
                                        "required"=>true,
                                        "label"=>" Titre"
                            ],
                            "description" => [
                                        "type" =>"textarea",
                                        "label"=>"Cacher la page",
                                        "cols"=>50,
                                        "rows"=>10,
                                        "label"=>"Description"
                            ],
                            "price" => [
                                        "type" =>"text",
                                        "maxString"=>10,
                                        "placeholder"=>"Prix du forfait",
                                        "required"=>true,
                                        "label"=>" Prix"
                            ]
            ]
        ];
    }

}
