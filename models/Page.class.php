<?php

class Page extends BaseSql
{

    protected $id = null;
    protected $slug;
    protected $title;
    protected $status;
    protected $hidden;
    protected $position;
    private $dateInserted;
    private $dateUpdated;

    public function __construct()
    {
        parent::__construct();
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getUrlName()
    {
        return $this->urlName;
    }

    /**
     * @param string
     */
    public function setSlug($slug)
    {
        $this->slug = $this->generate_slug($slug);
    }

    /**
     * @param string
     */
    public function getSlug()
    {
        return $this->slug;
    }

    /**
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * @param string
     */
    public function setTitle($title)
    {
        $this->title = trim($title);
    }

    /**
     * @return string
     */
    public function getContent()
    {
        return $this->content;
    }

    /**
     * @param  string
     */
    public function setContent($content)
    {
        $this->content = trim($content);
    }

    /**
     * @return date
     */
    public function getDateInserted()
    {
        return $this->dateInserted;
    }

    /**
     * @param  date
     */
    public function setDateInserted($dateInserted)
    {
        $this->dateInserted = $dateInserted;
    }

    /**
     * @return date
     */
    public function getDateUpdated()
    {
        return $this->dateUpdated;
    }

    /**
     * @param  date
     */
    public function setDateUpdated($dateUpdated)
    {
        $this->dateUpdated = $dateUpdated;
    }

    /**
     * @return boolean
     */
    public function getHidden()
    {
        return $this->hidden;
    }

    /**
     * @param  boolean
     */
    public function setHidden($hidden)
    {
        $this->hidden = $hidden;
    }

    /**
     * @return string
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * @param  string
     */
    public function setStatus($status)
    {
        $this->status = $status;
    }

    /**
     * @return string
     */
    public function getPosition()
    {
        return $this->position;
    }

    /**
     * @param  string
     */
    public function setPosition($position)
    {
        $this->position = $position;
    }

    private function generate_slug($string) {
        $table = array(
                'Š'=>'S', 'š'=>'s', 'Đ'=>'Dj', 'đ'=>'dj', 'Ž'=>'Z', 'ž'=>'z', 'Č'=>'C', 'č'=>'c', 'Ć'=>'C', 'ć'=>'c',
                'À'=>'A', 'Á'=>'A', 'Â'=>'A', 'Ã'=>'A', 'Ä'=>'A', 'Å'=>'A', 'Æ'=>'A', 'Ç'=>'C', 'È'=>'E', 'É'=>'E',
                'Ê'=>'E', 'Ë'=>'E', 'Ì'=>'I', 'Í'=>'I', 'Î'=>'I', 'Ï'=>'I', 'Ñ'=>'N', 'Ò'=>'O', 'Ó'=>'O', 'Ô'=>'O',
                'Õ'=>'O', 'Ö'=>'O', 'Ø'=>'O', 'Ù'=>'U', 'Ú'=>'U', 'Û'=>'U', 'Ü'=>'U', 'Ý'=>'Y', 'Þ'=>'B', 'ß'=>'Ss',
                'à'=>'a', 'á'=>'a', 'â'=>'a', 'ã'=>'a', 'ä'=>'a', 'å'=>'a', 'æ'=>'a', 'ç'=>'c', 'è'=>'e', 'é'=>'e',
                'ê'=>'e', 'ë'=>'e', 'ì'=>'i', 'í'=>'i', 'î'=>'i', 'ï'=>'i', 'ð'=>'o', 'ñ'=>'n', 'ò'=>'o', 'ó'=>'o',
                'ô'=>'o', 'õ'=>'o', 'ö'=>'o', 'ø'=>'o', 'ù'=>'u', 'ú'=>'u', 'û'=>'u', 'ý'=>'y', 'ý'=>'y', 'þ'=>'b',
                'ÿ'=>'y', 'Ŕ'=>'R', 'ŕ'=>'r', '/' => '-', ' ' => '-'
        );

        // Supprime les espaces déupliqués
        $stripped = preg_replace(array('/\s{2,}/', '/[\t\n]/'), ' ', $string);

        return strtolower(strtr($string, $table));
    }

     public function configFormHomeModification(){

        $form = [];
        $form ['config']= ["method"=>"PUT", "action"=>DIRNAME."pages/1", "value"=>"Enregistrer","back" =>DIRNAME."pages"];

        $section ['page']= [
                                "title" => "Informations de la page",
                                "input" => [
                                    "pageTitle" =>[
                                                "type" =>"text",
                                                "maxString"=>100,
                                                "placeholder"=>"Titre de la page",
                                                "required"=>true,
                                                "label"=>" Titre"
                                    ]
                                ]
        ];
        $section ['banner']=[
                            "title" => "Bannière",
                            "input" =>[
                                "bannerLogo" => [
                                            "type" => "file",
                                            "label" =>"Logo"
                                ],
                                "bannerSlogan" => [
                                            "type"=>"text",
                                            "placeholder"=>"Votre slogan",
                                            "maxString"=>100,
                                            "label"=>"Votre slogan"
                                ],
                                "banner" => [
                                            "type" =>"checkbox",
                                            "label"=>"Cacher la section"
                                ]
                            ]
        ];
        $section ['description']=[
                                "title" => "Présentation",
                                "input" =>[
                                            "dscpTitle" =>[
                                                "type"=>"text",
                                                "placeholder"=>"Titre de la section",
                                                "maxString"=>100,
                                                "label"=>"Votre slogan"
                                            ],
                                            "dscpText" =>[
                                                "type"=>"textarea",
                                                "cols"=>50,
                                                "rows"=>10,
                                                "label"=>"Description"
                                            ],
                                            "dscpImage" => [
                                                "type" => "file",
                                                "label" =>"Logo"
                                            ],
                                            "description" => [
                                                    "type" =>"checkbox",
                                                    "label"=>"Cacher la section"
                                            ]
                                ]
        ];
        $section ['pack']=[
                                "title" => "Offres",
                                "input" => [

                                        "card1Title" =>[
                                                "type"=>"text",
                                                "placeholder"=>"Titre de l'offre",
                                                "maxString"=>20,
                                                "label"=>"Titre"
                                        ],
                                        "card1Dscp" =>[
                                                "type"=>"textarea",
                                                "placeholder"=>"Description l'offre'",
                                                "cols"=>25,
                                                "rows"=>5,
                                                "label"=>"Description"
                                        ],
                                        "card1Price" =>[
                                                "type"=>"text",
                                                "placeholder"=>"Prix de l'offre",
                                                "maxString"=>10,
                                                "label"=>"Prix"
                                        ],
                                        "card2Title" =>[
                                                "type"=>"text",
                                                "placeholder"=>"Titre de l'offre",
                                                "maxString"=>20,
                                                "label"=>"Titre"
                                        ],
                                        "card2Dscp" =>[
                                                "type"=>"textarea",
                                                "placeholder"=>"Description l'offre'",
                                                "cols"=>25,
                                                "rows"=>5,
                                                "label"=>"Description"
                                        ],
                                        "card2Price" =>[
                                                "type"=>"text",
                                                "placeholder"=>"Prix de l'offre",
                                                "maxString"=>10,
                                                "label"=>"Prix"
                                        ],
                                        "card3Title" =>[
                                                "type"=>"text",
                                                "placeholder"=>"Titre de l'offre",
                                                "maxString"=>20,
                                                "label"=>"Titre"
                                        ],
                                        "card3Dscp" =>[
                                                "type"=>"textarea",
                                                "placeholder"=>"Description l'offre'",
                                                "cols"=>25,
                                                "rows"=>5,
                                                "label"=>"Description"
                                        ],
                                        "card3Price" =>[
                                                "type"=>"text",
                                                "placeholder"=>"Prix de l'offre",
                                                "maxString"=>10,
                                                "label"=>"Prix"
                                        ],
                                    
                                    "pack" => [
                                            "type" =>"checkbox",
                                            "label"=>"Cacher la section"
                                    ]     
                                ]
                        
        ];
        $section ['comment']= [
                                "title" => "Commentaires",
                                "input" => [
                                    "commentTitle" =>[
                                                "type" =>"text",
                                                "maxString"=>100,
                                                "placeholder"=>"Titre de la section",
                                                "label"=>" Titre"
                                    ],
                                    "comment" => [
                                            "type" =>"checkbox",
                                            "label"=>"Cacher la section"
                                    ]
                                ]
        ];
        $section ['timeslot']=[
                                "title" => "Horaires",
                                "input" => [
                                    "tmsTitle" =>[
                                                "type" =>"text",
                                                "maxString"=>100,
                                                "placeholder"=> "Entrez un titre",
                                                "label" => "Titre"
                                    ],
                                    "tmsDscp" =>[
                                                "type" =>"text",
                                                "maxString"=>150,
                                                "placeholder"=> "Entrez une description",
                                                "label" => "Description"
                                    ],
                                    "tmsMon" =>[
                                                "type" =>"text",
                                                "maxString"=>50,
                                                "placeholder"=> "Entrez une description",
                                                "label" => "Lundi"
                                    ],
                                    "tmsTue" =>[
                                                "type" =>"text",
                                                "maxString"=>50,
                                                "placeholder"=> "Entrez une description",
                                                "label" => "Mardi"
                                    ],
                                    "tmsWed" =>[
                                                "type" =>"text",
                                                "maxString"=>50,
                                                "placeholder"=> "Entrez une description",
                                                "label" => "Mercredi"
                                    ],
                                    "tmsThu" =>[
                                                "type" =>"text",
                                                "maxString"=>50,
                                                 "placeholder"=> "Entrez une description",
                                                "label" => "Jeudi"
                                    ],
                                    "tmsFri" =>[
                                                "type" =>"text",
                                                "maxString"=>50,
                                                "placeholder"=> "Entrez une description",
                                                "label" => "Vendredi"
                                    ],
                                    "tmsSat" =>[
                                                "type" =>"text",
                                                "maxString"=>50,
                                                "placeholder"=> "Entrez une description",
                                                "label" => "Samedi"
                                    ],

                                    "tmsSun" =>[
                                                "type" =>"text",
                                                "maxString"=>50,
                                                "placeholder"=> "Entrez une description",
                                                "label" => "Dimanche"
                                    ],

                                    "timeslot" => [
                                                "type" =>"checkbox",
                                                "label"=>"Cacher la section"
                                    ]
                                ]
        ];

        $form['sections'] = $section;
        return $form ;
    }

    public function configFormPage($nbPages =null){
         return [
                    "config"=>["method"=>"PUT",  "value"=>"Enregistrer", "type" => "button","back" =>DIRNAME."pages"],
                    "title" => "Informations de la page",
                    "input"=>[
                                    "pageTitle" =>[
                                                "type" =>"text",
                                                "maxString"=>100,
                                                "placeholder"=>"Titre de la page",
                                                "required"=>true,
                                                "label"=>" Titre"
                                    ],
                                    "pagePosition" => [
                                        "type" =>"select",
                                        "label"=>"Ordre d'affichage",
                                        "values"=> $nbPages
                                    ],
                                    "pageHidden" => [
                                                "type" =>"checkbox",
                                                "label"=>"Cacher la page"
                                    ]
                    ]
                ];

    }

    public function configFormPageAdd($nbPages =null){
         return [
                    "config"=>["method"=>"POST",  "action"=>DIRNAME."pages/add", "value"=>"Enregistrer","back" =>DIRNAME."pages","type" => "submit"],
                    "title" => "Informations de la page",
                    "input"=>[
                                    "pageTitle" =>[
                                                "type" =>"text",
                                                "maxString"=>100,
                                                "placeholder"=>"Titre de la page",
                                                "required"=>true,
                                                "label"=>" Titre"
                                    ],
                                    "pagePosition" => [
                                        "type" =>"select",
                                        "label"=>"Ordre d'affichage",
                                        "values"=> $nbPages
                                    ],
                                    "pageHidden" => [
                                                "type" =>"checkbox",
                                                "label"=>"Cacher la page"
                                    ],

                                    "contentPage" => [
                                                "type" =>"textarea"
                                    ]
                    ]
                ];

    }

    public function configFormContact(){
        return [
            "config"=>["method"=>"POST",  "action"=>DIRNAME."contact/send", "value"=>"Envoyer","type" => "button", "back" =>DIRNAME],
            "input"=>[
                "firstname" =>[
                    "type" =>"text",
                    "maxString"=>20,
                    "placeholder"=>"Votre prénom",
                    "required"=>true,
                    "label"=>" Prénom"
                ],
                "lastname" =>[
                    "type" =>"text",
                    "maxString"=>20,
                    "placeholder"=>"Votre nom",
                    "required"=>true,
                    "label"=>" Nom"
                ],
                "emailUser" =>[
                    "type" =>"email",
                    "maxString"=>100,
                    "placeholder"=>"Votre adresse email",
                    "required"=>true,
                    "label"=>" Email"
                ],
                "content" => [
                    "type" =>"textarea",
                    "placeholder"=>"Description l'offre'",
                    "cols"=>50,
                    "rows"=>15,
                    "label"=>"Message"
                ]
            ]
        ];

    }

    public function configFormMentionslegales(){
        return [
            "config"=>["method"=>"PUT",  "action"=>DIRNAME."pages/add", "value"=>"Enregistrer","back" =>DIRNAME."pages","type" => "submit"],
            "title" => "Informations de la page",
            "input"=>[
                "pageTitle" =>[
                    "type" =>"text",
                    "maxString"=>100,
                    "placeholder"=>"Titre de la page",
                    "required"=>true,
                    "label"=>" Titre"
                ],
                "contentPage" => [
                    "type" =>"textarea"
                ]
            ]
        ];
    }


}