<?php

class Timeslot extends BaseSql
{

    protected $id = null;
    protected $hour;
    protected $day;
    protected $idMonitor;
    protected $idStudent;

    public function __construct()
    {
        parent::__construct();
    }

    /**
     * @return null
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return time
     */
    public function getHour()
    {
        return $this->hour;
    }

    /**
     * @param time $hour
     */
    public function setHour($hour)
    {
        $this->hour = $hour;
    }

    /**
     * @return date
     */
    public function getDay()
    {
        return $this->day;
    }

    /**
     * @param date $day
     */
    public function setDay($day)
    {
        $this->day = $day;
    }

    /**
     * @return int
     */
    public function getIdMonitor()
    {
        return $this->idMonitor;
    }

    /**
     * @param int $idMonitor
     */
    public function setIdMonitor($idMonitor)
    {
        $this->idMonitor = $idMonitor;
    }

    /**
     * @return int
     */
    public function getIdStudent()
    {
        return $this->idStudent;
    }

    /**
     * @param int $idStudent
     */
    public function setIdStudent($idStudent)
    {
        $this->idStudent = $idStudent;
    }


    /**
     * Récupèrer les data des timeslots avec le nom du moniteur et de l'élève
     */
    public static function dataTimeSlotUser(){
        $baseSql = new BaseSql();

        $queryString = "SELECT u1.lastname as lastnameStudent, u1.firstname as firstnameStudent, u2.lastname as lastnameMonitor, u2.firstname as firstnameMonitor, t.id, t.hour, t.day FROM user as u1, user as u2, timeslot as t WHERE u1.id = t.id_student and u2.id = t.id_monitor ORDER BY t.day";
        $object = $baseSql->query($queryString ,null, null);

        return $object;
    }


    public function configFormAdd($object = null){
        $baseSql = new BaseSql();
        $selectStudent = [];
        $selectMonitor = [];

        //Liste des élèves + moniteurs
        $queryString = "SELECT u.lastname, u.firstname, u.id, r.name FROM user as u, role as r WHERE u.id_role = r.id and u.hidden = '0' and (r.name = 'Eleve' OR r.name = 'Moniteur') ORDER BY lastname";
        $res = $baseSql->query($queryString ,null, null);

        foreach ($res as $key => $value) {
            if($value['name'] == 'Eleve'){
                $selectStudent['option'][$value['id']] = $value['lastname'] . " " . $value['firstname'];
            }
            elseif($value['name'] == 'Moniteur'){
                $selectMonitor['option'][$value['id']] = $value['lastname'] . " " . $value['firstname'];
            }
        }

        return [
                    "config"=>["method"=>"POST", "action"=>"", "value"=>"Proposer l'horaire", "type"=>"submit"],
                    "input"=>[

                        "hour"=>[
                                        "type"=>"select",
                                        "options"=>["7h00 - 8h00"=>"7h00 - 8h00", "7h00 - 9h00"=>"7h00 - 9h00",
                                        "8h00 - 9h00"=>"8h00 - 9h00", "8h00 - 10h00"=>"8h00 - 10h00",
                                        "9h00 - 10h00"=>"9h00 - 10h00", "9h00 - 11h00"=>"9h00 - 11h00",
                                        "10h00 - 11h00"=>"10h00 - 11h00", "10h00 - 12h00"=>"10h00 - 12h00",
                                        "11h00 - 12h00"=>"11h00 - 12h00", "11h00 - 13h00"=>"11h00 - 13h00",
                                        "12h00 - 13h00"=>"12h00 - 13h00", "12h00 - 14h00"=>"12h00 - 14h00",
                                        "13h00 - 14h00"=>"13h00 - 14h00", "13h00 - 15h00"=>"13h00 - 15h00",
                                        "14h00 - 15h00"=>"14h00 - 15h00", "14h00 - 16h00"=>"14h00 - 16h00",
                                        "15h00 - 16h00"=>"15h00 - 16h00", "15h00 - 17h00"=>"15h00 - 17h00",
                                        "16h00 - 17h00"=>"16h00 - 17h00", "16h00 - 18h00"=>"16h00 - 18h00",
                                        "17h00 - 18h00"=>"17h00 - 18h00", "17h00 - 19h00"=>"17h00 - 19h00",
                                        "18h00 - 19h00"=>"18h00 - 19h00", "18h00 - 20h00"=>"18h00 - 20h00",
                                        "19h00 - 20h00"=>"19h00 - 20h00"
                                        ],
                                        "placeholder"=>"Horaire",
                                        "label"=>"Horaire",
                                        "required"=>true,
                                        "maxString"=>100,
                                        "minString"=>2,
                                        "value"=>isset($object)? $object->getHour() :""
                                    ],
                        "day"=>[
                                        "type"=>"date",
                                        "label"=>"Date",
                                        "placeholder"=>"jour",
                                        "required"=>true,
                                        "maxString"=>100,
                                        "minString"=>2,
                                        "value"=>isset($object)? $object->getDay() :""
                                    ]
                    ],
                    "selectStudent" => $selectStudent,
                    "selectMonitor" => $selectMonitor
        ];

    }

}