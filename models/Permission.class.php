<?php

class Permission extends BaseSql
{

    protected $id = null;
    protected $title;

    public function __construct()
    {
        parent::__construct();
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

   /**
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * @param string $route
     */
    public function setTitle($route)
    {
        $this->title = trim($route);
    }







}