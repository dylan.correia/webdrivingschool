<?php

class Section extends BaseSql {
    protected $id = null;
    protected $title;
    protected $hidden;
    protected $idPage;

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $idSection
     */
    public function setId($id)
    {
        $this->idSection = $id;
    }

    /**
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * @param string $title
     */
    public function setTitle($title)
    {
        $this->title = $title;
    }

    /**
     * @return boolean
     */
    public function getHidden()
    {
        return $this->hidden;
    }

    /**
     * @param boolean $hidden
     */
    public function setHidden($hidden)
    {
        $this->hidden = $hidden;
    }

    /**
     * @return int
     */
    public function getIdPage()
    {
        return $this->idPage;
    }

    /**
     * @param int $idPage
     */
    public function setIdPage($idPage)
    {
        $this->idPage = $idPage;
    }
}
