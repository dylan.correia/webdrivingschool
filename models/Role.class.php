<?php

class Role extends BaseSql
{

    protected $id = null;
    protected $description;
    protected $name;

    public function __construct()
    {
        parent::__construct();
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @param string $description
     */
    public function setDescription($description)
    {
        $this->description = trim($description);
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName($name)
    {
        $this->name = trim($name);
    }

    public function configFormRole(){
        return [
            "config"=>["method"=>"POST",  "value"=>"Enregistrer","action"=>DIRNAME."roles/add", "type" => "submit"],
            "input"=>[
                "roleName" =>[
                    "type" =>"text",
                    "maxString"=>100,
                    "placeholder"=>"Nom du rôle",
                    "required"=>true,
                    "label"=>" Nom"
                ],
                "roleDescription" => [
                    "type"=>"textarea",
                    "cols"=>50,
                    "rows"=>10,
                    "label"=>"Description"
                ]
            ]
        ];

    }





}