<?php

class Planning extends BaseSql{
	
	private $year = null;
    private $week = null;

	public function __construct($year = null, $week = null){
        $this->year = ($year == null)? date("Y") : $year;
        $this->week = ($week == null)? date("W") : $week;
	}

    /**
     * @return int
     */
    public function getYear()
    {
        return $this->year;
    }

    /**
     * @return int
     */
    public function getWeek()
    {
        return $this->week;
    }

    /**
     * @param int $year
     */
    public function setYear($year)
    {
        $this->year = $year;
    }

    /**
     * @param int $week
     */
    public function setWeek($week)
    {
        $this->week = $week;
    }

    /*
    * Méthode qui retourne le planning
    */
    public function draw($id_student = null, $id_monitor = null){
        $days = ['Lundi', 'Mardi', 'Mercredi', 'Jeudi', 'Vendredi', 'Samedi', 'Dimanche'];
        $hours = ['6h00', '7h00', '8h00', '9h00', '10h00', '11h00', '12h00', '13h00', '14h00', '15h00', '16h00', '17h00', '18h00', '19h00', '20h00', '21h00', '22h00', '23h00'];

        //Initialisation 
        for ($i=0; $i < count($hours); $i++) { 
            for ($j=0; $j < count($days); $j++) { 
                $drawPlanning[$hours[$i]][$days[$j]] = [];
            }
        }

        //Récupèration des données nécessaire
        $data = json_decode($this->popular($id_student, $id_monitor));

        //Attribution des data dans le planning
        foreach ($data->data as $key => $value) {
            $date = new DateTime($value->day);
            $day = $days[$date->format('N')-1];

            $timeSlotExploded = explode('-', $value->hour);
            $start = false;
            for ($i=0; $i < count($hours)-1; $i++) {
                if(trim($timeSlotExploded[0]) == $hours[$i]){
                    $start = true;
                }
                if($start){
                    $drawPlanning[$hours[$i]][$day][] = $value;
                }
                if(trim($timeSlotExploded[1]) == $hours[$i+1]){
                    break;
                }
            }
        }

        $tab['days'] = $days;
        $tab['data'] = $data;
        $tab['drawPlanning'] = $drawPlanning;

        return $tab;
    }

    /*
    * Méthode qui retourne la semaine avec les data
    */
    public function popular($id_student = null, $id_monitor = null){
        $strtotime = date($this->year . "-\W" . $this->week);
        $start = strtotime($strtotime);
        $end = strtotime("+6 days 23:59:59", $start);

        $obj = new stdclass();

        $obj->year = $this->year;
        $obj->start = date("Y-m-d", $start);
        $obj->today = date("Y-m-d");
        $obj->end = date("Y-m-d", $end);

        $obj->data = $this->getData($obj->start, $obj->end, $id_student, $id_monitor);

        return json_encode($obj);
    }

    /**
    * Méthode qui retourne les horaires selon les dates
    */
    public function getData($start, $end, $id_student = null, $id_monitor = null){
        $baseSql = new BaseSql();

        //GLOBAL
        if($id_student == null && $id_monitor == null){
            $queryString = "SELECT t.id, t.hour, t.day, u.lastname, u.firstname, t.id_monitor, u2.firstname as firstname_monitor, u2.lastname as lastname_monitor FROM timeslot as t, user as u, user as u2 WHERE u.id = t.id_student and u2.id = t.id_monitor and t.day BETWEEN '".$start."' and '".$end."' ORDER BY u2.lastname, u2.firstname, u.lastname, u.firstname";
        }  
        //DATA STUDENT
        elseif($id_student != null){
            $queryString = "SELECT t.id, t.hour, t.day, u2.lastname, u2.firstname FROM timeslot as t, user as u, user as u2 WHERE u.id = t.id_student and u2.id = t.id_monitor and t.day BETWEEN '".$start."' and '".$end."' and u.id='".$id_student."' ORDER BY u2.lastname, u2.firstname";
        }
        // DATA MONITOR
        else{
            $queryString = "SELECT t.id, t.hour, t.day, u.lastname, u.firstname FROM timeslot as t, user as u, user as u2 WHERE u.id = t.id_student and u2.id = t.id_monitor and t.day BETWEEN '".$start."' and '".$end."' and u2.id='".$id_monitor."' ORDER BY u.lastname, u.firstname";
        }

        $object = $baseSql->query($queryString ,null, null);

        return $object;
    }

    
}