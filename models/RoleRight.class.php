<?php

class RoleRight extends BaseSql
{

    protected $id = null;
    protected $idRole;
    protected $idRight;

    public function __construct()
    {
        parent::__construct();
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return int
     */
    public function getIdRole()
    {
        return $this->idRole;
    }

    /**
     * @param int $idRole
     */
    public function setIdRole($idRole)
    {
        $this->idRole = $idRole;
    }

   /**
     * @return int
     */
    public function getIdRight()
    {
        return $this->idRight;
    }

    /**
     * @param int $idRight
     */
    public function setIdRight($idRight)
    {
        $this->idRight = $idRight;
    }







}