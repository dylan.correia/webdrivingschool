<?php

class Progress extends BaseSql
{

    protected $id = null;
    protected $comment;
    protected $title;
    protected $dateInserted;
    protected $dateUpdated;
    protected $idTimeslot;
    protected $idMonitor;
    protected $studentShow;
    protected $hidden;

    public function __construct()
    {
        parent::__construct();
    }

    /**
     * @return null
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getComment()
    {
        return $this->comment;
    }

    /**
     * @param string $comment
     */
    public function setComment($comment)
    {
        $this->comment = trim($comment);
    }

    /**
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * @param string $title
     */
    public function setTitle($title)
    {
        $this->title = trim($title);
    }

    /**
     * @return timestamp
     */
    public function getDateInserted()
    {
        return $this->dateInserted;
    }

    /**
     * @param timestamp $title
     */
    public function setDateInserted($dateInserted)
    {
        $this->dateInserted = $dateInserted;
    }

    /**
     * @return timestamp
     */
    public function getDateUpdated()
    {
        return $this->dateUpdated;
    }

    /**
     * @param timestamp $title
     */
    public function setDateUpdated($dateUpdated)
    {
        $this->dateUpdated = $dateUpdated;
    }

    /**
     * @return int
     */
    public function getIdTimeslot()
    {
        return $this->idTimeslot;
    }

    /**
     * @param int $idTimeslot
     */
    public function setIdTimeslot($idTimeslot)
    {
        $this->idTimeslot = $idTimeslot;
    }

    /**
     * @return int
     */
    public function getIdMonitor()
    {
        return $this->idMonitor;
    }

    /**
     * @param int $idMonitor
     */
    public function setIdMonitor($idMonitor)
    {
        $this->idMonitor = $idMonitor;
    }

    /**
     * @return mixed
     */
    public function getStudentShow()
    {
        return $this->studentShow;
    }

    /**
     * @param mixed $studentShow
     */
    public function setStudentShow($studentShow)
    {
        $this->studentShow = $studentShow;
    }

    /**
     * @return mixed
     */
    public function getHidden()
    {
        return $this->hidden;
    }

    /**
     * @param mixed $hidden
     */
    public function setHidden($hidden)
    {
        $this->hidden = $hidden;
    }






    public function configFormProgress($selectTimeslot = null,$userId = null,$progressId = null,$update = false)
    {
        if ($update == true) {
            $method = 'PUT';
            $submit = 'button';
            $action = DIRNAME.'progress/'.$userId.'/update/'.$progressId;
            $value = 'MODIFIER';


        } else {
            $method = 'POST';
            $submit = 'submit';
            $action = DIRNAME.'progress/'.$userId.'/add';
            $value = 'AJOUTER';
        }

        return [
            "config" => ["method" => $method, "action" => $action, "type" => $submit, "value" => $value],
            "input" => [


                "title" => [
                    "type" => "text",
                    "placeholder" => "Insérer le titre",
                    "required" => true,
                    "maxString" => 100,
                    "minString" => 0,
                    "label" => "title"
                ],
                "comment" => [
                    "type" => "textarea",
                    "placeholder" => "Insérer l'observation",
                    "required" => true,
                    "label" => "comment",
                    "maxlength" => 500,
                    "rows" => 8,
                    "spellcheck" => true
                ],
                "timeslot" => [
                    "type" => "select",
                    "placeholder" => "Séléctionner une horaire",
                    "required" => true,
                    "label" => "Horaire",
                    "values" => $selectTimeslot
                ]
            ],
        ];

    }

}