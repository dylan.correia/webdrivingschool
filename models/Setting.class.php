<?php

class Setting extends BaseSql
{

    protected $id = null;
    protected $name;
    protected $value;

    public function __construct()
    {
        parent::__construct();
    }

    /**
     * @return null
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @return string
     */
    public function getValue()
    {
        return $this->value;
    }

    /**
     * @param string $value
     */
    public function setValue($value)
    {
        $this->value = $value;
    }


    public function configFormSetting(){
        return [
            "config"=>["method"=>"PUT",  "action"=>DIRNAME."settings", "value"=>"Enregistrer","type" => "button","back" =>DIRNAME.'dashboard'],
            "input"=>[
                "sitename" =>[
                    "type" =>"text",
                    "maxString"=>30,
                    "placeholder"=>"Nom du site",
                    "required"=>true,
                    "label"=>"Nom du site"
                ],
                "mailcompany" =>[
                    "type" =>"email",
                    "maxString"=>50,
                    "placeholder"=>"Email de l'école",
                    "required"=>true,
                    "label"=>"Email"
                ],
                "phone" =>[
                    "type" =>"number",
                    "maxString"=>10,
                    "placeholder"=>"N° de téléphone de l'école",
                    "required"=>true,
                    "label"=>"Telephone"
                ],
                "instagram" =>[
                    "type" =>"text",
                    "maxString"=>255,
                    "placeholder"=>"URL",
                    "label"=>"Instagram"
                ],
                "facebook" =>[
                    "type" =>"text",
                    "maxString"=>255,
                    "placeholder"=>"URL",
                    "label"=>"Facebook"
                ],
                "twitter" =>[
                    "type" =>"text",
                    "maxString"=>255,
                    "placeholder"=>"URL",
                    "label"=>"Twitter"
                ],
            ]
        ];

    }

}