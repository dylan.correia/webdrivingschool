<?php

class Theme extends BaseSql
{

    protected $id = null;
    protected $name;
    protected $description;
    protected $choosen;
    protected $image;

    public function __construct()
    {
        parent::__construct();
    }
    
    /**
     * @param int $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @param string $name
     */
    public function setName($name)
    {
        $this->name = ucfirst(trim($name));
    }

    /**
     * @param string $name
     */


    /**
     * @param string $description
     */
    public function setDescription($description)
    {
        $this->description = $description;
    }

    /**
     * @param boolean $choosen
     */
    public function setChoosen($choosen)
    {
        $this->choosen = $choosen;
    }

    /**
     * @param string $choosen
     */
    public function setImage($image)
    {
        $this->image = $image;
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @return string
     */
    public function getChoosen()
    {
        return $this->choosen;
    }

    /**
     * @return string
     */
    public function getImage()
    {
        return $this->image;
    }

}