<?php

class User extends BaseSql
{

    protected $id = null;
    protected $picture;
    protected $firstname;
    protected $lastname;
    protected $email;
    protected $password;
    protected $address;
    protected $city;
    protected $postcode;
    protected $token;
    protected $birthDate;
    protected $typeLicense;
    protected $hoursDrivingLicense;
    protected $lastConnection;
    protected $idRole;
    private $dateInserted;
    private $dateUpdated;
    protected $hidden = 0;


    public function __construct()
    {
        parent::__construct();
    }

    /**
     * @param int $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @param string $picture
     */
    public function setPicture($picture)
    {
        $this->picture = $picture;
    }

    /**
     * @param string $firstname
     */
    public function setFirstname($firstname)
    {
        $this->firstname = ucfirst(strtolower(trim($firstname)));
    }

    /**
     * @param string $lastname
     */
    public function setLastname($lastname)
    {
        $this->lastname = strtoupper(trim($lastname));
    }

    /**
     * @param string $email
     */
    public function setEmail($email)
    {
        $this->email = strtolower(trim($email));
    }

    /**
     * @param string $password
     */
    public function setPassword($password)
    {
        $this->password = password_hash($password, PASSWORD_DEFAULT);
    }

    /**
     * @param string $address
     */
    public function setAddress($address)
    {
        $this->address = trim($address);
    }

    /**
     * @return string $city
     */
    public function setCity($city)
    {
        $this->city = trim($city);
    }

    /**
     * @param int $postcode
     */
    public function setPostcode($postcode)
    {
        $this->postcode = $postcode;
    }

    /**
     * @param string $token
     */
    public function setToken($token)
    {
        $this->token = $token;
    }

    /**
     * @param date $birthDate
     */
    public function setBirthDate($birthDate)
    {
        $this->birthDate = $birthDate;
    }

    /**
     * @param string $typeLicense
     */
    public function setTypeLicense($typeLicense)
    {
        $this->typeLicense = strtoupper(trim($typeLicense));
    }

    /**
     * @param string $hoursDrivingLicense
     */
    public function setHoursDrivingLicense($hoursDrivingLicense)
    {
        $this->hoursDrivingLicense = $hoursDrivingLicense;
    }

    /**
     * @return timestamp $lastConnection
     */
    public function setLastConnection($lastConnection)
    {
        $this->lastConnection = $lastConnection;
    }

    /**
     * @return int $idRole
     */
    public function setIdRole($idRole)
    {
        $this->idRole = $idRole;
    }

    /**
     * @return date $dateInserted
     */
    public function setDateInserted($dateInserted)
    {
        $this->dateInserted = $dateInserted;
    }

    /**
     * @return date $dateUpdated
     */
    public function setDateUpdated($dateUpdated)
    {
        $this->dateUpdated = $dateUpdated;
    }

    /**
     * @return tinyint $hidden
     */
    public function setHidden($hidden)
    {
        $this->hidden = $hidden;
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getPicture()
    {
        return $this->picture;
    }

    /**
     * @return string
     */
    public function getFirstname()
    {
        return $this->firstname;
    }

    /**
     * @return string
     */
    public function getLastname()
    {
        return $this->lastname;
    }

    /**
     * @return string
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * @return string
     */
    public function getPassword()
    {
        return $this->password;
    }

    /**
     * @return string
     */
    public function getAddress()
    {
        return $this->address;
    }

    /**
     * @return string
     */
    public function getCity()
    {
        return $this->city;
    }

    /**
     * @return int
     */
    public function getPostcode()
    {
        return $this->postcode;
    }

    /**
     * @return string
     */
    public function getToken()
    {
        return $this->token;
    }

    /**
     * @return date
     */
    public function getBirthDate()
    {
        return $this->birthDate;
    }

    /**
     * @return stirng
     */
    public function getTypeLicense()
    {
        return $this->typeLicense;
    }

    /**
     * @return string
     */
    public function getHoursDrivingLicense()
    {
        return $this->hoursDrivingLicense;
    }

    /**
     * @return timestamp
     */
    public function getLastConnection()
    {
        return $this->lastConnection;
    }

    /**
     * @return int
     */
    public function getIdRole()
    {
        return $this->idRole;
    }

    /**
     * @return date
     */
    public function getDateInserted()
    {
        return $this->dateInserted;
    }

    /**
     * @return date
     */
    public function getDateUpdated()
    {
        return $this->dateUpdated;
    }

    /**
     * @return hidden
     */
    public function getHidden()
    {
        return $this->hidden;
    }

    /**
     * Filtre sur les users par rapport aux roles
     */
    public static function filterRoleUsers($idRole = null)
    {
        $baseSql = new BaseSql();

        if ($idRole != "Tous") {
            $queryString = "SELECT user.id, user.lastname, user.firstname, user.picture, user.last_connection, role.name FROM user, role WHERE role.id = user.id_role and user.hidden='0' and role.id = :id";
            $queryParams = ['id' => $idRole];
            $object = $baseSql->query($queryString, $queryParams, null);
        } else {
            $queryString = "SELECT user.id, user.lastname, user.firstname, user.picture, user.last_connection, role.name FROM user, role WHERE role.id = user.id_role and user.hidden='0'";
            $object = $baseSql->query($queryString, null, null);
        }

        return $object;
    }

    /**
     * Filtre sur les users par rapport à des mots
     */
    public static function filterSearchUsers($string)
    {
        $baseSql = new BaseSql();

        $queryString = "SELECT user.id, user.lastname, user.firstname, user.picture, user.last_connection, role.name FROM user, role WHERE role.id = user.id_role and (user.lastname LIKE '%" . $string . "%' OR user.firstname LIKE '%" . $string . "%' OR role.name LIKE '%" . $string . "%') and user.hidden='0'";
        $object = $baseSql->query($queryString, null, null);

        return $object;
    }

    /**
     * Recherche les données d'un utilisateur avec le nom de son rôle
     */
    public static function dataUserRole($id)
    {
        $baseSql = new BaseSql();

        $queryString = "SELECT user.id, user.lastname, user.firstname, user.email, user.birth_date as birthDate, user.password, user.address, user.city, user.postcode, user.type_license as typeLicense, user.hours_driving_license as hoursDrivingLicense, user.last_connection, user.id_role, role.name FROM user, role WHERE role.id = user.id_role and user.id='" . $id . "'";
        $object = $baseSql->query($queryString, null, null);

        return $object;
    }

    /**
     * Recherche le nombre de user par rapport au role donnée
     */
    public static function numberUserRole($string){
        $baseSql = new BaseSql();

        $queryString = "SELECT count(*) as numberUser FROM user, role WHERE role.id = user.id_role and role.name='".$string."'";
        $object = $baseSql->query($queryString ,null, null);

        return $object;
    }

    /**
     * Formulaire pour un utilisateur
     */
    public function configForm($value = null, $modification = false, $profile = false)
    {
        $allRole = Role::all();
        $selectRole = [];
        $selectRole['name'] = "role";

        foreach ($allRole as $keyRole => $valueRole) {
            $selectRole['option'][$valueRole->getId()] = $valueRole->getName();
        }

        if ($value == null || !$modification) {
            $method = "POST";
            $submit = "Créer";
            $action = DIRNAME . 'users/add';
            if ($value != null) {
                $selectRole['selected'] = $value['role'];
            }
        } else {
            $method = "PUT";
            $submit = "Modifier";
            $action = DIRNAME . 'users/' . $value['id'];
            $selectRole['selected'] = $value['id_role'];
        }

        if($profile){
            return [
                "config" => ["method" => $method, "action" => $action, "submit" => $submit],
                "input" => [

                    "picture" => [
                        "type" => "file",
                        "label" => "Image de profile"
                    ],
                    "lastname" => [
                        "type" => "text",
                        "placeholder" => "Insérer le nom",
                        "required" => true,
                        "maxString" => 100,
                        "minString" => 2,
                        "value" => $value['lastname'],
                        "label" => "Nom"
                    ],
                    "firstname" => [
                        "type" => "text",
                        "placeholder" => "Insérer le prénom",
                        "required" => true,
                        "maxString" => 100,
                        "minString" => 2,
                        "value" => $value['firstname'],
                        "label" => "Prénom"
                    ],
                    "email" => [
                        "type" => "email",
                        "placeholder" => "Insérer le email",
                        "required" => true,
                        "value" => $value['email'],
                        "label" => "Email"
                    ],
                    "emailConfirm" => [
                        "type" => "email",
                        "placeholder" => "Confirmer votre email",
                        "required" => true,
                        "confirm" => "email",
                        "value" => $value['email'],
                        "label" => "Email de confirmation"
                    ],
                    "address" => [
                        "type" => "text",
                        "placeholder" => "Insérer l'adresse",
                        "required" => true,
                        "maxString" => 100,
                        "minString" => 2,
                        "value" => $value['address'],
                        "label" => "Adresse"
                    ],
                    "city" => [
                        "type" => "text",
                        "placeholder" => "Insérer la ville",
                        "required" => true,
                        "maxString" => 100,
                        "minString" => 1,
                        "value" => $value['city'],
                        "label" => "Ville"
                    ],
                    "postcode" => [
                        "type" => "number",
                        "placeholder" => "Insérer le code postal",
                        "required" => true,
                        "maxNum" => 100,
                        "minNum" => 5,
                        "value" => $value['postcode'],
                        "label" => "Code postale"
                    ],
                    "password" => [
                        "type" => "password",
                        "maxString" => 32,
                        "minString" => 6,
                        "placeholder" => "Insérer un nouveau mot de passe",
                        "value" => "",
                        "label" => "Mot de passe"
                    ],
                    "passwordConfirm" => [
                        "type" => "password",
                        "maxString" => 32,
                        "minString" => 6,
                        "confirm" => "password",
                        "placeholder" => "Confirmation du nouveau mot de passe",
                        "value" => "",
                        "label" => "Confirmation de mot de passe"
                    ],
                    "birthDate" => [
                        "type" => "date",
                        "required" => true,
                        "value" => $value['birthDate'],
                        "label" => "Date de naissance"
                    ],
                    "typeLicense" => [
                        "type" => "text",
                        "placeholder" => "Insérer le type de permis",
                        "maxString" => 100,
                        "minString" => 1,
                        "value" => $value['typeLicense'],
                        "label" => "Type de permis"
                    ],
                    "hoursDrivingLicense" => [
                        "type" => "text",
                        "placeholder" => "Insérer le nombre d'heures",
                        "maxString" => 100,
                        "minString" => 1,
                        "value" => $value['hoursDrivingLicense'],
                        "label" => "Nombre d'heures"
                    ]

                ],
                "select" => $selectRole
            ];
        }
        else{
            return [
                "config" => ["method" => $method, "action" => $action, "submit" => $submit],
                "input" => [

                    "picture" => [
                        "type" => "file",
                        "label" => "Image de profile"
                    ],
                    "lastname" => [
                        "type" => "text",
                        "placeholder" => "Insérer le nom",
                        "required" => true,
                        "maxString" => 100,
                        "minString" => 2,
                        "value" => $value['lastname'],
                        "label" => "Nom"
                    ],
                    "firstname" => [
                        "type" => "text",
                        "placeholder" => "Insérer le prénom",
                        "required" => true,
                        "maxString" => 100,
                        "minString" => 2,
                        "value" => $value['firstname'],
                        "label" => "Prénom"
                    ],
                    "email" => [
                        "type" => "email",
                        "placeholder" => "Insérer le email",
                        "required" => true,
                        "value" => $value['email'],
                        "label" => "Email"
                    ],
                    "emailConfirm" => [
                        "type" => "email",
                        "placeholder" => "Confirmer votre email",
                        "required" => true,
                        "confirm" => "email",
                        "value" => $value['email'],
                        "label" => "Email de confirmation"
                    ],
                    "address" => [
                        "type" => "text",
                        "placeholder" => "Insérer l'adresse",
                        "required" => true,
                        "maxString" => 100,
                        "minString" => 2,
                        "value" => $value['address'],
                        "label" => "Adresse"
                    ],
                    "city" => [
                        "type" => "text",
                        "placeholder" => "Insérer la ville",
                        "required" => true,
                        "maxString" => 100,
                        "minString" => 1,
                        "value" => $value['city'],
                        "label" => "Ville"
                    ],
                    "postcode" => [
                        "type" => "number",
                        "placeholder" => "Insérer le code postal",
                        "required" => true,
                        "maxNum" => 100,
                        "minNum" => 5,
                        "value" => $value['postcode'],
                        "label" => "Code postale"
                    ],
                    "birthDate" => [
                        "type" => "date",
                        "required" => true,
                        "value" => $value['birthDate'],
                        "label" => "Date de naissance"
                    ],
                    "typeLicense" => [
                        "type" => "text",
                        "placeholder" => "Insérer le type de permis",
                        "maxString" => 100,
                        "minString" => 1,
                        "value" => $value['typeLicense'],
                        "label" => "Type de permis"
                    ],
                    "hoursDrivingLicense" => [
                        "type" => "text",
                        "placeholder" => "Insérer le nombre d'heures",
                        "maxString" => 100,
                        "minString" => 1,
                        "value" => $value['hoursDrivingLicense'],
                        "label" => "Nombre d'heures"
                    ]

                ],
                "select" => $selectRole
            ];
        }
    }

    public function configFormLogin()
    {

        return [
            "config" => ["method" => "POST", "action" => "", "submit" => "Connexion"],
            "input" => [

                "email" => [
                    "type" => "email",
                    "placeholder" => "Votre email",
                    "required" => true],

                "pwd" => [
                    "type" => "password",
                    "placeholder" => "Votre mot de passe",
                    "required" => true]

            ]
        ];

    }

    public function configFormLostpwd()
    {

        return [
            "config" => ["method" => "POST", "action" => "", "submit" => "Envoyer"],
            "input" => [

                "email" => [
                    "type" => "email",
                    "placeholder" => "Votre email",
                    "required" => true]


            ]
        ];

    }

    public function configFormResetpwd()
    {

        return [
            "config" => ["method" => "POST", "action" => "", "submit" => "Envoyer"],
            "input" => [

                "token" => [
                    "type" => "token",
                    "placeholder" => "",
                    "required" => true]


            ]
        ];

    }

    public function generatePwd($length)
    {
        $key = null;
        while (!Validate::checkPwd($key)) {
            $key = null;
            $pool = array_merge(range(0, 9), range('a', 'z'), range('A', 'Z'));

            for ($i = 0; $i < $length; $i++) {
                $key .= $pool[mt_rand(0, count($pool) - 1)];
            }
        }
        return $key;

    }

    public function generateToken()
    {
        $token = uniqid("",true);
        return $token;
    }
}