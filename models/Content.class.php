<?php
/**
 * Created by PhpStorm.
 * User: dylan
 * Date: 24/04/2018
 * Time: 16:42
 */

class Content extends BaseSql
{

    protected $id = null;
    protected $key;
    protected $value;
    protected $idSection;

    public function __construct()
    {
        parent::__construct();
    }

    /**
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param integer $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getKey()
    {
        return $this->key;
    }

    /**
     * @param string $key
     */
    public function setKey($key)
    {
        $this->key = $key;
    }

    /**
     * @return string
     */
    public function getValue()
    {
        return $this->value;
    }

    /**
     * @param string $value
     */
    public function setValue($value)
    {
        $this->value = $value;
    }

    /**
     * @return integer
     */
    public function getIdSection()
    {
        return $this->idSection;
    }

    /**
     * @param integer $idSection
     */
    public function setIdSection($idSection)
    {
        $this->idSection = $idSection;
    }


}