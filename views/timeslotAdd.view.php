<link rel="stylesheet" type="text/css" href="<?php echo DIRCSS; ?>view_user.css">

<main role="main">

      <!-- Main jumbotron for a primary marketing message or call to action -->
      <div class="container">

        <section id='formAddUser'>
          <article class="col-md-12">
            <div id="title">
              <h2>Ajouter un horaire</h2>
            </div>          
          </article>

          <article>
            <?php $this->addModal("formTimeslot",$config, $errors);?>
          </article>
        </section>

      </div> <!-- /container -->

</main>