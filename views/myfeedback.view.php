<label class="titre2"> Ajouter un commenatire </label><a href="<?php echo DIRNAME ."feedback/add" ?>"><i class="icon ion-ios-plus-outline userAddIcon"></i></a>
<table>
    <tr>
        <th>Date</th>
        <th>commentaire</th>
    </tr>
    <?php foreach ($feedbacks as $feedback) : ?>
        <tr class="line">
            <td><?php echo date_format(new DateTime($feedback['date_inserted'] ),"Y/m/d H:i");?></td>
            <td><?php echo substr($feedback['description'],0,15) ?>...</td>
            <td>
                <div class="btn-group">
                    <button class="btn icon edit ion-android-send" data-id="<?php echo $feedback['id']  ?>"></button>
                </div>
            </td>
        </tr>
    <?php  endforeach; ?>
</table>

<div id="modal" class="modal hide">
    <div class="modal-content">
        <div class="modal-header">
            <i class="icon ion-ios-close"  onclick="closeModal()"></i>
            <h2>Commentaire</h2>
        </div>
        <div class="modal-body">
            <section class="comment-content">
                <p></p>
                <span></span>
            </section>
            <div class="btn-group">
                <button id="delete"  title="supprimer" class="btn-danger">SUPPRIMER</button>
            </div>
        </div>
    </div>
</div>

<script>
    $('.ion-android-send').on('click',function (){
        id = $(this).data('id');
        $.ajax({
            url      : "<?php echo DIRNAME ?>"+"feedback/"+id,
            type     : "GET",
            success  : function(response) {
                var response = JSON.parse(response);

                if(response.status =='error'){
                    Toast.show(response.message,'error');
                } else {
                    var feedback = response.feedback;
                    $('.comment-content').find('p').text(feedback.description);
                    $('.comment-content').find('span').text('Ecrit le '+feedback.date_inserted);
                    $('#modal').show();
                }
            },
            error    : function(response) {
                console.log('error')
            }
        });



    });

    $('#delete').on('click',function () {
        $.ajax({
            url      : "<?php echo DIRNAME ?>"+"feedback/delete/"+id,
            type     : "GET",
            success  : function(response) {
                var response = JSON.parse(response);

                var feedbacks  = response.feedbacks; // recharger la page avec
                closeModal();
            },
            error    : function(response) {
                console.log('error')
            }
        });
    });


        window.onclick = function(event) {
        var modal = document.getElementById('modal');
        if (event.target == modal) {
            modal.style.display = "none";
        }
    }
</script>