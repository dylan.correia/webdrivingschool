
<link rel="stylesheet" type="text/css" href="<?php echo DIRCSS; ?>view_user.css">

    <main>
	   <div class="container-fluid">

            <section class="users">
                
                <article class="titleUsers">
                        <div class="row">
                            <div class="title col-sm-3">
                                <h2>HORAIRE</h2>    
                            </div>

                            <div class="addUser col-sm-9">
                                <p> Ajouter une heure de conduite <a href="timeslot/add"><i class="icon ion-ios-plus-outline"></i></a></p>
                            </div>
                        </div>
                </article>

                <article class="categoryUsers">
                    <div class="row">
                        <div class="offset-sm-2"></div>
                        <div class="col-sm-2">
                            <p>Date</p>
                        </div>
                        <div class="col-sm-2">
                            <p>Créneau horaire</p>
                        </div>
                        <div class="col-sm-2">
                            <p>Élève</p>
                        </div>
                        <div class="col-sm-2">
                            <p>Moniteur</p>
                        </div>
                        <div class="offset-sm-2"></div>
                    </div>
                </article>

                <article class="dataUsers">
                    <?php
                        /*
                        * Instanciation de la liste des horaires, du nombre d'horaires par page et du tableau recueillant les buttons pour naviguer entre les pages
                        */
                        $numberPerPage = 9;
                        $buttonForPage = [];
                    ?>

                    <?php foreach ($horaires as $key => $objHoraires): ?>
                        <?php if($key == 0): ?>
                            <?php $buttonForPage[$key/$numberPerPage] = ($key/$numberPerPage)+1; ?>
                            <div id="pageUsers<?php echo ($key/$numberPerPage)+1; ?>" class="pageUsers">
                        <?php endif; ?>

                        <div class="row dataUsersInto">
                            <div class="offset-sm-2"></div>
                            <?php
                                $date = new DateTime($objHoraires['day']);
                            ?>
                            <div class="col-sm-2">
                                <p><?php echo $date->format('d/m/Y'); ?></p>
                            </div>
                            <div class="col-sm-2">
                                <p><?php echo $objHoraires['hour']; ?></p>
                            </div>
                            <div class="col-sm-2">
                                <p><?php echo $objHoraires['lastnameStudent'] . " " . $objHoraires['firstnameStudent']; ?></p>
                            </div>
                            <div class="col-sm-2">
                                <p><?php echo $objHoraires['lastnameMonitor'] . " " . $objHoraires['firstnameMonitor']; ?></p>
                            </div>

                            <?php
                            $dateTimeslot = new DateTime($objHoraires['day']);
                            $dateToday = new DateTime();
                            $dateToday->modify("+2 day");
                            ?>
    
                            <?php if($dateToday < $dateTimeslot): ?>
                            <div class="col-sm-2">
                                <div class="actionUsers">
                                    <a href="<?php echo DIRNAME ?>timeslot/delete/<?php echo $objHoraires['id']; ?>"><i id="deleteUser<?php echo $objHoraires['id']; ?>" class="deleteUser icon ion-ios-trash-outline"></i></a>
                                </div>
                            </div>
                            <?php else: ?>
                            <div class="col-sm-2">
                                <div class="actionUsers">
                                    
                                </div>
                            </div>
                            <?php endif; ?>
                            
            

                        </div>

                        <?php if($key != 0 && $key != sizeof($horaires)-1 && $key % $numberPerPage == 0): ?>
                            <?php $buttonForPage[$key/$numberPerPage] = ($key/$numberPerPage)+1; ?>
                            <?php $numberPerPage = 10; ?>
                            </div><div id="pageUsers<?php echo ($key/$numberPerPage)+1; ?>" class="pageUsers">
                        <?php elseif($key == sizeof($horaires)-1):?>
                            </div>
                        <?php endif; ?>

                    <?php endforeach; ?>

                    <div id="paginationUsers">
                    <?php
                        for ($i=0; $i < sizeof($buttonForPage); $i++) { 
                            echo '<button id="buttonPageUsers' . $buttonForPage[$i] . '" class="buttonPageUsers">' . $buttonForPage[$i] . '</button>';
                        }
                    ?>
                    </div>
                </article>

            </section>


</main>

<script type="text/javascript" src="<?php echo DIRJS; ?>view_user.js"></script>