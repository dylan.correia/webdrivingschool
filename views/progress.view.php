<link rel="stylesheet" type="text/css" href="<?php echo DIRCSS; ?>view_user.css">

<main>

	<!-- Use date in JavaScript -->
	<input type="hidden" id="dirName" value="<?php echo DIRNAME; ?>">
	<input type="hidden" id="dirImages" value="<?php echo DIRIMAGES; ?>">
	<input type="hidden" id="urlUser" value="<?php echo DIRNAME . "users"; ?>">

	<div class="container-fluid">
		<section class="users">

			<article class="titleUsers">
				<div class="row filter_title">
					<div class="title col-sm-3">
						<h2>PROGRESSIONS</h2>
					</div>
					<div class="col-sm-4">
						<div class="searchUser">
							<label for="search">Recherche :</label>
							<input name="search" id="search" type="text" maxlength="255" placeholder="Tapez ici votre recherche ..." /><i id="wen" class="icon ion-search"></i>
						</div>
					</div>
				</div>
			</article>

			<article class="categoryUsers">
				<div class="row">
					<div class="offset-sm-3"></div>
					<div class="col-sm-3">
						<p>Nom</p>
					</div>
					<div class="col-sm-3">
						<p>Prénom</p>
					</div>
					<div class="offset-sm-3"></div>
				</div>
			</article>

			<article class="dataUsers">
				<?php
					/*
					* Instanciation de la liste des utilisateurs, du nombre d'utilisateurs par page et du tableau recueillant les buttons pour naviguer entre les pages
					*/
					$numberPerPage = 9;
					$buttonForPage = [];
				?>

				<?php foreach ($allUser as $key => $objUser): ?>
					<?php if($key == 0): ?>
						<?php $buttonForPage[$key/$numberPerPage] = ($key/$numberPerPage)+1; ?>
						<div id="pageUsers<?php echo ($key/$numberPerPage)+1; ?>" class="pageUsers">
					<?php endif; ?>

					<div class="row dataUsersInto">
						<div class="col-sm-3">
							<div class="pictureUsers">
                                <?php if(is_file("resource/images/picture_users/" . $objUser->getPicture())): ?>

                                    <img src="<?php echo DIRIMAGES ?>picture_users/<?php echo $objUser->getPicture(); ?>">

                                <?php else: ?>

                                    <img src="<?php echo DIRIMAGES ?>picture_users/unknown.png">

                                <?php endif; ?>
							</div>
						</div>
						<div class="col-sm-3">
							<p><?php echo $objUser->getLastname(); ?></p>
						</div>
						<div class="col-sm-3">
							<p><?php echo $objUser->getFirstname(); ?></p>
						</div>
						<div class="col-sm-3">
							<div class="actionUsers">
								<a href="<?php echo DIRNAME ?>progress/<?php echo $objUser->getId(); ?>"><i class="showPlanning icon ion-ios-bookmarks-outline"></i></a><a href="<?php echo DIRNAME ."progress/".$objUser->getId()."/add"?>"><i class="modifyUser icon ion-ios-plus-outline"></i></a>
							</div>
						</div>
					</div>

					<?php if($key != 0 && $key != sizeof($allUser)-1 && $key % $numberPerPage == 0): ?>
						<?php $buttonForPage[$key/$numberPerPage] = ($key/$numberPerPage)+1; ?>
						<?php $numberPerPage = 10; ?>
						</div><div id="pageUsers<?php echo ($key/$numberPerPage)+1; ?>" class="pageUsers">
					<?php elseif($key == sizeof($allUser)-1):?>
						</div>
					<?php endif; ?>

				<?php endforeach; ?>

				<div id="paginationUsers">
				<?php
					for ($i=0; $i < sizeof($buttonForPage); $i++) { 
						echo '<button id="buttonPageUsers' . $buttonForPage[$i] . '" class="buttonPageUsers">' . $buttonForPage[$i] . '</button>';
					}
				?>
				</div>
			</article>

		</section>
	</div>

</main>

<script type="text/javascript" src="<?php echo DIRJS; ?>view_user.js"></script>