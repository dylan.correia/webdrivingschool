<link rel="stylesheet" type="text/css" href="<?php echo DIRCSS; ?>view_progress.css">
<main>
	<div class="container">
                    <!-- Pages -->
                    <div class="row">
                        <div class="col-sm-12">
                            <section id="progress" class="progress">
                                <div class="col-12 relative">
                                    <div class="userNav">
                                        <h1 id="page-title"><?php echo isset($myprogress)? "Mes progressions ": "Progressions ".$user->getFirstname()." ". $user->getLastname()?></h1>
                                        <div class="filters">
                                            <div class="userAdd">
                                                <?php echo isset($myprogress)? "" : "<a href=\"". DIRNAME ."progress/".$params['idUser']."/add\"><label class=\"titre2 addBtn\"> Ajouter une progression </label><i class=\"icon ion-ios-plus-outline userAddIcon\"></i></a>"?>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <ul class="offset-1 col-10 offset-1 list">
                                        <?php foreach ($progress as $progres) : ?>
                                        <li>
                                            <h2 >
                                                <?php $date=date_create($progres['day']); echo $progres['hour']." ".date_format($date,"d/m/Y"). " - " . $progres['firstname']." ".$progres['lastname'];?></h2>
                                            <p class="list-title"><?php echo $progres['title'] ?></p>
                                            <p class="texte3"><?php echo $progres['comment'] ?></p>
                                            <section class="panel">
                                                <?php echo isset($myprogress)? "" : "<a href=\"".DIRNAME ."progress/".$user->getId()."/update/".$progres['0']."\"><i class=\"ion-ios-compose-outline edit \"></i></a>" ;?>
                                                <?php echo isset($myprogress)? "" : "<a href=\"". DIRNAME ."progress/delete/".$progres['0']."\" class=\"btn btn-delete\"><i class=\"ion-ios-trash-outline delete \"></i></a>";?>
                                            </section>
                                        </li>
                                        <?php endforeach; ?>
                                    </ul>
                                </div>
                            </section>
                        </div>
                    </div>
                </div>
</main>
<script>
            $("#progress").on('click', '.btn-delete', function() {
                var id =  $(this).data('0');
                $.ajax({
                    url      : "*/<?php echo DIRNAME ?>/*"+"progress/delete/"+id,
                    type     : "GET",
                    success  : function(response) {
                        var response = JSON.parse(response);
                        var status = response.status;
                        var message = response.message;
                        Toast.show(message,status);
                        var progress  =  response.progress;
                        serialize(progress);
                    },
                    error    : function(response) {
                        console.log('error')
                    }
        })

    });

    function serialize(progress) {
        $('.list').html('');
        for (var i in progress) {
            var tab = '';
        
            tab+="<li>";
            tab+="<label class=\"list-title\">"+progress[i].title+"</label>";
            tab+="<section class=\"panel\">";
            tab+="<a href=\"<?php echo DIRNAME ?>"+"progress/"+progress[i].id+"\"";
            tab+="<i class=\"ion-ios-compose-outline edit \"></i></a>";
            if(progress[i].status == 'DELETABLE'){
                tab+="<button data-id=\""+progress[i]+"\"";
                tab+="class=\"btn btn-delete\"><i class=\"ion-ios-trash-outline delete \"></i></button>";
            }
            tab+="</section";
            tab+="</li>";
            $('.list').append(tab);
        }
    }
</script>