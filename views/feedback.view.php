
<main class="container">
    <section class="tab-list">
        <div class="tab-list-title">
            <h1>Commentaires</h1>
        </div>
        <div class="row column-entitled">
            <label class="col-sm-2">Nom</label>
            <label class="col-sm-2">Prénom</label>
            <label class="col-sm-2">Date</label>
            <label class="col-sm-2">Commentaire</label>
        </div>
        <?php foreach ($feedbacks as $feedback) : ?>
        <div class="row tab-list-row">
            <label class="col-sm-2"><?php echo $feedback['lastname'] ?></label>
            <label class="col-sm-2"><?php echo $feedback['firstname']  ?></label>
            <label class="col-sm-2"><?php echo date_format(new DateTime($feedback['date_inserted'] ),"Y/m/d H:i");?></label>
            <label class="col-sm-2"><?php echo substr($feedback['description'],0,15) ?>...</label>
            <div class="col-sm-2">

                <button class="btn icon edit ion-android-send" data-id="<?php echo $feedback['id']  ?>"></button>
            </div>
        </div>
        <?php endforeach; ?>
    </section>
</main>
<div id="modal" class="modal hide">
    <div class="modal-content">
        <div class="modal-header">
            <i class="icon ion-ios-close"  onclick="closeModal()"></i>
            <h2>Commentaire</h2>
        </div>
        <div class="modal-body">
            <section class="comment-content">
                <p></p>
                <span></span>
            </section>
            <div class="btn-group">
                <button title="supprimer" class="btn medium-icon  danger ion-android-close"></button>
                <button title="valider" class="btn medium-icon validate ion-android-checkmark-circle" ></button>
            </div>
        </div>
    </div>
</div>

<script>
    $('.container').on('click','.ion-android-send', function (){
        id = $(this).data('id');
        $.ajax({
            url      : "<?php echo DIRNAME ?>"+"feedback/"+id,
            type     : "GET",
            success  : function(response) {
                var response = JSON.parse(response);

                if(response.status =='error'){
                    Toast.show(response.message,'error');
                } else {
                    var feedback = response.feedback;
                    $('.comment-content').find('p').html(feedback.description);
                    $('.comment-content').find('span').text('Ecrit part '+feedback.lastname+' '+feedback.firstname+' le '+feedback.date_inserted);
                    $('#modal').show();
                }
            },
            error    : function(response) {
                console.log('error')
            }
        });



    });

    $('.danger').on('click',function () {
        $.ajax({
            url      : "<?php echo DIRNAME ?>"+"feedback/"+id,
            type     : "PUT",
            data     : {'status' : 'INVALID'},
            success  : function(response) {
                var response = JSON.parse(response);
                loadFeedbacks(response.feedbacks)
                closeModal();
                Toast.show(response.message,response.status)
            },
            error    : function(response) {
                console.log('error')
            }
        });
    });

    $('.validate').on('click',function () {
        $.ajax({
            url      : "<?php echo DIRNAME ?>"+"feedback/"+id,
            type     : "PUT",
            data     : {'status' : 'VALID'},
            success  : function(response) {
                var response = JSON.parse(response);
                // console.log(response)
                loadFeedbacks(response.feedbacks)
                closeModal();
                Toast.show(reponse.message,response.status)
            },
            error    : function(response) {
                console.log('error')
            }
        });

    });

    function loadFeedbacks(feedbacks){

        $('.tab-list-row').remove();
        var tab = '';
        for (var i in feedbacks) {

            tab += '<div class="row tab-list-row">';
            tab += '<label class="col-sm-2">'+feedbacks[i].lastname+'</label>';
            tab += '<label class="col-sm-2">'+feedbacks[i].firstname+'</label>';
            tab += '<label class="col-sm-2">'+feedbacks[i].date_inserted+'</label>';
            tab += '<label class="col-sm-2">'+feedbacks[i].description+'</label>';
            tab += '<div class="col-sm-2">';
            tab += '<button class="btn icon edit ion-android-send" data-id="'+feedbacks[i].id+'">';
            tab += '</div>';
            tab += '</div>';
        }
        $('.column-entitled').after(tab)
    }
    window.onclick = function(event) {
    var modal = document.getElementById('modal');
    if (event.target == modal) {
        modal.style.display = "none";
    }
    }
</script>