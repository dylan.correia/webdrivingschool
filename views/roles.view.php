<link rel="stylesheet" type="text/css" href="css/view_page.css">
<main>
	<div class="container">
        <div class="row">
            <div class="col-sm-12">
                <section id="pages">
                    <div class="userNav">
                        <h1 id="page-title">ROLES</h1>
                        <div class="filters">
                            <div class="userAdd">
                                <label class="titre2"> Ajouter un rôle </label><a href="<?php echo DIRNAME ."roles/add" ?>"><i class="icon ion-ios-plus-outline userAddIcon"></i></a>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <ul class="offset-1 col-10 offset-1 list">
                            <?php foreach ($roles as $role) : ?>
                            <li>
                                <label class="list-title"><?php echo $role->getName(); ?></label>
                                <section class="panel">
                                    <a href="<?php echo DIRNAME ."roles/".$role->getId();?>"><i class="ion-ios-compose-outline edit pulse"></i></a>
                                    <button data-id="<?php echo $role->getId(); ?>" class="btn btn-delete"><i class="ion-ios-trash-outline delete pulse"></i></button>
                                </section>
                            </li>
                            <?php endforeach; ?>
                        </ul>
                    </div>
                </section>
            </div>
        </div>
    </div>
</main>
<script>
    $(".container").on('click', '.btn-delete', function() {
        var id =  $(this).data('id');
        $.ajax({
            url      : "<?php echo DIRNAME ?>"+"roles/delete/"+id,
            type     : "GET",
            success  : function(response) {
                var response = JSON.parse(response);
                var status = response.status;
                var message = response.message;
                Toast.show(message,status);
                var roles  =  response.roles;
                if(status == 'success'){
                    loadRoles(roles);
                }
            },
            error    : function(response) {
                console.log('error')
            }
        })

    });

    function loadRoles(roles) {
        $('.list').html('');
        for (var i in roles) {
            var tab = '';
            tab+="<li>";
            tab+="<label class=\"list-title\">"+roles[i].name+"</label>";
            tab+="<section class=\"panel\">";
            tab+="<a href=\"<?php echo DIRNAME ?>"+"roles/"+roles[i].id+"\"";
            tab+="<i class=\"ion-ios-compose-outline edit pulse\"></i></a>";
            tab+="<button data-id=\""+roles[i].id+"\"";
            tab+="class=\"btn btn-delete\"><i class=\"ion-ios-trash-outline delete pulse\"></i></button>";
            tab+="</section";
            tab+="</li>";
            $('.list').append(tab);
        }
    }
</script>