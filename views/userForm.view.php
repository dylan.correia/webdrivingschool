<link rel="stylesheet" type="text/css" href="<?php echo DIRCSS; ?>view_user.css">

<main>

	<!-- Use date in JavaScript -->
	<?php if(isset($profile)): ?>
		<input type="hidden" id="urlUser" value="<?php echo DIRNAME . "planning"; ?>">
	<?php else: ?>
		<input type="hidden" id="urlUser" value="<?php echo DIRNAME . "users"; ?>">
	<?php endif; ?>

	<div class='container'>
		<section id='formAddUser'>
			<article id="titleUsers">
				<div id="title">
					<h2><?php echo $title; ?></h2>
				</div>
			</article>
			<article>
				<?php $this->addModal("formUser",$config, $errors);?>
			</article>
		</section>
	</div>

</main>

<script type="text/javascript" src="<?php echo DIRJS; ?>view_user.js"></script>