<main>
		<div >
			<?php if(!$descriptionHidden): ?>
			<div class="group-grey row">
				<div class="container">
					<h1 class="col-12"><?php echo $dscpTitle; ?></h1>
					<div class="row">
						<p class="col-md-6">
							<?php echo $dscpText; ?>
						</p>
						<img class="col-md-6" src="<?php echo DIRIMAGES."home/".$dscpImage ?>">
					</div>
				</div>
			</div>
			<?php endif; ?>
			<!-- les cards -->
			<div class="container">
				<?php if(!$packHidden): ?>
				<div class="row">
					<div class="card">
						<div class="card-header">
							<h3><?php echo $card1Title; ?></h3>
						</div>
						<div class="card-content">
							<?php echo nl2br($card1Dscp); ?>
							
						</div>
						<div class="card-footer">
							<h3><?php echo $card1Price; ?></h3>
						</div>
					</div>
					<div class="card">
						<div class="card-header">
							<h3><?php echo $card2Title; ?></h3>
						</div>
						<div class="card-content">
							<?php echo nl2br($card2Dscp); ?>
							
						</div>
						<div class="card-footer">
							<h3><?php echo $card2Price; ?></h3>
						</div>
					</div>
					<div class="card">
						<div class="card-header">
							<h3><?php echo $card3Title; ?></h3>
						</div>
						<div class="card-content">
							<?php echo nl2br($card3Dscp); ?>
							
						</div>
						<div class="card-footer">
							<h3><?php echo $card3Price; ?></h3>
						</div>
					</div>
				</div>
				<?php endif; ?>
			</div>
			</div>
			<?php if(!$commentHidden): ?>
			<div  class="group-grey">
				<div class="container">
					<h1  class="col-12"><?php echo $commentTitle; ?></h1>
					<div class="row">
						<?php foreach($comments as $comment): ?>
						<div id="comment">
							<p>
								"<?php echo $comment['description'];?>"
							</p>
							<div>
                                <?php if(is_file("resource/images/picture_users/" . $comment['picture'])): ?>
                                    <img class="icon-user-comment" src="<?php echo DIRIMAGES ?>picture_users/<?php echo $comment['picture']; ?>" alt='pictureProfile'>
                                <?php else: ?>
                                    <img class="icon-user-comment" src="<?php echo DIRIMAGES ?>picture_users/unknown.png"  alt='pictureProfile'>
                                <?php endif; ?>
								<?php echo $comment['lastname'].' '.$comment['firstname'].' le '.date_format(new DateTime($comment['date_inserted'] ),"d/m/Y");?>
							</div>
						</div>
						<?php endforeach ;?>
					</div>
					
				</div>
			</div>
			<?php endif; ?>
			<?php if(!$timeslotHidden): ?>
			<div  class="container">
				<h1><?php echo $tmsTitle; ?></h1>
				<div class="row">
					
					<span class="col-12"> <?php echo $tmsDscp; ?></span>
					<table id="horaires" class="col-12">
					   <tr class="row">
					       <td class="col-md-4"> <strong>Lundi</strong></td>
					       <td class="col-md-8"><?php echo $tmsMon; ?></td>
					   </tr>
					   <tr class="row">
					       <td class="col-md-4"><strong>Mardi</strong></td>
					       <td class="col-md-8"><?php echo $tmsTue; ?></td>
					   </tr>
					   <tr class="row">
					       <td class="col-md-4"><strong>Mercredi</strong></td>
					       <td class="col-md-8"><?php echo $tmsWed; ?></td>
					   </tr>
					   <tr class="row">
					       <td class="col-md-4"><strong>Jeudi</strong></td>
					       <td class="col-md-8"><?php echo $tmsThu; ?></td>
					   </tr>
					   <tr class="row">
					       <td class="col-md-4"><strong>Vendredi</strong></td></td>
					       <td class="col-md-8"><?php echo $tmsFri; ?></td>
					   </tr>
					   <tr class="row">
					       <td class="col-md-4"><strong>Samedi</strong></td>
					       <td class="col-md-8"><?php echo $tmsSat; ?></td>
					   </tr>
					   <tr class="row">
					       <td class="col-md-4"><strong>Dimanche</strong></td>
					       <td class="col-md-8"><?php echo $tmsSun; ?></td>
					   </tr>
					</table>
				</div>
			</div>
			<?php endif; ?>
		</div>
	</main>
	