<main class="container">
    <div class="form-tab">
        <h1><?php echo $title ?></h1>
        <?php $this->addModal($form,$config, null,$values); ?>
    </div>
</main>
<script type="text/javascript">
    $('#allChecked').on('click',function () {
        if( $(this).prop( 'checked')){
            $('input[type=checkbox]').each(function() {
                $(this).prop( "checked", true );
            });
        } else {
            $('input[type=checkbox]').each(function() {
                $(this).prop( "checked", false );
            });
        }
    });

    $('input[type=checkbox]').on('click',function () {
        var allChecked = true;
        $('input[type=checkbox]').each(function() {
            if(!$(this).prop( "checked") && $(this).attr('id') != "allChecked"){
                allChecked = false;
            }
        });

        if(allChecked){
            $('#allChecked').prop( "checked", true );
        } else {
            $('#allChecked').prop( "checked", false );

        }
    });


    $('input[type=button]').on('click',function () {

        var role = {};
        $('form [name]').each(function () {
            if($( this ).attr('type') == 'checkbox'){
                if($(this).is(':checked')){
                    role[$(this).attr('name')] = $(this).attr('name');
                }
            } else {
                role[$(this).attr('name')] = $(this).val();
            }
        });

        $.ajax({
            url      : $('form').attr('action'),
            type     : 'PUT',
            data : role,
            success  : function(response) {
                var status = JSON.parse(response).status;
                var message = JSON.parse(response).message;
                Toast.show(message,status)
            },
            error    : function(response) {
                console.log('error')
            }
        });
    });
</script>