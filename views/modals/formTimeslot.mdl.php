<form method="<?php echo $config["config"]["method"]?>" action="<?php echo $config["config"]["action"]?>">

	<?php foreach ($config["input"] as $name => $attributs):?>
		
		<?php if($attributs["type"]=="text" || $attributs["type"]=="email" || $attributs["type"]=="number" || $attributs["type"]=="password" || $attributs["type"]=="date"):?>
		
			<div class='row'>
				<div class='col-sm-2 label'><label><?php echo $attributs["label"];?><?php if(isset($attributs["required"])){ echo '<span class="required">*</span>';} ?></label></div>
				<div class='col-sm-10'>
					<input 
						type="<?php echo $attributs["type"];?>" 
						placeholder="<?php echo $attributs["placeholder"];?>"
						name="<?php echo $name;?>"
						value="<?php if(isset($attributs["value"])) echo $attributs["value"] ?>"
						<?php echo (isset($attributs["required"]))?"required='required'":"";?>
					>
				</div>
			</div>

		<?php elseif($attributs["type"]=="select"):?> 
		
			<div class='row'>
				<div class='col-sm-2 label'><label><?php echo $attributs['label'] ?><span class='required'>*</span></label></div>
				<div class='col-sm-10'>
					<select name="<?php echo $name ?>" required>
						<option value="">Choisissez un créneau horaire</option>
						<?php foreach ($attributs['options'] as $key => $value):?>
							<option value="<?php echo $key?>"><?php echo $value?></option>
						<?php endforeach;?>
					</select>
				</div>
			</div>

		<?php endif;?>

	<?php endforeach;?>

	<div class='row'>
		<div class='col-sm-2 label'><label>Élève<span class='required'>*</span></label></div>
		<div class='col-sm-10'>
			<select name="idStudent" required="required">

				<option value="">Choisissez un élève</option>

				<?php foreach ($config['selectStudent']["option"] as $name => $attributs):?>
					<option value="<?php echo $name; ?>"><?php echo $attributs; ?></option>
				<?php endforeach;?>

			</select>
		</div>
	</div>

	<div class='row'>
		<div class='col-sm-2 label'><label>Moniteur<span class='required'>*</span></label></div>
		<div class='col-sm-10'>
			<select name="idMonitor" required="required">

				<option value="">Choisissez un moniteur</option>

				<?php foreach ($config['selectMonitor']["option"] as $name => $attributs):?>
					<option value="<?php echo $name; ?>"><?php echo $attributs; ?></option>
				<?php endforeach;?>

			</select>
		</div>
	</div>

    <button onclick="window.history.back()"> ANNULER</button>
    <input type="<?php echo $config["config"]["type"];?>" class="btn  button-primary" value="<?php echo $config["config"]["value"];?>">

</form>

