

<form method="<?php echo $config["config"]["method"]?>" action="<?php echo $config["config"]["action"]?>">

	<?php foreach ($config["input"] as $name => $attributs):?>
		<div class="input-group row">
			<span class="col-md-3"><?php echo $attributs["label"];  if(isset($attributs["required"])){ echo '<span class="required">*</span>';} ?></span>
			<?php if($attributs["type"]=="text" || $attributs["type"]=="email" || $attributs["type"]=="number" || $attributs["type"]=="password"):?>

				<input 
					type="<?php echo $attributs["type"];?>"
					class = "input-form col-md-6" 
					placeholder="<?php echo $attributs["placeholder"];?>"
					name="<?php echo $name;?>"
                    value="<?php echo isset($values[$name])?$values[$name]:"";?>"
                    maxlength="<?php echo isset($attributs["maxString"])?$attributs["maxString"]:"";?>"
					<?php echo (isset($attributs["required"]))?"required='required'":"";?>
				>

			<?php endif;?>
			<?php if($attributs["type"]=="textarea"): ?>
				<textarea class="input-form col-md-6" name="<?php echo $name;?>" rows="<?php echo $attributs["rows"];?>"
					cols="<?php echo $attributs["cols"];?>"><?php echo isset($values[$name])?$values[$name]:"";?></textarea>
            <?php endif;?>
        </div>

	<?php endforeach;?>
    <a class="cancel" href="<?php echo $config["config"]["back"];?>">Annuler</a>

    <input id="save" type="<?php echo $config["config"]["type"];?>" class="btn  button-primary" value="<?php echo $config["config"]["value"];?>">

</form>

