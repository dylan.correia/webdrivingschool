<form method="<?php echo $config["config"]["method"]?>" action="<?php echo $config["config"]["action"]?>" enctype="multipart/form-data">

	<?php foreach ($config["sections"] as $SectionName => $sectionAttr):?>
	<section id="<?php echo  $SectionName;?>">
            <?php foreach ($sectionAttr["input"] as $name => $attributs):?>
                <div class="row input-group">
                <label class="col-sm-3"><?php echo $attributs["label"];if(isset($attributs["required"])){ echo '<span class="required">*</span>';} ?></label>
                <?php if($attributs["type"]=="text" ):?>

                    <input
                            class="input-form col-sm-8"
                            type="<?php echo $attributs["type"];?>"
                            placeholder="<?php echo $attributs["placeholder"];?>"
                            value="<?php echo $values[$name];?>"
                            name="<?php echo $name;?>"
                            maxlength="<?php echo isset($attributs["maxString"])?$attributs["maxString"]:"";?>"
                        <?php echo (isset($attributs["required"]))?"required='required'":"";?>
                    >

                <?php endif;?>
                <?php if($attributs["type"]=="textarea"): ?>
                    <textarea class="input-form col-sm-8" name="<?php echo $name;?>" rows="<?php echo $attributs["rows"];?>"
                              cols="<?php echo $attributs["cols"];?>"><?php echo $values[$name];?></textarea>
                <?php endif;?>
                <?php if($attributs["type"]=="checkbox" || $attributs["type"]=="file"): ?>
                    <input

                            type="<?php echo $attributs["type"];?>"
                        <?php echo ($values[$name]==1)? "checked":"";?>
                            name="<?php echo $name;?>"
                    >
                <?php endif;?>
                </br>
        </div>
        <?php endforeach;?>

		
	</section>

<?php endforeach;?>
	<a  class="cancel" href="<?php echo $config["config"]["back"];?>">ANNULER</a>
	<input  class="button-primary" id="save" type="button" value="<?php echo $config["config"]["value"];?>">
	
</form>
