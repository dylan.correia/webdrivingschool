<form method="<?php echo $config["config"]["method"] ?>" action="<?php echo $config["config"]["action"] ?>"
      id="ProgressForm">

    <?php foreach ($config["input"] as $name => $attributs): ?>

        <?php if ($attributs["type"] == "select"): ?>
            <div class="row">
                <div class='col-sm-2 label'><label>Horaire<span class='required'>*</span></label></div>
                <div class="col-sm-10">
                    <select name="<?php echo $name; ?>"<?php echo isset($values["timeslot"]) ? "disabled" : "" ?> >
                        <option value="">Séléctionner une horaire</option>
                        <?php foreach ($attributs["values"] as $value): ?>
                            <option value="<?php echo $value["id"]; ?>"<?php echo isset($values["timeslot"]) ? "selected" : ""; ?>>
                                <?php echo $value["hour"] . " " . $value["day"]; ?>
                            </option>
                        <?php endforeach; ?>
                    </select></div>
            </div>
        <?php endif; ?>

        <?php if ($attributs["type"] == "text"): ?>
            <div class="row">
                <div class='col-sm-2 label'><label>Titre<span class='required'>*</span></label></div>
                <div class="col-sm-10">
                    <input
                            type="<?php echo $attributs["type"]; ?>"
                            placeholder="<?php echo $attributs["placeholder"]; ?>"
                            name="<?php echo $name; ?>"
                            value="<?php echo isset($values[$name]) ? $values[$name] : ""; ?>"
                        <?php echo (isset($attributs["required"])) ? "required='required'" : ""; ?>
                    ></div>
            </div>

        <?php endif; ?>

        <?php if ($attributs["type"] == "textarea"): ?>
            <div class="row">
                <div class='col-sm-2 label'><label>Observation<span class='required'>*</span></label></div>
                <div class="col-sm-10">
            <textarea
                    form="ProgressForm"
                    placeholder="<?php echo $attributs["placeholder"]; ?>"
                    name="<?php echo $name; ?>"
                    maxlength="<?php echo $attributs["maxlength"]; ?>"
                    rows="<?php echo $attributs["rows"]; ?>"
                    spellcheck="<?php echo $attributs["spellcheck"]; ?>"
                <?php echo (isset($attributs["required"])) ? "required='required'" : ""; ?>
            ><?php echo isset($values[$name]) ? $values[$name] : ""; ?></textarea></div>
            </div>

        <?php endif; ?>


    <?php endforeach; ?>
    <?php echo isset($values[$name]) ? "<input type=\"hidden\" name=\"id\" value=\"" . $values['id'] . "\">" : ""; ?>


    <div id="buttons" class="row">
        <div class="offset-sm-1 col-sm-5">
                <button onclick="window.history.go(-1); return false;" class="btn button-secondary back" id="buttonCancelProgress">RETOUR</button>
        </div>
        <div class="col-sm-5">
            <input type="<?php echo $config["config"]["type"]; ?>" class="btn button-primary"
                   id="save" value="<?php echo $config["config"]["value"];?>">
        </div>
    </div>

</form>