<form method="<?php echo $config["config"]["method"]?>" action="<?php echo $config["config"]["action"]?>" enctype="multipart/form-data">

	<?php foreach ($config["input"] as $name => $attributs):?>
		
		<?php if($attributs["type"]=="text" || $attributs["type"]=="email" || $attributs["type"]=="number" || $attributs["type"]=="password"):?>

		<div class='row'>
			<div class='col-sm-2 label'><label><?php echo $attributs["label"];?><?php if(isset($attributs["required"])){ echo '<span class="required">*</span>';} ?></label></div>
			<div class='col-sm-10'>
				<input 
					type="<?php echo $attributs["type"];?>" 
					placeholder="<?php echo $attributs["placeholder"];?>"
					name="<?php echo $name;?>"
					value="<?php if(isset($attributs["value"])) echo $attributs["value"];?>"
					<?php echo (isset($attributs["required"]))?"required='required'":"";?>
				>
			</div>
		</div>

		<?php elseif($attributs["type"]=="file" || $attributs["type"]=="date"):?>

		<div class='row'>
			<?php if($attributs["type"]=="file"): ?>
				<div class='col-sm-2 labelFile'><label><?php echo $attributs["label"];?><?php if(isset($attributs["required"])){ echo '<span class="required">*</span>';} ?></label></div>
			<?php else: ?>
				<div class='col-sm-2 label'><label><?php echo $attributs["label"];?><?php if(isset($attributs["required"])){ echo '<span class="required">*</span>';} ?></label></div>
			<?php endif; ?>
			<div class='col-sm-10'>
				<input 
					type="<?php echo $attributs["type"];?>" 
					name="<?php echo $name;?>"
					value="<?php if(isset($attributs["value"])) echo $attributs["value"];?>"
					<?php echo (isset($attributs["required"]))?"required='required'":"";?>
				>
			</div>
		</div>

		<?php endif;?>

	<?php endforeach;?>

	<div class='row'>
		<div class='col-sm-2 label'><label>Rôle<span class='required'>*</span></label></div>
		<div class='col-sm-10'>
			<select name="<?php echo $config["select"]["name"];?>" required="required">

				<option value="">Choisissez un rôle</option>

				<?php foreach ($config["select"]["option"] as $name => $attributs):?>

					<option value="<?php echo $name; ?>" <?php if(isset($config["select"]["selected"]) && $config["select"]["selected"] == $name){ ?> selected="selected" <?php } ?> ><?php echo $attributs; ?></option>

				<?php endforeach;?>

			</select>
		</div>
	</div>

	<div id="buttons" class="row">
		<div class="offset-sm-1 col-sm-5">
			<button id="buttonCancel">Annuler</button>
		</div>
		<div class="col-sm-5">
			<?php if(isset($config["config"]["method"]) && $config["config"]["method"] == "POST"): ?>
				<input type="submit" value="<?php echo $config["config"]["submit"];?>" class="button-primary">
			<?php else: ?>
				<input type="submit" value="<?php echo $config["config"]["submit"];?>" class="button-primary" id="updateUsers">
			<?php endif; ?>
		</div>
	</div>

</form>