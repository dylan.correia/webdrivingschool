
<form class="row" method="<?php echo $config["config"]["method"]?>" action="<?php echo $config["config"]["action"]?>">
        <section class=" col-6">
            <?php foreach ($config["input"] as $name => $attributs):?>


                <div class="input-group row">
                    <span class="col-md-3"> <?php echo $attributs["label"];  if(isset($attributs["required"])){ echo '<span class="required">*</span>';} ?></span>
                <?php if($attributs["type"]=="text"):?>
                    <input value="<?php echo isset($values[$name])?$values[$name]:"" ;?>"
                           type="text"
                           class="input-form col-md-8"
                           placeholder="<?php $attributs['placeholder'];?>"
                           maxlength="<?php echo isset($attributs["maxString"])?$attributs["maxString"]:"";?>"
                           name="<?php echo $name ?>" <?php echo (isset($attributs["required"]))?"required='required'":"";?>>
                <?php endif ; ?>
                <?php if($attributs["type"]=="textarea"):?>
                <textarea
                        class="input-form col-md-8"
                        rows="<?php echo $attributs["rows"];?>"
                        cols="<?php echo $attributs["cols"];?>"
                        name="<?php echo $name;?>"
                    ><?php echo isset($values[$name])?$values[$name]:"";?></textarea>
                <?php endif; ?>
                </div>

            <?php endforeach; ?>

            <a class="cancel" href="<?php echo DIRNAME.'roles' ;?>">ANNULER</a>
            <input type="<?php echo $config["config"]["type"]?>" class="btn  button-primary" value="ENREGISTRER">
        </section>
        <section class=" col-6 border-section" >
            <h2>Privilèges du rôle</h2>
            <div class="checkbox-group-h3">
                <h3>Tous les privilèges</h3>
                <label class="switch switch-type" role="switch">
                    <input id="allChecked" type="checkbox" class="switch-toggle" <?php echo isset($values['allChecked'])? "checked" :"";?>>
                    <span class="switch-label"></span>
                </label>
            </div>
            <?php foreach ($values['permissions'] as $permission): ?>
                <div class="checkbox-group">
                    <label><?php echo  $permission->getTitle()?></label>
                    <label class="switch switch-type" role="switch">
                        <input name="<?php  echo $permission->getId()?>" <?php echo isset($permission->checked)? "checked" :"";?> type="checkbox" class="switch-toggle" >
                        <span class="switch-label"></span>
                    </label>
                </div>
            <?php endforeach; ?>
        </section>
</form>