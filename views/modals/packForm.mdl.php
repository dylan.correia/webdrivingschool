
<?php foreach ($config["input"] as $name => $attributs):?>
	<div class="row input-group ">
		<label  class="col-md-4"><?php echo $attributs["label"];  if(isset($attributs["required"])){ echo '<span class="required">*</span>';} ?></label>
		<?php if($attributs["type"]=="text" || $attributs["type"]=="email" || $attributs["type"]=="number" || $attributs["type"]=="password"):?>

			<input 
				class="input-form col-md-4"
				type="<?php echo $attributs["type"];?>" 
				placeholder="<?php echo $attributs["placeholder"];?>"
				value="<?php echo $values[$name];?>"
				name="<?php echo $name;?>"
				maxlength="<?php echo isset($attributs["maxString"])?$attributs["maxString"]:"";?>"
				<?php echo (isset($attributs["required"]))?"required='required'":"";?>
			>

		<?php endif;?>
		<?php if($attributs["type"]=="textarea"): ?>
				<textarea  class="input-form col-md-4" name="<?php echo $name;?>" 
					 rows="<?php echo $attributs["rows"];?>" 
				cols="<?php echo $attributs["cols"];?>" ><?php echo isset($values[$name])?$values[$name]:"";?></textarea>
		<?php endif;?>
		</br>
	</div>
<?php endforeach;?>
<div class="input-group ">
	<button id="cancel" class="cancel">ANNULER</button>
	<button class="button-primary" id="savePack"> Enregistrer</button>
	<button class="hide button-primary" id="updatePack"> Modifier</button>
</div>
