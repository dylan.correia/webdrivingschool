<form method="<?php echo $config["config"]["method"]?>" action="<?php echo $config["config"]["action"]?>">

	<?php foreach ($config["input"] as $name => $attributs):?>
		<div class="input-group">
			<?php if( isset($attributs["label"])): ?>

			<label><?php echo $attributs["label"]; if(isset($attributs["required"])){ echo '<span class="required">*</span>';} ?></label>
			<?php endif; ?>
			<?php if($attributs["type"]=="text"):?>

				<input 
					class="input-form"
					type="<?php echo $attributs["type"];?>" 
					placeholder="<?php echo $attributs["placeholder"];?>"
					value="<?php echo isset($values[$name])?$values[$name]:"";?>"
					name="<?php echo $name;?>"
					maxlength="<?php echo isset($attributs["maxString"])?$attributs["maxString"]:"";?>"
					<?php echo (isset($attributs["required"]))?"required='required'":"";?>
				>

			<?php endif;?>

			<?php if($attributs["type"]=="checkbox"):?>
					<input 
					class="input-form"
					type="<?php echo $attributs["type"];?>" 
					<?php echo ($values[$name]==1)? "checked":"";?> 
					name="<?php echo $name;?>"
				>
			<?php endif;?>
			<?php if($attributs["type"]=="textarea"): ?>
					<textarea class="editor input-form" rows="40" name="<?php echo $name;?>" 
						><?php echo isset($values[$name])?$values[$name]:"";?></textarea>
			<?php endif;?>
            <?php if($attributs["type"]=="select"): ?>
                <select name="<?php echo $name;?>">
                    <?php foreach ($attributs['values'] as $value): ?>
                        <option value="<?php  echo $value['value']?>"
                            <?php echo ($value['value'] == $values[$name]) ?  "selected=\"selected\"":"" ?>>
                            <?php  echo $value['label']; ?>
                        </option>
                    <?php endforeach; ?>
                </select>
            <?php endif;?>
		</div>
	<?php endforeach;?>

	<a class="cancel" href="<?php echo $config["config"]["back"];?>">ANNULER</a>
	<input  class="button-primary" id="save" type="<?php echo $config["config"]["type"];?>" value="<?php echo $config["config"]["value"];?>">
</form>

