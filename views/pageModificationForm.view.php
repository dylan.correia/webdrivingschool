<main class="container">
    <div class="form-tab">
        <h1>MODIFICATION DE LA PAGE</h1>
        <?php $this->addModal($form,$config, $errors,$values); ?>

    </div>
</main>
<script type="text/javascript">

    $('input[type=file]').on('change', prepareUpload);

    function prepareUpload(event)
    {
        var fileName =  event.target.name;
        var fileType = event.target.files[0].type;

        if(![ "image/png", "image/gif ", "image/jpeg","image/svg+xml"].includes(fileType)){
            Toast.show('Le format de l\'image est incorrect','error')
        }

        var oFileReader = new FileReader();
        oFileReader.readAsDataURL(event.target.files[0]);

        oFileReader.onload = function() {
            $('[name="'+fileName+'"]').data('base64',oFileReader.result.replace(/^data:.*?;base64,/, ""));
            $('[name="'+fileName+'"]').data('type',fileType);
        }
    }

    $('#save').on('click',function () {

        var pageDescription = {
			page: {},
			section : {},
			content : {},
            files : {}
		};
		var method = $('form').attr('method');
		var url = $('form').attr('action');
		
		$('form [name]').each(function() {
			if($(this).val()){
				if($(this).attr('name')=='pageTitle' || $(this).attr('name')=='pagePosition'){
					pageDescription.page[$(this).attr('name')] = $(this).val();
				}else if($(this).attr('name')=='pageHidden'){
					pageDescription.page[$(this).attr('name')] = $(this).is(':checked') ? 1 : 0;
				} else if($( this ).attr('type') == 'text'){
					pageDescription.content[$(this).attr('name')] = $(this).val();
				} else if($( this ).attr('type') == 'checkbox'){
					pageDescription.section[$(this).attr('name')] = $(this).is(':checked') ? 1 : 0;
				} else {
					pageDescription.content[$(this).attr('name')] = $(this).val();
				}
			}

		});

		if(method == 'PUT' && tinymce.activeEditor){
			pageDescription.content['contentPage'] =  tinymce.activeEditor.getContent();
		}

        $('input[type=file]').each(function() {
            if($(this).data('base64')){
                var image  = {
                    base64 :"",
                    type:""
                };
                image.base64 =  $('[name="'+$(this).attr('name')+'"]').data('base64');
                image.type = $('[name="'+$(this).attr('name')+'"]').data('type');
                pageDescription.files[$(this).attr('name')] = image;
            }
        });

		$.ajax({
            url      : url,
            type     : method,
            data : {pageDscp :pageDescription},
            success  : function(response) {
                var status = JSON.parse(response).status;
                var message = JSON.parse(response).message;
    			Toast.show(message,status)
            },
            error    : function(response) {
                console.log('error')
            }
        });

	});
</script>