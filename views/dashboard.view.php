<link rel="stylesheet" type="text/css" href="css/view_dashboard.css">

<main>
	
	<div class="container-fluid">
		<!-- Planning -->
		<div class="row">
			<div class="col-sm-12">
				<section id="planning">
					<div class="row">
						<div class="col-12">
							<h2 id="title_planning">PLANNING</h2>
						</div>
					</div>
					<div class="row">
						<div class="offset-1 col-10 offset-1">
							<img id="dashboard_calendar" src="./resource/images/dashboard_calendar.jpg" alt="planning">
						</div>
					</div>
					<div class="row">
						<div class="col-12">
							<a href="<?php echo DIRNAME ?>planning"><button class="button-primary">VOIR</button></a>
						</div>
					</div>
				</section>
			</div>
		</div>

		<div class="row">
			<div class="col-ml-6">
				<!-- Schedule -->
				<section id="schedule">
					<div class="row">
						<div class="col-12">
							<h2 id="title_planning">HORAIRES</h2>
						</div>
					</div>
					<div class="row">
						<div class="offset-1 col-10 offset-1">
							<table>
								<tr>
									<th>Date</th>
									<th>Heure</th>
									<th>Nom</th>
									<th>Prénom</th>
								</tr>

								<?php foreach($allTimeslot as $key => $value): ?>

									<?php
									if($key >= 5){
										break;
									}
									?>

									<tr>
										<?php
										$dateTimeslot = new DateTime($value['day']);
										?>
										<td><?php echo $dateTimeslot->format('d/m/Y'); ?></td>
										<td><?php echo $value['hour']; ?></td>
										<td><?php echo $value['lastname']; ?></td>
										<td><?php echo $value['firstname']; ?></td>
									</tr>

								<?php endforeach; ?>

							</table>
						</div>
					</div>
					<div class="row">
						<div class="col-12">
							<a href="<?php echo DIRNAME ?>timeslot"><button class="button-primary">VOIR</button></a>
						</div>
					</div>
				</section>
			</div>

			<div class="col-ml-6">
				<!-- Schedule -->
				<section id="users">
					<div class="row">
						<div class="col-12">
							<h2 id="title_planning">UTILISATEURS</h2>
						</div>
					</div>

					<div class="row">
						<div class="offset-1 col-10 offset-1">
							<table>
								<tr>
									<th>Rôle</th>
									<th>Nom</th>
									<th>Prénom</th>
									<th>Dernière connexion</th>
								</tr>

								<?php foreach($allUser as $key => $objUser): ?>

									<?php
									if($key >= 5){
										break;
									}
									?>

									<tr>
										<td><?php echo $objUser->nameRole; ?></td>
										<td><?php echo $objUser->getLastname(); ?></td>
										<td><?php echo $objUser->getFirstname(); ?></td>
										<?php
										$dateUser = new DateTime($objUser->getLastConnection());
										?>
										<td><?php echo $dateUser->format('d/m/Y H:i'); ?></td>
									</tr>

								<?php endforeach; ?>

							</table>
						</div>
					</div>
					<div class="row">
						<div class="col-12">
							<a href="<?php echo DIRNAME ?>users"><button class="button-primary">VOIR</button></a>
						</div>
					</div>
				</section>
			</div>
		</div>

	</div>


</main>