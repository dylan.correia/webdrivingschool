<div class="container">
    <?php if(!isset($noComments)): ?>
        <div id="comments" >
            <?php foreach ($comments as $comment) : ?>
                <div class="row">
                    <section class="comment-content">
                        <p>"<?php echo $comment['description']; ?>"</p>
                        <div class="comment-picture">
                            <?php if(is_file(DIRIMAGES."/picture_users/" . $comment['picture'])): ?>
                                <img class="icon-user-comment" src="<?php echo DIRIMAGES ?>picture_users/<?php echo $comment['picture']; ?>" alt='pictureProfile'>
                            <?php else: ?>
                                <img class="icon-user-comment" src="<?php echo DIRIMAGES ?>picture_users/unknown.png"  alt='pictureProfile'>
                            <?php endif; ?>
                            <?php echo $comment['lastname'].' '.$comment['firstname'].' le '.$comment['date_inserted'];?>
                        </div>
                    </section>
                </div>
            <?php endforeach ?>
        </div>
        <?php for($i=1;$i<=$nbPages & $i<=10;$i++): ?>
            <button class="pageNumber <?php echo ($i==1)?"pageNumber-active":""; ?>"><?php echo $i?></button>
        <?php endfor ?>
    <?php else: ?>
        <div class="row">
            Aucun commentaire
        </div>
    <?php endif; ?>
</div>
<span id="dirimages" class="hide"><?php echo DIRIMAGES?></span>
<script type="text/javascript">
	$(".container").on('click', '.pageNumber', function() {
        $('.pageNumber-active')[0].classList.remove("pageNumber-active");
        btnNumber =  $(this).text();
    	$(this).addClass('pageNumber-active');
    	$.ajax({
            url      : "<?php echo DIRNAME ?>"+"comments/pages/"+btnNumber,
            type     : "GET",
            success  : function(response) {
                loadComments(JSON.parse(response));
            },
            error    : function(response) {
                console.log('error')
            }
        })
    	

	});

	function loadComments(packs){

        var dirImages = $('#dirimages').text();
		$('#comments').text('')
		for(var i in packs){
			var tab = '';
				tab += '<div class="row">';
				tab += '<section class="comment-content">';
				tab += '<p>"'+packs[i].description+'"</p>';
				tab += '<div class="comment-picture">';
                if(packs[i].picture != "" && imageExists(dirImages + 'picture_users/' + packs[i].picture)){
                    tab += '<img class="icon-user-comment" src="' + dirImages + 'picture_users/' + packs[i].picture + '">';
                }
                else{
                    tab += '<img class="icon-user-comment" src="' + dirImages + 'picture_users/unknown.png">';
                }
				tab += packs[i].lastname+' '+packs[i].firstname;
				tab += '</br>';
				tab += '<span> '+packs[i].date_inserted+'</span>';
				tab += '</div>';
				tab += '</section>';
				tab += '</div>';
			$('#comments').append(tab)
		}
        
	}

    function imageExists(image_url){
        var http = new XMLHttpRequest();

        http.open('HEAD', image_url, false);
        http.send();

        return http.status != 404;
    }
</script>