<html>
    <head>
        <title>WDS Administration</title>
        <meta name="description" content="Administration du CMS">
        <meta charset="utf-8">
        <link rel="stylesheet" type="text/css" href="<?php echo DIRCSS; ?>grille.css">
        <link rel="stylesheet" type="text/css" href="<?php echo DIRCSS; ?>ionicons.css">
        <link rel="stylesheet" type="text/css" href="<?php echo DIRCSS; ?>template_back.css">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
        <script type="text/javascript" src="<?php echo DIRJS; ?>plugin/tinymce/tinymce.min.js"></script>
        <script type="text/javascript" src="<?php echo DIRJS; ?>plugin/tinymce/langs/fr.js"></script>
        <script type="text/javascript" src="<?php echo DIRJS; ?>index.js"></script>
    </head>
    <body>

        <header>
            <div id="toast"><?php  echo isset($errors)?implode(' ',$errors):""; ?></div>
            <section id="top-nav">
                <article id="burgerMenu">
                    <img class="svg" src="<?php echo DIRIMAGES; ?>ionicons/android-menu.svg" alt="burgerMenu">
                </article>

                <article id="nameSiteWeb">
                    <img class="svg" src="<?php echo DIRIMAGES; ?>ionicons/ios-home-outline.svg" alt="Home">
                    <a target="_blank" href="<?php echo DIRNAME ?>"><?php echo $sitename ?></a>
                </article>
                <article id="profile">
                    <div id="profileNamePicture">
                        <span><?php echo Auth::firstname().' '.Auth::lastname() ?></span>

                        <?php if(is_file("resource/images/picture_users/" . Auth::picture())): ?>
                            <img class="pictureProfile" src="<?php echo DIRIMAGES ?>picture_users/<?php echo Auth::picture(); ?>" alt='pictureProfile'>
                        <?php else: ?>
                            <img class="pictureProfile" src="<?php echo DIRIMAGES ?>picture_users/unknown.png"  alt='pictureProfile'>
                        <?php endif; ?>

                        <img class="chevronProfile openProfile" src="<?php echo DIRIMAGES; ?>tpl/open-profile.png" alt="openProfile"><img class="chevronProfile closeProfile" src="<?php echo DIRIMAGES; ?>tpl/close-profile.png" alt="closeProfile">
                    </div>
                    <div id="profileSettings">
                        <div id="myProfile">
                            <img src="<?php echo DIRIMAGES; ?>tpl/picture-profile.png" alt="logoProfile">
                            <span><a class="logout-btn" href="<?php echo DIRNAME ?>profile">Mon profil</a></span>
                        </div>
                        <div id="logout">
                            <img class="svg" src="<?php echo DIRIMAGES; ?>ionicons/log-out.svg" alt="logoDeconnexion">
                            <span><a class="logout-btn" href="<?php echo DIRNAME ?>logout">Se déconnecter</a></span>
                        </div>
                    </div>
                </article>
            </section>
        </header>
        <nav id="mainNavigation">
            <div class="logo">
                <img src="<?php echo DIRIMAGES; ?>tpl/logo-WDS.png" alt="logo" id="logo">
            </div>
            <ul>
                <?php if(Auth::hasPermission('dashboardRight')):?>
                <li class="<?php echo ($menuPage=="dashboard")?"active":""; ?>">
                    <img class="svg" src="<?php echo DIRIMAGES; ?>ionicons/ios-speedometer-outline.svg" alt="tableauDeBord">
                    <a href="<?php echo DIRNAME ?>dashboard">Tableau de bord</a>
                </li>
                <?php endif ?>
                <?php if(Auth::hasPermission('pagesRight')):?>
                <li class="<?php echo ($menuPage=="pages")?"active":""; ?>">
                    <img class="svg" src="<?php echo DIRIMAGES; ?>ionicons/ios-paper-outline.svg" alt="pages">
                    <a href="<?php echo DIRNAME ?>pages">Pages</a>
                </li>
                <?php endif ?>
                <?php if(Auth::hasPermission('usersRight')):?>
                <li class="<?php echo ($menuPage=="users")?"active":""; ?>">
                    <img class="svg" src="<?php echo DIRIMAGES; ?>ionicons/ios-people-outline.svg" alt="utilisateurs">
                    <a href="<?php echo DIRNAME ?>users">Utilisateurs</a>
                </li>
                <?php endif ?>
                <?php if(Auth::hasPermission('rolesRight')):?>
                <li class="<?php echo ($menuPage=="roles")?"active":""; ?>">
                    <img class="svg" src="<?php echo DIRIMAGES; ?>ionicons/ios-compose-outline.svg" alt="roles">
                    <a href="<?php echo DIRNAME ?>roles">Rôles</a>
                </li>
                <?php endif ?>
                <?php if(Auth::hasPermission('planningRight')):?>
                <li class="<?php echo ($menuPage=="planning")?"active":""; ?>">
                    <img class="svg" src="<?php echo DIRIMAGES; ?>ionicons/ios-calendar-outline.svg" alt="planning">
                    <a href="<?php echo DIRNAME ?>planning">Planning</a>
                </li>
                <?php endif ?>
                <?php if(Auth::hasPermission('myPlanningRight')):?>
                <li class="<?php echo ($menuPage=="planning")?"active":""; ?>">
                    <img class="svg" src="<?php echo DIRIMAGES; ?>ionicons/ios-calendar-outline.svg" alt="planning">
                    <a href="<?php echo DIRNAME ?>planning/myplanning">Mon planning</a>
                </li>
                <?php endif ?>
                <?php if(Auth::hasPermission('timeslotRight')):?>
                <li class="<?php echo ($menuPage=="timeslot")?"active":""; ?>">
                    <img class="svg" src="<?php echo DIRIMAGES; ?>ionicons/ios-time-outline.svg" alt="horaires">
                    <a href="<?php echo DIRNAME ?>timeslot">Horaires</a>
                </li>
                <?php endif ?>
                <?php if(Auth::hasPermission('progressionsRight') ):?>
                <li class="<?php echo ($menuPage=="progress")?"active":""; ?>">
                    <img class="svg" src="<?php echo DIRIMAGES; ?>ionicons/ios-bookmarks-outline.svg" alt="progressions">
                    <a href="<?php echo DIRNAME ?>progress">Progressions</a>
                </li>
                <?php endif ?>
                <?php if(Auth::hasPermission('myProgessionRight')):?>
                <li class="<?php echo ($menuPage=="myprogress")?"active":""; ?>">
                    <img class="svg" src="<?php echo DIRIMAGES; ?>ionicons/ios-bookmarks-outline.svg" alt="progressions">
                    <a href="<?php echo DIRNAME ?>myprogress">Mes progressions</a>
                </li>
                <?php endif ?>
                <?php if(Auth::hasPermission('feedbackRight')):?>
                <li class="<?php echo ($menuPage=="feedback")?"active":""; ?>">
                    <img class="svg" src="<?php echo DIRIMAGES; ?>ionicons/ios-chatboxes-outline.svg" alt="commentaires">
                    <a href="<?php echo DIRNAME ?>feedback">Commentaires</a>
                </li>
                <?php endif ?>
                <?php if( Auth::hasPermission('myFeedbackRight')):?>
                <li class="<?php echo ($menuPage=="myfeedback")?"active":""; ?>">
                    <img class="svg" src="<?php echo DIRIMAGES; ?>ionicons/ios-chatboxes-outline.svg" alt="commentaires">
                    <a href="<?php echo DIRNAME ?>myfeedback">Mes commentaires</a>
                </li>
                <?php endif ?>
                <?php if(Auth::hasPermission('themesRight')):?>
                <li class="<?php echo ($menuPage=="themes")?"active":""; ?>">
                    <img class="svg" src="<?php echo DIRIMAGES; ?>ionicons/ios-monitor-outline.svg" alt="theme">
                    <a href="<?php echo DIRNAME ?>themes">Thème</a>
                </li>
                <?php endif ?>
                <?php if(Auth::hasPermission('settingsRight')):?>
                <li class="<?php echo ($menuPage=="settings")?"active":""; ?>">
                    <img class="svg" src="<?php echo DIRIMAGES; ?>ionicons/ios-gear-outline.svg" alt="reglages">
                    <a href="<?php echo DIRNAME ?>settings">Réglages</a>
                </li>
                <?php endif ?>
                <?php if(Auth::hasPermission('statisticsRight')):?>
                <li class="<?php echo ($menuPage=="statistics")?"active":""; ?>">
                    <img class="svg" src="<?php echo DIRIMAGES; ?>ionicons/ios-pie-outline.svg" alt="statistiques">
                    <a href="<?php echo DIRNAME ?>statistics">Statistiques</a>
                </li>
                <?php endif ?>
            </ul>
        </nav>

        <div class="clearHeader"></div>
        <div id="contentBack">

            <?php include "views/".$this->view; ?>
            
        </div>

    </body>
    <script type="text/javascript" src="<?php echo DIRJS; ?>template_back.js"></script>
    <script type="application/javascript">
        if(textToast = $('#toast').text()) {
            Toast.show(textToast,'error')
        }
    </script>

</html>
