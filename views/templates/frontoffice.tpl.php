<!DOCTYPE html>
<html>
<head>
	<link href="https://fonts.googleapis.com/css?family=Lato" rel="stylesheet">
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
 
	<title><?php echo $title ; ?></title>
	<meta charset="utf-8">
    <meta name="keywords" content="<?php echo $sitename ?>" />
    <link rel="stylesheet" type="text/css" href="<?php echo DIRCSS; ?>grille.css">
    <link rel="stylesheet" type="text/css" href="<?php echo DIRCSS.$theme; ?>.css">
    <link rel="stylesheet" type="text/css" href="<?php echo DIRCSS; ?>ionicons.css">

</head>
<body>

	<header>
		<div class="head-header">
			<div id="header-information">
				<div>
					<i class="icon ion-android-call"></i> 
					<span><?php echo $phone; ?></span>
					<span class='email'><?php echo $mailcompany; ?></span>
				</div>
			</div>
		</div>
	</header>


	<div class="menu-bis">
		<input id="burger" type="checkbox" />

		<label for="burger">
		    <span></span>
		    <span></span>
		    <span></span>
		</label>

		<nav class="<?php echo ($active==1)?"banner":"nav-background"; ?>">
            <section>
                <a href="<?php echo DIRNAME; ?>"> <img src="<?php echo DIRIMAGES.'home/'.$bannerLogo; ?>" id="logo"></a>
                <ul>
                    <?php foreach($pages as $page) :?>
                            <li ><a class="<?php echo ($active==$page->getId())?"active":""; ?>" href="<?php echo DIRNAME.$page->getSlug();?>"><?php echo $page->getTitle();?></a></li>
                    <?php endforeach ;?>
                </ul>
            </section>
            <button type="button" id="btn-eleve" class="connexion">CONNEXION</button>
            <?php if($active ==1 && !$bannerHidden): ?>
                <section>
                    <div id="big-logo"><img src="<?php echo DIRIMAGES."home/".$bannerLogo ?>"></div>
                    <div id="sous-title-nav"><span><?php echo $bannerSlogan;?></span></div>
                </section>
            <?php endif; ?>
		</nav>
	</div>
	<div class="menu">
		
		<div class="burger-menu" >
			
			<input id="burger" type="checkbox" />

			<label for="burger">
			    <span></span>
			    <span></span>
			    <span></span>
			</label>
		</div> 

		<nav class="<?php echo ($active==1)?"banner":"nav-background"; ?>">
			<section id="menuNav">
                <a href="<?php echo DIRNAME; ?>"> <img src="<?php echo DIRIMAGES.'home/'.$bannerLogo; ?>" id="logo"></a>
					<ul>
					<?php foreach($pages as $page) :?>
						<li ><a class="<?php echo ($active==$page->getId())?"active":""; ?>" href="<?php echo DIRNAME.$page->getSlug();?>"><?php echo $page->getTitle();?></a></li>
					<?php endforeach ;?>
					</ul>
					<button type="button" id="btn-eleve" class="connexion">ESPACES ELEVES</button>
			</section>
			<?php if($active ==1 && !$bannerHidden): ?>
			<section>
				<div id="big-logo"><img src="<?php echo DIRIMAGES."home/".$bannerLogo ?>"></div>
				<div id="sous-title-nav"><span><?php echo $bannerSlogan;?></span></div>
			</section>
			<?php endif; ?>
		</nav>
	</div>

	 <?php include "views/".$this->view; ?>
			
	<footer>
		<a href="<?php echo DIRNAME?>mentionslegales">Mentions légales</a>
		<div id="socialIcons">
            <?php if(!empty($facebook)): ?>
			<a target="_blank" href="<?php echo $facebook?>"><i class="icon ion-social-facebook"></i></a>
            <?php endif; ?>
            <?php if(!empty($twitter)): ?>
            <a target="_blank" href="<?php echo $twitter?>"><i class="icon ion-social-twitter"></i></a>
            <?php endif; ?>
            <?php if(!empty($instagram)): ?>
                <a target="_blank" href="<?php echo $instagram?>"><i class="icon ion-social-instagram"></i></a>
            <?php endif; ?>

        </div>
	</footer>
</body>
    <div id="modal-connexion" class="modal hide">
		<div class="modal-content">
				<div class="modal-header">
                    <i class="icon ion-ios-close"  onclick="closeModal()"></i>
                    <h2>CONNEXION</h2>
				</div>

				<div class="modal-body">
                    <div class="modal-error row">
                        <?php if(isset($_SESSION['errormsg'])): ?>
                            <?php unset ($_SESSION['errorpwd']);?>
                            <?php echo $_SESSION['errormsg'];?>
                        <?php endif; ?>
                    </div>
					<form method="POST" action="<?php echo DIRNAME ?>login">
						<div class="row">
				  		<label class="col-md-4">Identifiant</label>
				    	<input class=" input-form col-md-4" name="email" type="email">
			  		</div> 
					<div class="row">
					  	<label class="col-md-4">Mot de passe</label>
					    <input class=" input-form col-md-4" name="pwd" type="password">
					</div>
					<div class="row">
			    		<a onclick="closeModal()" href="#" class="lostpwd" >Mot de passe oublié</a>
					</div>
			  		<button class="btn">S'identifier</button>
					</form>
			 		
			  	</div>
		</div>
	</div>

<div id="modal-lostpwd" class="modal hide">
    <div class="modal-content">
        <div class="modal-header">
            <i class="icon ion-ios-close"  onclick="closeModal()"></i>
            <h2>MOT DE PASSE PERDU</h2>
        </div>

        <div class="modal-body">
            <div class="modal-error row">
                <?php if(isset($_SESSION['errorpwd'])): ?>
                    <?php unset ($_SESSION['errormsg']);?>
                    <?php echo $_SESSION['errorpwd'];?>
                <?php endif; ?>
            </div>
            <form method="POST" action="<?php echo DIRNAME ?>lostpwd">
                <div class="row">
                    <label class="col-md-4">Email</label>
                    <input class=" input-form col-md-4" name="email" type="email">
                </div>
                <div class="row">
                    <p>Si l'adresse rentrée est correcte, vous recevrez un nouveau mot de passe par mail.</p>
                </div>
                <button class="btn">Envoyer</button>
            </form>

        </div>
    </div>
</div>

</html>

<script type="text/javascript">
	$('.connexion').on('click',function (){
		$('#modal-connexion').show();
	});

    $('.lostpwd').on('click',function (){
        $('#modal-lostpwd').show();
    });

	window.onclick = function(event) {
		var modal = document.getElementById('modal-connexion');
    	if (event.target == modal) {
        	modal.style.display = "none";
        }
    }

    function closeModal(){
        $('.modal').hide();
    }


    <?php if(isset($_SESSION['errormsg'])): ?>
        <?php echo '$(window).on(\'load\',function(){$(\'#modal-connexion\').show();});';?>
        <?php unset($_SESSION['errormsg']);?>
    <?php endif; ?>


    <?php if(isset($_SESSION['errorpwd'])): ?>
    <?php echo '$(window).on(\'load\',function(){$(\'#modal-lostpwd\').show();});';?>
    <?php unset($_SESSION['errorpwd']);?>
    <?php endif; ?>


</script>
