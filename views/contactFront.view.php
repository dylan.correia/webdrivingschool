<main class="container">
    <?php $this->addModal('form',$config, null); ?>
    <h1 id="msg" class="hide"></h1>
</main>
<script>

    $("#save").on('click',  function() {

        var form = {}
        $('form [name]').each(function() {
            form[$(this).attr('name')] = $(this).val();
        });

        $.ajax({
            url      : "<?php echo DIRNAME ?>"+"sendmessage",
            type     : "POST",
            data     : form,
            success  : function(response) {

                var response = JSON.parse(response);

                $('#msg').text(response.message)
                $('#msg').show()
            },
            error    : function(response) {
                console.log('error')
            }
        })

    });
</script>