<link rel="stylesheet" type="text/css" href="css/view_page.css">
<main>
	<div class="container">
                    <!-- Pages -->
                    <div class="row">
                        <div class="col-sm-12">
                            <section id="pages">
                                <div class="col-12 relative">
                                    <div class="userNav">
                                        <h1 id="page-title">PAGES</h1>
                                        <div class="filters">
                                            <div class="userAdd">
                                                <a href="<?php echo DIRNAME ."pages/add" ?>"><label class="titre2 addBtn"> Ajouter une page </label><i class="icon ion-ios-plus-outline userAddIcon"></i></a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <ul class="offset-1 col-10 offset-1 list">
                                        <?php foreach ($pages as $page) : ?>
                                        <li>
                                            <label class="list-title"><?php echo $page->getTitle(); ?></label>
                                            <section class="panel">
                                                <a href="<?php echo DIRNAME ."pages/".$page->getId();?>"><i class="ion-ios-compose-outline edit pulse"></i></a>
                                                <?php if($page->getStatus()=='DELETABLE'): ?>
                                                <button data-id="<?php echo $page->getId(); ?>" class="btn btn-delete"><i class="ion-ios-trash-outline delete pulse"></i></button>
                                                <?php endif; ?>
                                            </section>
                                        </li>
                                        <?php endforeach; ?>
                                    </ul>
                                </div>
                            </section>
                        </div>
                    </div>
                </div>
</main>
<script>
    $("#pages").on('click', '.btn-delete', function() {
        var id =  $(this).data("id");
        $.ajax({
            url      : "<?php echo DIRNAME ?>"+"pages/delete/"+id,
            type     : "GET",
            success  : function(response) {
                var response = JSON.parse(response);
                var status = response.status;
                var message = response.message;
                Toast.show(message,status);
                var pages  =  response.pages;
                serialize(pages);
            },
            error    : function(response) {
                console.log('error')
            }
        })

    });

    function serialize(pages) {
        $('.list').html('');
        for (var i in pages) {
            var tab = '';
        
            tab+="<li>";
            tab+="<label class=\"list-title\">"+pages[i].title+"</label>";
            tab+="<section class=\"panel\">";
            tab+="<a href=\"<?php echo DIRNAME ?>"+"pages/"+pages[i].id+"\"";
            tab+="<i class=\"ion-ios-compose-outline edit pulse\"></i></a>";
            if(pages[i].status == 'DELETABLE'){
                tab+="<button data-id=\""+pages[i].id+"\"";
                tab+="class=\"btn btn-delete\"><i class=\"ion-ios-trash-outline delete pulse\"></i></button>";
            }
            tab+="</section";
            tab+="</li>";
            $('.list').append(tab);
        }
    }
</script>