<link rel="stylesheet" type="text/css" href="<?php echo DIRCSS; ?>view_user.css">
<div id="toast"></div>
<main>

    <!-- Use date in JavaScript -->
    <input type="hidden" id="urlUser" value="<?php echo DIRNAME . "users"; ?>">

    <div class='container'>
        <div class='row'>
            <div class='col-sm-12'>
                <section id='formAddUser'>
                    <article id="titleUsers">
                        <div class="row">
                            <div id="title" class="col-sm-12">
                                <h2><?php echo $title; ?></h2>
                            </div>
                        </div>
                    </article>
                    <article><?php echo isset($values) ? $this->addModal($form, $config, null, $values) : $this->addModal($form, $config, null); ?>
                    </article>
                </section>
            </div>
        </div>
    </div>

</main>

<script>


    function goBack() {
        window.history.back();
    }

    function dataForm() {
        var dataForm = {};
        $('form [name]').each(function () {
            dataForm[$(this)[0].name] = $(this)[0].value;
        });
        return dataForm;
    }

    $('body').on('click', '#save', function () {

        var data = dataForm();
        var method = $('form').attr('method');
        var url = $('form').attr('action');

        $.ajax({
            type: method,
            url: url,
            data: {form: data},
            success: function (response) {
                console.log(response);
                var response = JSON.parse(response);
                var status = response.status;
                var message = response.message;
                Toast.show(message, status);
            },
            error: function (response) {
                console.log(reponse);
                var response = JSON.parse(response);
                var status = response.message;
                var message = response.message;
                Toast.show(message, status);
            }
        });

        return false;
    });
</script>