<main class="container">
    <section class="form-tab">
        <h1>MODIFICATION DE LA PAGE</h1>
        <?php $this->addModal($form,$config, $errors,$values); ?>
    </section>
    <section class="form-tab">
        <h1>FORFAITS</h1>
        <div class="tab-pack">
            <button id="add" class="icon ion-ios-plus-outline btn addIcon"> </button> <!-- en attendant le css du tableau ... -->
            <table>
                <tr>
                    <th>Identifiant</th>
                    <th>Titre</th>
                    <th>Prix</th>
                </tr>
                <?php foreach ($packs as $pack) : ?>
                    <tr>
                        <td><?php echo $pack->getId() ?></td>
                        <td><?php echo $pack->getTitle() ?></td>
                        <td><?php echo $pack->getPrice() ?></td>
                        <td>
                            <div class="btn-group">
                                <button class="btn icon ion-ios-compose-outline pulse edit" data-id="<?php echo $pack->getId(); ?>"></button>
                                <button data-id="<?php echo $pack->getId(); ?>" class="btn ion-ios-trash-outline delete pulse"></button>
                            </div>
                        </td>
                    </tr>
                <?php  endforeach; ?>
            </table>
        </div>
    </section>
</main>
<div id="modalAdd" class="modal hide">
    <div class="modal-content">
        <div class="modal-header">
            <i class="icon ion-ios-close"  onclick="closeModal()"></i>
            <h2>Ajout d'un forfait</h2>
        </div>
        <div class="modal-body">
            <?php $this->addModal($formMdl,$configMdl, $errors); ?>
        </div>
    </div>
</div>
<script type="text/javascript">
    window.onclick = function(event) {
        var modal = document.getElementById('modalAdd');
        if (event.target == modal) {
            modal.classList.add("hide");
        }
    }

    $('tbody').on('click','.delete',function () {
        var id =  $(this).data('id');
        var line = $(this).closest('tr');

        $.ajax({
            url      : "<?php echo DIRNAME?>"+"packs/delete/"+id,
            type     : "GET",
            success  : function(response) {

                var response = JSON.parse(response);
                var status = response.status;
                var message = response.message;

                if(status ==='success'){
                    line.remove()
                }
                Toast.show(message,status);

            },
            error    : function(response) {
                console.log('error')
            }
        })
    });

    $('tbody').on('click','.edit',function () {
        id =  $(this).data('id');
        var line = $(this).closest('tr');

        $('#updatePack').removeClass('hide');
        $('#savePack').addClass('hide');

        $.ajax({
            url      : "<?php echo DIRNAME?>"+"packs/"+id,
            type     : "GET",
            success  : function(response) {
                var response = JSON.parse(response);
                var status = response.status;

                if(status ==='error'){
                    var pack = response.pack;
                    var message = response.message;
                    Toast.show(message,status); 
                } else {
                    loadPack(response.pack[0]);
                    $('.modal-header h2').text('Modification du forfait');
                    $('#modalAdd').removeClass('hide');
                }
            },
            error    : function(response) {
                console.log('error')
            }
        })
    });
    $('#add').on('click',function () {
        $('[name="title"]').val('');
        $('[name="description"]').val('');
        $('[name="price"]').val('');
        $('#modalAdd').removeClass('hide');
        $('.modal-header h2').text('Ajout d\'un forfait');

        $('#updatePack').addClass('hide');
        $('#savePack').removeClass('hide');
        
    });
    $('#cancel').on('click',function () {
        $('#modalAdd').addClass('hide');
    });
    $('#save').on('click',function () {
        var pageDescription = {
            page: {}
        };

        pageDescription.page['pageHidden'] = $('[name="pageHidden"').is(':checked') ? 1 : 0;
        pageDescription.page['pageTitle'] = $('[name="pageTitle"').val();
        pageDescription.page['pagePosition'] = $('[name="pagePosition"').val();

        $.ajax({
            url      : $('form').attr('action'),
            type     : $('form').attr('method'),
            data : {pageDscp : pageDescription},
            success  : function(response) {
                var status = JSON.parse(response).status;
                var message = JSON.parse(response).message;
                Toast.show(message,status)
            },
            error    : function(response) {
                console.log('error')
            }
        });

    });

    $('#savePack').on('click',function () {
        var pack = {};

        $('.modal-body [name]').each(function() {
            pack[$(this).attr('name')] = $(this).val();

        });

        $.ajax({
            url      : "<?php echo DIRNAME?>"+"packs/add",
            type     : 'POST',
            data : pack,
            success  : function(response) {
                var status = JSON.parse(response).status;
                var message = JSON.parse(response).message;
                var packs = JSON.parse(response).packs;
                Toast.show(message,status);
                if(status ==='success'){
                    $('#modalAdd').addClass('hide');
                    loadTable(packs);
                }
            },
            error    : function(response) {
                console.log('error')
            }
        });

    });

     $('#updatePack').on('click',function () {

        var pack = {};

        $('.modal-body [name]').each(function() {
            pack[$(this).attr('name')] = $(this).val();
        });
        pack.id = id;

        $.ajax({
            url      : "<?php echo DIRNAME?>"+"packs/"+id,
            type     : "PUT",
            data     : pack,
            success  : function(response) {
                var response = JSON.parse(response);
                var pack = response.pack;
                var message = response.message;
                var status = response.status;
                Toast.show(message,status); 
                   
                if(status ==='success'){
                    var packs = response.packs;
                    loadTable(packs);
                    $('#modalAdd').addClass('hide');
                    $('.modal-header h2').text('Modification du forfait');
                }
                Toast.show(message,status); 
            },
            error    : function(response) {
                console.log('error')
            }
        })
    });

    function loadTable(packs){
        $('tbody').html('');
        var tab = '';
        for (var i in packs) {
            tab+='<tr>';
            tab+='<td>'+packs[i].id+'</td>';
            tab+='<td>'+packs[i].title+'</td>';
            tab+='<td>'+packs[i].price+'</td>';
            tab+='<td>';
            tab+='<button class="btn icon ion-ios-compose-outline pulse edit" data-id="'+packs[i].id+'"></button>';
            tab+='<button class="btn ion-ios-trash-outline delete pulse" data-id="'+packs[i].id+'"></button>';
            tab+='</td>';
            tab+='</tr>';
        }
        $('tbody').html(tab);
    }

    function loadPack(pack){
        $('[name="title"]').val(pack.title);  
        $('[name="description"]').val(pack.description);
        $('[name="price"]').val(pack.price);
    }
</script>