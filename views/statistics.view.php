<link rel="stylesheet" type="text/css" href="<?php echo DIRCSS; ?>view_statistics.css">

<main>

	<div class="container-fluid">
		<section class="statistics">
				
			<article class="titleStatistics">
				<div class="row">
					<div class="title col-sm-12">
						<h2>STATISTIQUES</h2>
					</div>
				</div>
			</article>

			<article class="dataStatistics">
				<div class="row">

					<div class='col-lg-3 col-md-6'>
						<div class='card'>
							<h3>Nombre d'inscriptions</h3>
							<div id='dataNumberUsers'>
								<div class="tab">
								  <button class="tablinks allTab">Tout</button>
								  <button class="tablinks yearTab">Cette année</button>
								  <button class="tablinks monthTab">Ce mois</button>
								  <button class="tablinks todayTab">Aujourd'hui</button>
								</div>
								<div class="tabcontent allTabcontent">
									<p class='titleNumber'>Nombre d'inscriptions depuis toujours :</p>
									<p class='textNumber'><?php echo $dataSignUp['all']; ?></p>
								</div>
								<div class="tabcontent yearTabcontent tabcontentHide">
									<p class='titleNumber'>Nombre d'inscriptions cette année :</p>
									<p class='textNumber'><?php echo $dataSignUp['year']; ?></p>
								</div>
								<div class="tabcontent monthTabcontent tabcontentHide">
									<p class='titleNumber'>Nombre d'inscriptions ce mois-ci :</p>
									<p class='textNumber'><?php echo $dataSignUp['month']; ?></p>
								</div>
								<div class="tabcontent todayTabcontent tabcontentHide">
									<p class='titleNumber'>Nombre d'inscriptions aujourd'hui :</p>
									<p class='textNumber'><?php echo $dataSignUp['today']; ?></p>
								</div>
							</div>
						</div>
					</div>

					<div class='col-lg-3 col-md-6'>
						<div class='card'>
							<h3>Nombre d'utilisateurs par type de permis</h3>
							<div id='dataUsersLicense'>
								<div class="tab">
								  <button class="tablinks allTab">Tout</button>
								  <button class="tablinks yearTab">Cette année</button>
								  <button class="tablinks monthTab">Ce mois</button>
								  <button class="tablinks todayTab">Aujourd'hui</button>
								</div>
								<div class="tabcontent allTabcontent">
									<p>Nombre d'utilisateurs par type de permis depuis toujours :</p>
									<canvas id="canvasAll" class="canvas" width="150" height="150"></canvas>
								</div>
								<div class="tabcontent yearTabcontent tabcontentHide">
									<p>Nombre d'utilisateurs par type de permis cette année :</p>
									<canvas id="canvasYear" class="canvas" width="150" height="150"></canvas>
								</div>
								<div class="tabcontent monthTabcontent tabcontentHide">
									<p>Nombre d'utilisateurs par type de permis ce mois-ci :</p>
									<canvas id="canvasMonth" class="canvas" width="150" height="150"></canvas>
								</div>
								<div class="tabcontent todayTabcontent tabcontentHide">
									<p>Nombre d'utilisateurs par type de permis aujourd'hui :</p>
									<canvas id="canvasToday" class="canvas" width="150" height="150"></canvas>
								</div>
							</div>
						</div>
					</div>

					<div class='col-lg-3 col-md-6'>
						<div class='card'>
							<h3>Nombre de commentaires</h3>
							<div id='dataNumberFeedback'>
								<div class="tab">
								  <button class="tablinks allTab">Tout</button>
								  <button class="tablinks yearTab">Cette année</button>
								  <button class="tablinks monthTab">Ce mois</button>
								  <button class="tablinks todayTab">Aujourd'hui</button>
								</div>
								<div class="tabcontent allTabcontent">
									<p class='titleNumber'>Nombre de commentaires depuis toujours :</p>
									<p class='textNumber'><?php echo $dataFeedback['all']; ?></p>
								</div>
								<div class="tabcontent yearTabcontent tabcontentHide">
									<p class='titleNumber'>Nombre de commentaires cette année :</p>
									<p class='textNumber'><?php echo $dataFeedback['year']; ?></p>
								</div>
								<div class="tabcontent monthTabcontent tabcontentHide">
									<p class='titleNumber'>Nombre de commentaires ce mois-ci :</p>
									<p class='textNumber'><?php echo $dataFeedback['month']; ?></p>
								</div>
								<div class="tabcontent todayTabcontent tabcontentHide">
									<p class='titleNumber'>Nombre de commentaires aujourd'hui :</p>
									<p class='textNumber'><?php echo $dataFeedback['today']; ?></p>
								</div>
							</div>
						</div>
					</div>

					<div class='col-lg-3 col-md-6'>
						<div class='card'>
							<h3>Nombre de visiteurs</h3>
							<div id='dataNumberVisitor'>
								<div class="tab">
								  <button class="tablinks allTab">Tout</button>
								  <button class="tablinks yearTab">Cette année</button>
								  <button class="tablinks monthTab">Ce mois</button>
								  <button class="tablinks todayTab">Aujourd'hui</button>
								</div>
								<div class="tabcontent allTabcontent">
									<p class='titleNumber'>Nombre de visiteur depuis toujours :</p>
									<p class='textNumber'><?php echo $dataVisitor['all']; ?></p>
								</div>
								<div class="tabcontent yearTabcontent tabcontentHide">
									<p class='titleNumber'>Nombre de visiteur cette année :</p>
									<p class='textNumber'><?php echo $dataVisitor['year']; ?></p>
								</div>
								<div class="tabcontent monthTabcontent tabcontentHide">
									<p class='titleNumber'>Nombre de visiteur ce mois-ci :</p>
									<p class='textNumber'><?php echo $dataVisitor['month']; ?></p>
								</div>
								<div class="tabcontent todayTabcontent tabcontentHide">
									<p class='titleNumber'>Nombre de visiteur aujourd'hui :</p>
									<p class='textNumber'><?php echo $dataVisitor['today']; ?></p>
								</div>
							</div>
						</div>
					</div>

				</div>
			</article>

		</section>
	</div>

</main>

<script type="text/javascript" src="<?php echo DIRJS; ?>view_statistics.js"></script>
<script type="text/javascript" src="<?php echo DIRJS; ?>Chart.min.js"></script>
<script type="text/javascript" src="<?php echo DIRJS; ?>jquery-3.3.0.min.js"></script>
