<div class="row">
    <?php if(!isset($noPacks)) : ?>
        <?php foreach ($packs as $pack): ?>

        <div class="card">
            <div class="card-header">
                <h3><?php echo $pack->getTitle();?></h3>
            </div>
            <div class="card-content">
                <?php echo nl2br($pack->getDescription());?>
            </div>
            <div class="card-footer">
                <h3><?php echo $pack->getPrice();?></h3>
            </div>
        </div>
        <?php endforeach; ?>
    <?php else : ?>
        <div class="row">
            Aucune offre disponible
        </div>
    <?php endif; ?>
</div>