<main class="container">
    <div class="form-tab">
        <h1>REGLAGES</h1>
        <?php $this->addModal($form,$config, $errors,$values); ?>
    </div>
</main>
<script>
    $('#save').on('click',function () {
        var method = $('form').attr('method');
        var url = $('form').attr('action');
        var settings ={};

        $('form [name]').each(function() {
            settings[$(this).attr('name')] = $(this).val();
        });

        $.ajax({
            url      : url,
            type     : method,
            data : settings,
            success  : function(response) {
                var status = JSON.parse(response).status;
                var message = JSON.parse(response).message;
                Toast.show(message,status)
            },
            error    : function(response) {
                console.log('error')
            }
        });
    });

</script>