<main class="container">
	<h1>Installation</h1>
    <?php $this->addModal($form,$config, null,$values); ?>
	<label style ="display:none" class="error" id="error"></label>
</main>
<img style ="display:none" src="resource/images/spinner.svg">
<div style ="display:none"id="message">
    <p>Votre application possède actuellement un seul utilisateur qui est administrateur à partir de ce compte vous pourrez créer tous les autres</p>
    <section>
        <h2>Indetifiant</h2>
        <span>admin@admin.com</span>
    </section>
    <section>
        <h2>Mot de passe</h2>
        <span>Root123</span>
    </section>
    <h1> Pour lancer l'application veuillez recharger la page </h1>
</div>
<script type="text/javascript">
	 $('#save').on('click',function () {
	 	$('img').show();
		var url = $('form').attr('action');
		var form = {};

		$('form [name]').each(function() {
			form[$(this).attr('name')] = $(this).val();
		});

		$.ajax({
            url      : url,
            type     : 'POST',
            data : form,
            success  : function(response) {

                var status = JSON.parse(response).status;
                var message = JSON.parse(response).message;

                if(status == 'error'){
                	$('#error').text(message);
                	$('#error').show();
                } else {
                	$('#error').hide();
                	$('#message').show();
                }

	 			$('img').hide();
            },
            error    : function(response) {
	 			$('img').hide();

                console.log('error')
            }
        });

	});
</script>