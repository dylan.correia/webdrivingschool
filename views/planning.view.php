<link rel="stylesheet" type="text/css" href="<?php echo DIRCSS; ?>view_planning.css">
<main>

	<div class="container-fluid">
		<section class="planning">

			<article class="titlePlanning">
				<div class="row">
					<div class="title col-sm-3">
						<h2>PLANNING</h2>
					</div>
					<div class="offset-sm-6"></div>
					<div class="col-sm-3">
						<p class='weekMoove'>
							<?php if($week == '1'): ?>
								<span class='weekChange weekBack' id='week_52_<?php echo ($year-1); ?>'><</span>
								<span class='weekCurrently'>Semaine : <?php echo $week; ?></span>
								<span class='weekChange weekAfter' id='week_<?php echo ($week+1) . "_" . $year; ?>'>></span>
							<?php elseif($week == '52'): ?>
								<span class='weekChange weekBack' id='week_<?php echo ($week-1) . "_" . $year; ?>'><</span>
								<span class='weekCurrently'>Semaine : <?php echo $week; ?></span>
								<span class='weekChange weekAfter' id='week_1_<?php echo ($year+1); ?>'>></span>
							<?php else: ?>
								<span class='weekChange weekBack' id='week_<?php echo ($week-1) . "_" . $year; ?>'><</span>
								<span class='weekCurrently'>Semaine : <?php echo $week; ?></span>
								<span class='weekChange weekAfter' id='week_<?php echo ($week+1) . "_" . $year; ?>'>></span>
							<?php endif; ?>
						</p>
					</div>
				</div>
			</article>

			<article class="dataPlanning">
				<?php if(isset($planning)): ?>

					<div class='planningOwn'>
						<?php if(isset($role) && isset($firstname) && isset($lastname)): ?>
							<p id='user_<?php echo $idUser; ?>' class='ownerPlanning'><?php echo $lastname . ' ' . $firstname . ' (' . $role . ')'; ?></p>
						<?php else: ?>
							<p class='ownerPlanning'>Planning général</p>
						<?php endif; ?>
					</div>

					<div id="planning">
						<!-- Création du planning avec les valeurs -->
						<table>
							<tr>
								<td class='menuPlanning'>Horaire</td>

								<?php
								$tabWeeks = [];
								//Ajout de l'en-tête avec la date et le jour de la semaine
								$dateFirst = new DateTime($data->start);
								$dateToday = new DateTime($data->today);
								$dateEnd = new DateTime($data->end);
								$iterator = 0;

								while($dateFirst <= $dateEnd){

									if($dateFirst == $dateToday){
										?><td class='menuPlanning today'><?php echo $days[$iterator] . ' ' . $dateFirst->format('d/m/y'); ?></td><?php
									}
									else{
										?><td class='menuPlanning'><?php echo $days[$iterator] . ' ' . $dateFirst->format('d/m/y'); ?></td><?php
									}

									$tabWeeks[] = $dateFirst->format('d/m/Y');
									$dateFirst->modify('+1 day');
									$iterator++;
								}
								?>

							</tr>

						<?php if(isset($role) && isset($firstname) && isset($lastname)): ?>

							<?php foreach($planning as $key => $value): ?>
								<tr id='plannigDay<?php echo $key; ?>'>

								<td class='menuPlanning'><?php echo $key; ?></td>
								<?php foreach($value as $keyDay => $valueDay): ?>
									<td id='plannigDay<?php echo $keyDay; ?>' class='boxPlanning'>

										<?php foreach($valueDay as $keyData => $valueData): ?>
											<?php echo '<p title="' . $valueData->lastname . ' ' . $valueData->firstname . '">' . substr($valueData->lastname, 0, 1) . '. ' . $valueData->firstname . '</p>' ?>
										<?php endforeach; ?>

									</td>
								<?php endforeach; ?>

								</tr>
							<?php endforeach; ?>

						<?php else: ?>
							<?php $numberMonitor = (isset($numberMonitor))? $numberMonitor[0]['numberUser'] : 0 ?>

							<?php foreach($planning as $key => $value): ?>
								<tr id='plannigDay<?php echo $key; ?>'>

								<td class='menuPlanning'><?php echo $key; ?></td>

								<?php $iteratorWeek = 0; ?>
								<?php foreach($value as $keyDay => $valueDay): ?>
									<?php 
									$monitorSingle = [];
									foreach($valueDay as $keyData => $valueData){
										if(!in_array($valueData->id_monitor ,$monitorSingle)){
											$monitorSingle[] = $valueData->id_monitor;
										}
									}
									?>

									<?php
									//Remplie -> Plus possible de mettre des heures car tous les moniteurs sont pris (Vert)
									if(count($monitorSingle) >= $numberMonitor || $numberMonitor == 0){
										$class = 'planningRed';
									}
									//Personne sur le créneau alors que des moniteurs sont dispos (Blanc)
									elseif(count($monitorSingle) == 0 && $numberMonitor > 0){
										$class = 'planningWhite';
									}
									//Reste des places (Vert)
									else{
										$class = 'planningGreen';
									}
									?>
									
									<td class='<?php echo $class; ?> boxPlanning' id='plannigDay<?php echo $keyDay; ?>'>
										<div class='popin' id='popin_<?php echo $key . "_" . $keyDay; ?>'>
											<p class='popinInfo popinDate'>Date : <span class='dateTimeslot'><?php echo $tabWeeks[$iteratorWeek]; ?></span></p>
											<p class='popinInfo popinHours'>Heure : <span class='hoursTimeslot'><?php echo $key . " - " . ((int)(explode("h", $key)[0])+1) . "h00"; ?></span></p>
											<?php 
											$monitorLastname = ''; 
											$monitorFirstname = '';
											?>
											<?php foreach($valueDay as $keyData => $valueData): ?>

												<?php if($monitorLastname != $valueData->lastname_monitor && $monitorFirstname != $valueData->firstname_monitor): ?>
													<strong><p><?php echo $valueData->lastname_monitor . ' ' . $valueData->firstname_monitor . " (Moniteur)"; ?></p></strong>
													<?php
													$monitorLastname = $valueData->lastname_monitor;
													$monitorFirstname = $valueData->firstname_monitor;
													?>
												<?php endif; ?>

												<p><?php echo $valueData->lastname . ' ' . $valueData->firstname . " (Élève)" ?><i id='delete_<?php echo $valueData->id; ?>' class="iconClose ion-close"></i></p>
											<?php endforeach; ?>
											<?php if($class != "planningRed"): ?>
												<p class='addTimeslot'><i class="iconAdd ion-ios-plus-outline"></i></p>
											<?php endif; ?>
										</div>
									</td>
									<?php $iteratorWeek++; ?>
								<?php endforeach; ?>

								</tr>
							<?php endforeach; ?>

						<?php endif; ?>

						</table>
					</div>

				<?php endif; ?>

				<?php if(isset($allMonitor) && isset($allStudent)): ?>
					<div id='addTimeslot'>
						<div class='titleAddTimeslot'>Ajout horaire</div>
						<div id='dateTimeslot' class='popinInfo'></div>
						<div id='hoursTimeslot' class='popinInfo'></div>

						<div class='monitorTimeslotBlock'>
							<label>Moniteur : </label>
							<select id='monitorTimeslot'>
								<option value="">Choisissez un moniteur</option>
								<?php foreach($allMonitor as $key => $value): ?>
									<option value="<?php echo $value['id']; ?>"><?php echo $value['lastname'] . " " . $value['firstname']; ?></option>
								<?php endforeach; ?>
							</select>
						</div>

						<div class='studentTimeslotBlock'>
							<label>Élève : </label>
							<select id='studentTimeslot'>
								<option value="">Choisissez un élève</option>
								<?php foreach($allStudent as $key => $value): ?>
									<option value="<?php echo $value['id']; ?>"><?php echo $value['lastname'] . " " . $value['firstname']; ?></option>
								<?php endforeach; ?>
							</select>
						</div>
						<div class='buttonTimeslotBlock'>
							<button id='cancelAddTimeslot' class='button-primary'>Annuler</button>
							<button id='validateAddTimeslot' class='button-primary'>Valider</button>
						</div>
					</div>
				<?php endif; ?>

			</article>

		</section>
	</div>

</main>

<script type="text/javascript" src="<?php echo DIRJS; ?>view_planning.js"></script>