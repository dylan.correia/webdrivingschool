<div class="container">
    <div class="form-tab">

        <div class="row">
            <h1>THEMES</h1>
        </div>
        <div class="row">
            <?php foreach ($themes as $theme): ?>
                <div class="theme-box">
                    <h2><?php echo $theme->getName() ?></h2>
                    <button id="<?php echo $theme->getId() ?>" class="see button-primary">VOIR</button>
                </div>
            <?php endforeach; ?>
        </div>
    </div>
</div>
<div id="modal-theme" class="modal hide">
    <div class="modal-content">
        <div class="modal-header">
            <i class="icon ion-ios-close" class="col-4" onclick="closeModal()"></i>
            <h2 id="titleTheme" class="col-8"></h2>
        </div>

        <div class="modal-body">
                <p id="descriptionTheme"></p>
                <div class="miniature">
                    <img id="imageTheme" src="<?php echo DIRIMAGES; ?>themes">
                </div>
                <button id="chooseTheme" class="btn button-primary">Choisir</button>
        </div>
    </div>
</div>
<script type="text/javascript">
    var src = $('#imageTheme').attr('src');
    $('.see').on('click',function (){

        id = $(this).attr('id');

        $.ajax({
            url      : "<?php echo DIRNAME ?>"+"themes/"+id,
            type     : "GET",
            success  : function(response) {
                if(response){
                    response = JSON.parse(response);

                    $('#titleTheme').text(response.name);
                    $('#descriptionTheme').text(response.description);
                    // var src = $('#imageTheme').attr('src');
                    $('#imageTheme').attr('src',src+'/'+response.image);
                }
            },
            error    : function(response) {
                console.log('error')
            }
        });
        $('#modal-theme').show();
    });
    $('#chooseTheme').on('click',function (){

        $.ajax({
            url      : "<?php echo DIRNAME ?>"+"themes/"+id,
            type     : "PUT",
            success  : function(response) {
                var status = JSON.parse(response).status;
                var message = JSON.parse(response).message;
                Toast.show(message,status)
            },
            error    : function(response) {
                console.log('error')
            }
        });
        closeModal()
    });

    window.onclick = function(event) {
        var modal = document.getElementById('modal-theme');
        if (event.target == modal) {
            modal.style.display = "none";
        }
    }
</script>
