-- phpMyAdmin SQL Dump
-- version 4.7.4
-- https://www.phpmyadmin.net/
--
-- Hôte : 127.0.0.1
-- Généré le :  mar. 24 juil. 2018 à 23:38
-- Version du serveur :  10.1.29-MariaDB
-- Version de PHP :  7.2.0

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données :  `install_test`
--

-- --------------------------------------------------------

--
-- Structure de la table `ability`
--


DROP TABLE IF EXISTS `ability`;
CREATE TABLE `ability` (
  `id` int(11) NOT NULL,
  `controller` varchar(25) CHARACTER SET utf8 NOT NULL,
  `method` varchar(25) CHARACTER SET utf8 NOT NULL,
  `id_permission` varchar(100) CHARACTER SET utf8 NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `ability`
--

INSERT INTO `ability` (`id`, `controller`, `method`, `id_permission`) VALUES
(10, 'Page', 'create', 'pagesRight'),
(11, 'Page', 'add', 'pagesRight'),
(12, 'Page', 'delete', 'pagesRight'),
(13, 'Page', 'index', 'pagesRight'),
(14, 'Page', 'show', 'pagesRight'),
(15, 'Page', 'update', 'pagesRight'),
(16, 'Statistic', 'index', 'statisticsRight'),
(17, 'Statistic', 'chart', 'statisticsRight'),
(18, 'Setting', 'index', 'settingsRight'),
(19, 'Setting', 'update', 'settingsRight'),
(20, 'Dashboard', 'index', 'dashboardRight'),
(21, 'User', 'index', 'usersRight'),
(22, 'User', 'show', 'usersRight'),
(23, 'User', 'create', 'usersRight'),
(24, 'User', 'add', 'usersRight'),
(25, 'User', 'delete', 'usersRight'),
(26, 'User', 'update', 'usersRight'),
(27, 'User', 'search', 'usersRight'),
(28, 'Role', 'index', 'rolesRight'),
(29, 'Role', 'show', 'rolesRight'),
(30, 'Role', 'add', 'rolesRight'),
(31, 'Role', 'create', 'rolesRight'),
(32, 'Role', 'delete', 'rolesRight'),
(33, 'Role', 'update', 'rolesRight'),
(34, 'Planning', 'myPlanning', 'myPlanningRight'),
(35, 'Planning', 'show', 'planningRight'),
(36, 'Planning', 'index', 'planningRight'),
(37, 'Planning', 'create', 'planningRight'),
(38, 'TimeSlot', 'index', 'timeslotRight'),
(39, 'TimeSlot', 'show', 'timeslotRight'),
(40, 'TimeSlot', 'add', 'timeslotRight'),
(41, 'TimeSlot', 'create', 'timeslotRight'),
(42, 'TimeSlot', 'delete', 'timeslotRight'),
(43, 'TimeSlot', 'update', 'timeslotRight'),
(44, 'Progress', 'index', 'progressionsRight'),
(45, 'Progress', 'show', 'progressionsRight'),
(46, 'Progress', 'add', 'progressionsRight'),
(47, 'Progress', 'create', 'progressionsRight'),
(48, 'Progress', 'delete', 'progressionsRight'),
(49, 'Progress', 'update', 'progressionsRight'),
(50, 'Progress', 'myprogress', 'progressionsRight'),
(51, 'Feedback', 'index', 'feedbackRight'),
(52, 'Feedback', 'add', 'feedbackRight'),
(53, 'Feedback', 'create', 'feedbackRight'),
(54, 'Feedback', 'delete', 'feedbackRight'),
(55, 'Feedback', 'update', 'feedbackRight'),
(56, 'Feedback', 'myfeedback', 'myFeedbackRight'),
(57, 'Theme', 'index', 'themesRight'),
(58, 'Theme', 'show', 'themesRight'),
(59, 'Theme', 'update', 'themesRight'),
(60, 'Planning', 'navigation', 'planningRight'),
(61, 'Planning', 'addtimeslot', 'planningRight'),
(62, 'Planning', 'removetimeslot', 'planningRight'),
(63, 'Planning', 'showAdmin', 'timeslotRight'),
(64, 'Progress', 'myprogress', 'myProgessionRight'),
(65, 'Feedback', 'show', 'feedbackRight'),
(66, 'Progress', 'list', 'progressionsRight'),
(67, 'Pack', 'delete', 'pagesRight'),
(68, 'Pack', 'show', 'pagesRight'),
(69, 'Pack', 'update', 'pagesRight');

-- --------------------------------------------------------

--
-- Structure de la table `content`
--

DROP TABLE IF EXISTS `content`;
CREATE TABLE `content` (
  `id` int(11) NOT NULL,
  `key` varchar(100) DEFAULT NULL,
  `value` longtext,
  `id_section` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `content`
--

INSERT INTO `content` (`id`, `key`, `value`, `id_section`) VALUES
(1, 'bannerLogo', 'bannerLogo.svg', 1),
(3, 'bannerSlogan', 'Votre permis Ã  porter de main', 1),
(4, 'bannerHidden', '1', 1),
(5, 'dscpTitle', 'NOTRE SERVICE', 2),
(6, 'dscpText', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.', 2),
(7, 'dscpImage', 'dscpImage.svg', 2),
(8, 'dscpHidden', '0', 2),
(9, 'card1Title', 'PERMIS B', 3),
(10, 'card1Dscp', '50 heures de code\n25 heures de conduite\n8 moniteurs Ã  l\'Ã©coute\n1 diplome rÃ©ussi', 3),
(11, 'card1Price', '1199â‚¬', 3),
(12, 'card2Title', 'PERMIS A', 3),
(13, 'card2Dscp', '60 heures de code\n35 heures de conduite\n8 moniteurs Ã  l\'Ã©coute\n1 diplome rÃ©ussi', 3),
(14, 'card2Price', '1399â‚¬', 3),
(17, 'card3Title', 'PERMIS C', 3),
(18, 'card3Dscp', '40 heures de code\n20 heures de conduite\n8 moniteurs Ã  l\'Ã©coute\n1 diplome rÃ©ussi', 3),
(19, 'card3Price', '1399â‚¬', 3),
(20, 'packHidden', '0', 3),
(23, 'commentTitle', 'COMMENTAIRES', 4),
(24, 'commentHidden', '0', 4),
(25, 'tmsTitle', ' ADRESSE ET HORAIRES', 5),
(26, 'tmsDscp', '  Notre agence se trouve au 135 avenue de la rÃ©publique, 93300 aubervilliers', 5),
(27, 'tmsMon', '8h-12h/14-20h', 5),
(28, 'tmsTue', '8h-12h/14-20h', 5),
(29, 'tmsWed', '8h-12h/14-20h', 5),
(30, 'tmsThu', '8h-12h/14-20h', 5),
(31, 'tmsFri', '8h-12h/14-20h', 5),
(32, 'tmsSat', '10h-12h/14h-18h', 5),
(33, 'tmsFri', '8h-12h/14-20h', 5),
(34, 'tmsSat', '10h-12h/14h-18h', 5),
(37, 'tmsSun', '10h-12h/14h-18h', 5),
(38, 'tmsHidden', '0', 5),
(40, 'commentTitle', 'COMMENTAIRES', 4),
(41, 'tinymce', 'Ecrivez les metions légales', 6);

-- --------------------------------------------------------

--
-- Structure de la table `feedback`
--

DROP TABLE IF EXISTS `feedback`;
CREATE TABLE `feedback` (
  `id` int(11) NOT NULL,
  `description` varchar(500) DEFAULT NULL,
  `status` varchar(20) NOT NULL,
  `id_student` int(11) NOT NULL,
  `date_inserted` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `deleted` TINYINT NOT NULL

) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure de la table `pack`
--

DROP TABLE IF EXISTS `pack`;
CREATE TABLE `pack` (
  `id` int(11) NOT NULL,
  `title` varchar(100) CHARACTER SET utf8 NOT NULL,
  `description` longtext CHARACTER SET utf8 NOT NULL,
  `price` varchar(100) CHARACTER SET utf8 NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `pack`
--

INSERT INTO `pack` (`id`, `title`, `description`, `price`) VALUES
(11, 'PERMIS A', '40 heures de code\n20 heures de conduite\n8 moniteurs Ã  l\'Ã©coute\n1 diplÃ´me rÃ©ussi', '1000â‚¬'),
(12, 'PERMIS B', '40 heures de code\n20 heures de conduite\n8 moniteurs Ã  l\'Ã©coute\n1 diplome rÃ©ussi', '1500â‚¬'),
(13, 'PERMIS A (30H)', '30 heures de code\n30 heures de conduite\n8 moniteurs Ã Â  l\'Ã©coute\n1 diplÃ´me rÃ©ussi', '1300â‚¬'),
(22, 'mm', '70 heures\n30 mdmdmd\ndldldlm', '1000â‚¬'),
(23, 'PERMIS H', '40 heures de code\n20 heures de conduite\n8 moniteurs Ã  l\'Ã©coute\n1 diplome rÃ©ussi', '900â‚¬');

-- --------------------------------------------------------

--
-- Structure de la table `page`
--

DROP TABLE IF EXISTS `page`;
CREATE TABLE `page` (
  `id` int(11) NOT NULL,
  `slug` varchar(100) NOT NULL,
  `title` varchar(100) NOT NULL,
  `status` varchar(50) DEFAULT NULL,
  `position` int(11) NOT NULL,
  `date_inserted` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `date_updated` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `hidden` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `page`
--

INSERT INTO `page` (`id`, `slug`, `title`, `status`, `position`, `date_inserted`, `date_updated`, `hidden`) VALUES
(1, 'accueil', 'ACCUEIL', 'UPDATABLE', 1, '2018-04-13 18:19:51', '2018-06-24 18:04:11', 0),
(2, 'forfaits', 'FORFAITS', 'UPDATABLE', 2, '2018-04-13 18:19:51', '2018-06-30 20:55:34', 0),
(3, 'commentaires', 'COMMENTAIRES', 'UPDATABLE', 3, '2018-04-15 10:29:11', '2018-06-30 17:23:08', 0),
(4, 'contact', 'CONTACT', 'UPDATABLE', 4, '2018-04-15 10:30:14', '2018-06-30 17:19:55', 0),
(5, 'mentionslegales', 'MENTIONS LEGALES', 'UPDATABLE', 99999, '2018-04-15 10:29:11', '2018-04-15 10:29:11', 1);

-- --------------------------------------------------------

--
-- Structure de la table `permission`
--

DROP TABLE IF EXISTS `permission`;
CREATE TABLE `permission` (
  `id` varchar(100) NOT NULL,
  `title` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `permission`
--

INSERT INTO `permission` (`id`, `title`) VALUES
('dashboardRight', 'Visualiser le dashboard'),
('depositTimeSlotRight', 'Déposer un horaire'),
('feedbackRight', 'Valider un commentaire'),
('myFeedbackRight', 'Gérer ses commentaires'),
('myPlanningRight', 'Accéder à son planning'),
('myProgessionRight', 'Voir ses progressions'),
('pagesRight', 'Gérer les pages'),
('planningRight', 'Accéder au planning d’un élève'),
('progressionsRight', 'Gérer les progressions d’un élève'),
('rolesRight', 'Gérer un rôle'),
('settingsRight', 'Gérer les réglages'),
('statisticsRight', 'Voir les statistiques'),
('themesRight', 'Gérer les thèmes'),
('timeslotRight', 'Gérer les horaires'),
('usersRight', 'Gérer les utilisateurs');

-- --------------------------------------------------------

--
-- Structure de la table `progress`
--


DROP TABLE IF EXISTS `progress`;
CREATE TABLE `progress` (
  `id` int(11) NOT NULL,
  `comment` longtext,
  `title` varchar(150) NOT NULL,
  `date_inserted` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `date_updated` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `id_timeslot` int(11) NOT NULL,
  `id_monitor` int(11) NOT NULL,
  `student_show` int(1),
  `hidden` int(1)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure de la table `role`
--

DROP TABLE IF EXISTS `role`;
CREATE TABLE `role` (
  `id` int(11) NOT NULL,
  `name` varchar(100) NOT NULL,
  `description` longtext
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


--
-- Déchargement des données de la table `role`
--

INSERT INTO `role` (`id`, `name`, `description`) VALUES
(1, 'Administrateur', 'Utilisateur possÃ©dant tous les droits'),
(2, 'Secretaire', 'RÃ´le d\'une secrÃ©taire'),
(4, 'ElÃ¨ve', 'test');

-- --------------------------------------------------------

--
-- Structure de la table `role_permission`
--

DROP TABLE IF EXISTS `role_permission`;
CREATE TABLE `role_permission` (
  `id` int(11) NOT NULL,
  `id_role` int(11) NOT NULL,
  `id_permission` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `role_permission`
--

INSERT INTO `role_permission` (`id`, `id_role`, `id_permission`) VALUES
(240, 1, 'dashboardRight'),
(241, 1, 'feedbackRight'),
(242, 1, 'pagesRight'),
(243, 1, 'planningRight'),
(244, 1, 'progressionsRight'),
(245, 1, 'rolesRight'),
(246, 1, 'settingsRight'),
(247, 1, 'statisticsRight'),
(248, 1, 'themesRight'),
(249, 1, 'timeslotRight'),
(250, 1, 'usersRight'),
(251, 2, 'planningRight'),
(252, 2, 'progressionsRight'),
(253, 2, 'timeslotRight'),
(254, 2, 'usersRight'),
(255, 4, 'depositTimeSlotRight'),
(256, 4, 'myFeedbackRight'),
(257, 4, 'myPlanningRight'),
(258, 4, 'myProgessionRight');

-- --------------------------------------------------------

--
-- Structure de la table `section`
--

DROP TABLE IF EXISTS `section`;
CREATE TABLE `section` (
  `id` int(11) NOT NULL,
  `title` varchar(45) DEFAULT NULL,
  `hidden` tinyint(1) DEFAULT NULL,
  `id_page` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `section`
--

INSERT INTO `section` (`id`, `title`, `hidden`, `id_page`) VALUES
(1, 'banner', 0, 1),
(2, 'description', 0, 1),
(3, 'pack', 0, 1),
(4, 'comment', 0, 1),
(5, 'timeslot', 0, 1),
(6, 'templatePage', 0, 5);

-- --------------------------------------------------------

--
-- Structure de la table `setting`
--

DROP TABLE IF EXISTS `setting`;
CREATE TABLE `setting` (
  `id` int(11) NOT NULL,
  `name` varchar(150) NOT NULL,
  `value` varchar(500) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `setting`
--

INSERT INTO `setting` (`id`, `name`, `value`) VALUES
(1, 'sitename', 'Mon site'),
(2, 'mailcompany', 'contact@newdriving.fr'),
(3, 'phone', '0132323232'),
(4, 'instagram', 'https://www.instagram.com/esgiparis/?hl=fr'),
(5, 'facebook', 'https://fr-fr.facebook.com/ESGIParis/'),
(6, 'twitter', 'https://twitter.com/esgi?lang=fr');

-- --------------------------------------------------------

--
-- Structure de la table `theme`
--

DROP TABLE IF EXISTS `theme`;
CREATE TABLE `theme` (
  `id` int(11) NOT NULL,
  `name` varchar(150) NOT NULL,
  `description` longtext,
  `choosen` tinyint(1) DEFAULT NULL,
  `image` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `theme`
--

INSERT INTO `theme` (`id`, `name`, `description`, `choosen`, `image`) VALUES
(1, 'wild-watermelon', 'Un thÃ¨me glamour et soft', 1, 'wild-watermelon.png'),
(2, 'blue-sky', 'Un thÃ¨me au couleur du ciel donnant vie Ã  votre contenu ! ', 0, 'blue-sky.png');

-- --------------------------------------------------------

--
-- Structure de la table `timeslot`
--

DROP TABLE IF EXISTS `timeslot`;
CREATE TABLE `timeslot` (
  `id` int(11) NOT NULL,
  `hour` varchar(255) NOT NULL,
  `day` date NOT NULL,
  `id_student` int(11) NOT NULL,
  `id_monitor` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure de la table `user`
--

DROP TABLE IF EXISTS `user`;
CREATE TABLE `user` (
  `id` int(11) NOT NULL,
  `lastname` varchar(50) NOT NULL,
  `firstname` varchar(50) NOT NULL,
  `email` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `address` varchar(255) NOT NULL,
  `city` varchar(100) NOT NULL,
  `postcode` varchar(5) NOT NULL,
  `token` varchar(255) NOT NULL,
  `birth_date` date NOT NULL,
  `type_license` varchar(45) NOT NULL,
  `picture` varchar(255) DEFAULT NULL,
  `hours_driving_license` varchar(45) NOT NULL,
  `last_connection` timestamp(5) NOT NULL DEFAULT CURRENT_TIMESTAMP(5) ON UPDATE CURRENT_TIMESTAMP(5),
  `date_inserted` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `date_updated` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `id_role` int(11) NOT NULL,
  `hidden` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `user`
--

INSERT INTO `user` (`id`, `lastname`, `firstname`, `email`, `password`, `address`, `city`, `postcode`, `token`, `birth_date`, `type_license`, `picture`, `hours_driving_license`, `last_connection`, `date_inserted`, `date_updated`, `id_role`, `hidden`) VALUES
(3, 'Super', 'Admin', 'admin@admin.com', '$2y$10$G9jTkw8U16b2mIOFVe828O04SDJQy6yKnvpAGJwduNUuLMwUF1FlK', '12 rue Charge de Gaulle', 'Paris', '75010', '5b579c68352376.25263208', '2018-05-03', 'A', 'bb', '500', '2018-06-22 19:50:31.47552', '2018-05-12 17:39:51', '2018-06-22 19:50:31', 1, 0),
(4, 'TEST', 'Test', 'test@gmail.com', '$2y$10$d/QcWPCCnIePAKWpmXzCVe9snjFRU824KI5P2e7vh9G2vdK2BfTPu', 'jdiejdiejdiejdiojei', 'jdioejidojei', '91245', 'HjgJHHGjgYg564JHBjBHn', '2018-05-10', 'B', NULL, '90', '2018-05-15 16:10:37.32485', '2018-05-15 16:10:31', '2018-05-15 16:10:37', 1, 1),
(5, 'TEST', 'Dylan', 'dylan.correia@hotmail.com', '$2y$10$H6j5/dWMy6s5WmJkCkUKfO0kksi2yRbX1uvSZg0dH39.R.7chmCOW', 'nfieonfieonfoienfeio', 'Yerres', '95330', '8e7ca503939f5f48ec74adb326e267a4', '2018-06-08', 'B', NULL, '20', '2018-06-30 21:01:59.76074', '2018-06-30 16:49:47', '2018-06-30 21:01:59', 1, 1),
(6, 'DDD', 'Dylan', 'dylan.correia@hotmail.com', '$2y$10$BTDpLnTsSbUcJ2uvcoscceCYcVSinT8e.8nhPtP57otKZpzlS8LQ6', 'cdcdscds', 'Yerres', '95330', 'HjgJHHGjgYg564JHBjBHn', '2018-06-09', 'B', NULL, '20', '2018-06-30 21:09:57.16337', '2018-06-30 21:02:35', '2018-06-30 21:09:57', 1, 0),
(7, 'TEST', 'Test', 'test@gmail.com', '$2y$10$kMvqH0ZwRYvlytIJSnjA2.QYEwaLn8qPhiN32IhrOwSDGeSLxlaxO', 'jdiejdiejdiejdiojei', 'jdioejidojei', '91245', 'HjgJHHGjgYg564JHBjBHn', '2018-06-07', 'B', NULL, '20', '2018-07-01 17:05:32.78387', '2018-06-30 21:11:40', '2018-07-01 17:05:32', 1, 0);

DROP TABLE IF EXISTS `visitor`;
CREATE TABLE `visitor` (
  `id_visitor` int(11) NOT NULL,
  `ip_address` varchar(255) NOT NULL,
  `date_inserted` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


--
-- Index pour les tables déchargées
--

--
-- Index pour la table `ability`
--
ALTER TABLE `ability`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_ability_permission1` (`id_permission`);

--
-- Index pour la table `content`
--
ALTER TABLE `content`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_content_section1_idx` (`id_section`);

--
-- Index pour la table `feedback`
--
ALTER TABLE `feedback`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_feedback_user_idx` (`id_student`);

--
-- Index pour la table `pack`
--
ALTER TABLE `pack`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `page`
--
ALTER TABLE `page`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `permission`
--
ALTER TABLE `permission`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `progress`
--
ALTER TABLE `progress`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_progress_timeslot1_idx` (`id_timeslot`),
  ADD KEY `fk_progress_user1_idx` (`id_monitor`);

--
-- Index pour la table `role`
--
ALTER TABLE `role`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `role_permission`
--
ALTER TABLE `role_permission`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_role_permission_role1` (`id_role`),
  ADD KEY `fk_role_permission_permission1` (`id_permission`);

--
-- Index pour la table `section`
--
ALTER TABLE `section`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_section_page1_idx` (`id_page`);

--
-- Index pour la table `setting`
--
ALTER TABLE `setting`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `theme`
--
ALTER TABLE `theme`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `timeslot`
--
ALTER TABLE `timeslot`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_timeslot_user1_idx` (`id_student`);

--
-- Index pour la table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_user_role1_idx` (`id_role`);

--
-- AUTO_INCREMENT pour les tables déchargées
--

--
-- AUTO_INCREMENT pour la table `ability`
--
ALTER TABLE `ability`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=70;

--
-- AUTO_INCREMENT pour la table `content`
--
ALTER TABLE `content`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=49;

--
-- AUTO_INCREMENT pour la table `feedback`
--
ALTER TABLE `feedback`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=107;

--
-- AUTO_INCREMENT pour la table `pack`
--
ALTER TABLE `pack`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=24;

--
-- AUTO_INCREMENT pour la table `page`
--
ALTER TABLE `page`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;

--
-- AUTO_INCREMENT pour la table `progress`
--
ALTER TABLE `progress`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT pour la table `role`
--
ALTER TABLE `role`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT pour la table `role_permission`
--
ALTER TABLE `role_permission`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=259;

--
-- AUTO_INCREMENT pour la table `section`
--
ALTER TABLE `section`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;

--
-- AUTO_INCREMENT pour la table `setting`
--
ALTER TABLE `setting`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT pour la table `theme`
--
ALTER TABLE `theme`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT pour la table `timeslot`
--
ALTER TABLE `timeslot`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT pour la table `user`
--
ALTER TABLE `user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- Contraintes pour les tables déchargées
--

--
-- Contraintes pour la table `ability`
--
ALTER TABLE `ability`
  ADD CONSTRAINT `fk_ability_permission1` FOREIGN KEY (`id_permission`) REFERENCES `permission` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Contraintes pour la table `content`
--
ALTER TABLE `content`
  ADD CONSTRAINT `fk_content_section1` FOREIGN KEY (`id_section`) REFERENCES `section` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION;

--
-- Contraintes pour la table `feedback`
--
ALTER TABLE `feedback`
  ADD CONSTRAINT `fk_feedback_user` FOREIGN KEY (`id_student`) REFERENCES `user` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Contraintes pour la table `progress`
--
ALTER TABLE `progress`
  ADD CONSTRAINT `fk_progress_timeslot1` FOREIGN KEY (`id_timeslot`) REFERENCES `timeslot` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_progress_user1` FOREIGN KEY (`id_monitor`) REFERENCES `user` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Contraintes pour la table `role_permission`
--
ALTER TABLE `role_permission`
  ADD CONSTRAINT `fk_role_permission_permission1` FOREIGN KEY (`id_permission`) REFERENCES `permission` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_role_permission_role1` FOREIGN KEY (`id_role`) REFERENCES `role` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION;

--
-- Contraintes pour la table `section`
--
ALTER TABLE `section`
  ADD CONSTRAINT `fk_section_page1` FOREIGN KEY (`id_page`) REFERENCES `page` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION;

--
-- Contraintes pour la table `timeslot`
--
ALTER TABLE `timeslot`
  ADD CONSTRAINT `fk_timeslot_user1` FOREIGN KEY (`id_student`) REFERENCES `user` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Contraintes pour la table `user`
--
ALTER TABLE `user`
  ADD CONSTRAINT `fk_user_role1` FOREIGN KEY (`id_role`) REFERENCES `role` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
