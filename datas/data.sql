
--
-- Déchargement des données de la table `page`
--

INSERT INTO `page` (`id`, `slug`, `title`, `status`, `position`, `date_inserted`, `date_updated`, `hidden`) VALUES
(1, 'accueil', 'ACCUEIL', 'UPDATABLE', 1, '2018-04-13 18:19:51', '2018-06-24 18:04:11', 0),
(2, 'forfaits', 'FORFAITS', 'UPDATABLE', 2, '2018-04-13 18:19:51', '2018-06-30 20:55:34', 0),
(3, 'commentaires', 'COMMENTAIRES', 'UPDATABLE', 3, '2018-04-15 10:29:11', '2018-06-30 17:23:08', 0),
(4, 'contact', 'CONTACT', 'UPDATABLE', 4, '2018-04-15 10:30:14', '2018-06-30 17:19:55', 0),
(5, 'mentionslegales', 'MENTIONS LEGALES', 'UPDATABLE', '99999', '2018-04-15 10:29:11', '2018-04-15 10:29:11', 1),
(12, 'description', 'DESCRIPTION', 'DELETABLE', 5, '2018-04-29 17:06:30', '2018-06-24 18:04:11', 0),
(17, 'mdmjdl', 'mdmjdl', 'DELETABLE', 6, '2018-06-30 09:01:06', '2018-06-30 17:06:42', 0);



--
-- Déchargement des données de la table `section`
--

INSERT INTO `section` (`id`, `title`, `hidden`, `id_page`) VALUES
(1, 'banner', 0, 1),
(2, 'description', 0, 1),
(3, 'pack', 0, 1),
(4, 'comment', 0, 1),
(5, 'timeslot', 0, 1),
(6, 'templatePage', 0, 5),
(11, 'templatePage', 0, 12),
(16, 'templatePage', 0, 17);

--
-- Déchargement des données de la table `content`
--

INSERT INTO `content` (`id`, `key`, `value`, `id_section`) VALUES
(1, 'bannerLogo', 'bannerLogo.svg', 1),
(3, 'bannerSlogan', 'Votre permis Ã  porter de main', 1),
(4, 'bannerHidden', '1', 1),
(5, 'dscpTitle', 'NOTRE SERVICE', 2),
(6, 'dscpText', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.', 2),
(7, 'dscpImage', 'dscpImage.svg', 2),
(8, 'dscpHidden', '0', 2),
(9, 'card1Title', 'PERMIS B', 3),
(10, 'card1Dscp', '50 heures de code\n25 heures de conduite\n8 moniteurs Ã  l\'Ã©coute\n1 diplome rÃ©ussi', 3),
(11, 'card1Price', '1199â‚¬', 3),
(12, 'card2Title', 'PERMIS A', 3),
(13, 'card2Dscp', '60 heures de code\n35 heures de conduite\n8 moniteurs Ã  l\'Ã©coute\n1 diplome rÃ©ussi', 3),
(14, 'card2Price', '1399â‚¬', 3),
(17, 'card3Title', 'PERMIS C', 3),
(18, 'card3Dscp', '40 heures de code\n20 heures de conduite\n8 moniteurs Ã  l\'Ã©coute\n1 diplome rÃ©ussi', 3),
(19, 'card3Price', '1399â‚¬', 3),
(20, 'packHidden', '0', 3),
(23, 'commentTitle', 'COMMENTAIRES', 4),
(24, 'commentHidden', '0', 4),
(25, 'tmsTitle', ' ADRESSE ET HORAIRES', 5),
(26, 'tmsDscp', '  Notre agence se trouve au 135 avenue de la rÃ©publique, 93300 aubervilliers', 5),
(27, 'tmsMon', '8h-12h/14-20h', 5),
(28, 'tmsTue', '8h-12h/14-20h', 5),
(29, 'tmsWed', '8h-12h/14-20h', 5),
(30, 'tmsThu', '8h-12h/14-20h', 5),
(31, 'tmsFri', '8h-12h/14-20h', 5),
(32, 'tmsSat', '10h-12h/14h-18h', 5),
(33, 'tmsFri', '8h-12h/14-20h', 5),
(34, 'tmsSat', '10h-12h/14h-18h', 5),
(37, 'tmsSun', '10h-12h/14h-18h', 5),
(38, 'tmsHidden', '0', 5),
(40, 'commentTitle', 'COMMENTAIRES', 4),
(41, 'tinymce', 'Ecrivez les metions légales', 6),
(43, 'tinymce', '<p style=\"text-align: center;\"><span style=\"font-size: 14pt;\"><strong>DESCRIPTION</strong></span></p>\n<p style=\"text-align: left;\"><span style=\"font-size: 10pt;\">Voici une description de site plus que normal ceci est une description blablabalbalbalbalbalbla on test le html blablba.</span></p>\n<p><span style=\"font-size: 10pt;\"><span style=\"font-size: 13.3333px;\">&nbsp; &nbsp; &nbsp; Vous cherchez une auto-&eacute;cole qui vous permette de suivre la formation ad&eacute;quate pour obtenir votre permis auto ou permis moto ? Auto-&eacute;cole&nbsp;Auber se met &agrave; votre service. </span></span></p>\n<p><span style=\"font-size: 10pt;\"><span style=\"font-size: 13.3333px;\">&nbsp; &nbsp; Vous pouvez y suivre la formation au permis B et &agrave; la conduite accompagn&eacute;e, mais aussi la conduite acc&eacute;l&eacute;r&eacute;e. Elle vous soumet &eacute;galement ses stages moto du permis A et AM. Vous avez un projet d&rsquo;auto-&eacute;cole en vue ? L&rsquo;enseigne vous apprend les rouages du m&eacute;tier, formation au ECSR pour que vous puissiez devenir moniteur d\'auto-&eacute;cole (anciennement BEPECASER). </span></span></p>\n<p><span style=\"font-size: 10pt;\"><span style=\"font-size: 13.3333px;\">&nbsp; &nbsp; Pour cela, elle vous aide &agrave; remettre &agrave; niveau vos connaissances du code de la route et la r&egrave;glementation concernant la circulation sur route, autoroute ou en agglomeration .</span></span></p>\n<p>&nbsp;</p>\n<p><span style=\"font-size: 13.3333px;\">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod</span></p>\n<p><span style=\"font-size: 13.3333px;\"> tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,</span></p>\n<p><span style=\"font-size: 13.3333px;\"> quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo</span></p>\n<p><span style=\"font-size: 13.3333px;\"> consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse</span></p>\n<p><span style=\"font-size: 13.3333px;\"> cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non</span></p>\n<p>&nbsp;</p>\n<p><span style=\"font-size: 13.3333px;\"> proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</span></p>', 11),
(48, 'tinymce', '', 16);


INSERT INTO `role` (`id`, `name`, `description`) VALUES
(1, 'Administrateur', 'Utilisateur possÃ©dant tous les droits'),
(2, 'Secretaire', 'RÃ´le d\'une secrÃ©taire'),
(4, 'ElÃ¨ve', 'test');


--
-- Déchargement des données de la table `user`
--

INSERT INTO `user` (`id`, `lastname`, `firstname`, `email`, `password`, `address`, `city`, `postcode`, `token`, `birth_date`, `type_license`, `picture`, `hours_driving_license`, `last_connection`, `date_inserted`, `date_updated`, `id_role`, `hidden`) VALUES
(3, 'MARTIN', 'Jean', 'jeanmartin@gmail.com', '$2y$10$G9jTkw8U16b2mIOFVe828O04SDJQy6yKnvpAGJwduNUuLMwUF1FlK', '12 rue Charge de Gaulle', 'Paris', '75010', 'HjgJHHGjgYg564JHBjBHn', '2018-05-03', 'A', 'bb', '500', '2018-06-22 19:50:31.47552', '2018-05-12 17:39:51', '2018-06-22 19:50:31', 1, 0),
(4, 'TEST', 'Test', 'test@gmail.com', '$2y$10$d/QcWPCCnIePAKWpmXzCVe9snjFRU824KI5P2e7vh9G2vdK2BfTPu', 'jdiejdiejdiejdiojei', 'jdioejidojei', '91245', 'HjgJHHGjgYg564JHBjBHn', '2018-05-10', 'B', NULL, '90', '2018-05-15 16:10:37.32485', '2018-05-15 16:10:31', '2018-05-15 16:10:37', 1, 1),
(5, 'TEST', 'Dylan', 'dylan.correia@hotmail.com', '$2y$10$H6j5/dWMy6s5WmJkCkUKfO0kksi2yRbX1uvSZg0dH39.R.7chmCOW', 'nfieonfieonfoienfeio', 'Yerres', '95330', '8e7ca503939f5f48ec74adb326e267a4', '2018-06-08', 'B', NULL, '20', '2018-06-30 21:01:59.76074', '2018-06-30 16:49:47', '2018-06-30 21:01:59', 1, 1),
(6, 'DDD', 'Dylan', 'dylan.correia@hotmail.com', '$2y$10$BTDpLnTsSbUcJ2uvcoscceCYcVSinT8e.8nhPtP57otKZpzlS8LQ6', 'cdcdscds', 'Yerres', '95330', 'HjgJHHGjgYg564JHBjBHn', '2018-06-09', 'B', NULL, '20', '2018-06-30 21:09:57.16337', '2018-06-30 21:02:35', '2018-06-30 21:09:57', 1, 0),
(7, 'TEST', 'Test', 'test@gmail.com', '$2y$10$kMvqH0ZwRYvlytIJSnjA2.QYEwaLn8qPhiN32IhrOwSDGeSLxlaxO', 'jdiejdiejdiejdiojei', 'jdioejidojei', '91245', 'HjgJHHGjgYg564JHBjBHn', '2018-06-07', 'B', NULL, '20', '2018-07-01 17:05:32.78387', '2018-06-30 21:11:40', '2018-07-01 17:05:32', 1, 0);


--
-- Déchargement des données de la table `feedback`
--

INSERT INTO `feedback` (`id`, `description`, `status`, `id_student`, `date_inserted`) VALUES
(2, '\"Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod\r\n								tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,\r\n								quis nostrud exercitation.', 'VALID', 3, '2018-05-12 17:40:39'),
(3, 'helllo', 'hello', 3, '2018-05-12 17:41:01'),
(4, 'hellla', 'hello', 3, '2018-05-12 17:41:01'),
(5, 'mmm', 'mm', 3, '2018-05-12 17:45:32'),
(6, 'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod\r\n								tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,\r\n								quis nostrud exercitation.', 'nnn', 3, '2018-05-12 17:45:32'),
(7, 'lorem blbla 0', 'VALID', 3, '2018-05-14 14:08:48'),
(8, 'lorem blbla 1', 'VALID', 3, '2018-05-14 14:08:49'),
(9, 'lorem blbla 2', 'VALID', 3, '2018-05-14 14:08:49'),
(10, 'lorem blbla 3', 'VALID', 3, '2018-05-14 14:08:49'),
(11, 'lorem blbla 4', 'VALID', 3, '2018-05-14 14:08:49'),
(12, 'lorem blbla 5', 'VALID', 3, '2018-05-14 14:08:49'),
(13, 'lorem blbla 6', 'VALID', 3, '2018-05-14 14:08:49'),
(14, 'lorem blbla 7', 'VALID', 3, '2018-05-14 14:08:49'),
(15, 'lorem blbla 8', 'VALID', 3, '2018-05-14 14:08:49'),
(16, 'lorem blbla 9', 'VALID', 3, '2018-05-14 14:08:49'),
(17, 'lorem blbla 10', 'VALID', 3, '2018-05-14 14:08:49'),
(18, 'lorem blbla 11', 'VALID', 3, '2018-05-14 14:08:49'),
(19, 'lorem blbla 12', 'VALID', 3, '2018-05-14 14:08:49'),
(20, 'lorem blbla 13', 'VALID', 3, '2018-05-14 14:08:49'),
(21, 'lorem blbla 14', 'VALID', 3, '2018-05-14 14:08:49'),
(22, 'lorem blbla 15', 'VALID', 3, '2018-05-14 14:08:49'),
(23, 'lorem blbla 16', 'VALID', 3, '2018-05-14 14:08:49'),
(24, 'lorem blbla 17', 'VALID', 3, '2018-05-14 14:08:49'),
(25, 'lorem blbla 18', 'VALID', 3, '2018-05-14 14:08:49'),
(26, 'lorem blbla 19', 'VALID', 3, '2018-05-14 14:08:49'),
(27, 'lorem blbla 20', 'VALID', 3, '2018-05-14 14:08:49'),
(28, 'lorem blbla 21', 'VALID', 3, '2018-05-14 14:08:49'),
(29, 'lorem blbla 22', 'VALID', 3, '2018-05-14 14:08:49'),
(30, 'lorem blbla 23', 'VALID', 3, '2018-05-14 14:08:49'),
(31, 'lorem blbla 24', 'VALID', 3, '2018-05-14 14:08:49'),
(32, 'lorem blbla 25', 'VALID', 3, '2018-05-14 14:08:49'),
(33, 'lorem blbla 26', 'VALID', 3, '2018-05-14 14:08:50'),
(34, 'lorem blbla 27', 'VALID', 3, '2018-05-14 14:08:50'),
(35, 'lorem blbla 28', 'VALID', 3, '2018-05-14 14:08:50'),
(36, 'lorem blbla 29', 'VALID', 3, '2018-05-14 14:08:50'),
(37, 'lorem blbla 30', 'VALID', 3, '2018-05-14 14:08:50'),
(38, 'lorem blbla 31', 'VALID', 3, '2018-05-14 14:08:50'),
(39, 'lorem blbla 32', 'VALID', 3, '2018-05-14 14:08:50'),
(40, 'lorem blbla 33', 'VALID', 3, '2018-05-14 14:08:50'),
(41, 'lorem blbla 34', 'VALID', 3, '2018-05-14 14:08:50'),
(42, 'lorem blbla 35', 'VALID', 3, '2018-05-14 14:08:50'),
(43, 'lorem blbla 36', 'VALID', 3, '2018-05-14 14:08:50'),
(44, 'lorem blbla 37', 'VALID', 3, '2018-05-14 14:08:50'),
(45, 'lorem blbla 38', 'VALID', 3, '2018-05-14 14:08:50'),
(46, 'lorem blbla 39', 'VALID', 3, '2018-05-14 14:08:50'),
(47, 'lorem blbla 40', 'VALID', 3, '2018-05-14 14:08:50'),
(48, 'lorem blbla 41', 'VALID', 3, '2018-05-14 14:08:50'),
(49, 'lorem blbla 42', 'VALID', 3, '2018-05-14 14:08:50'),
(50, 'lorem blbla 43', 'VALID', 3, '2018-05-14 14:08:50'),
(51, 'lorem blbla 44', 'VALID', 3, '2018-05-14 14:08:50'),
(52, 'lorem blbla 45', 'VALID', 3, '2018-05-14 14:08:50'),
(53, 'lorem blbla 46', 'VALID', 3, '2018-05-14 14:08:50'),
(54, 'lorem blbla 47', 'VALID', 3, '2018-05-14 14:08:50'),
(55, 'lorem blbla 48', 'VALID', 3, '2018-05-14 14:08:50'),
(56, 'lorem blbla 49', 'VALID', 3, '2018-05-14 14:08:50'),
(57, 'lorem blbla 50', 'VALID', 3, '2018-05-14 14:08:50'),
(58, 'lorem blbla 51', 'VALID', 3, '2018-05-14 14:08:50'),
(59, 'lorem blbla 52', 'VALID', 3, '2018-05-14 14:08:50'),
(60, 'lorem blbla 53', 'VALID', 3, '2018-05-14 14:08:50'),
(61, 'lorem blbla 54', 'VALID', 3, '2018-05-14 14:08:50'),
(62, 'lorem blbla 55', 'VALID', 3, '2018-05-14 14:08:51'),
(63, 'lorem blbla 56', 'VALID', 3, '2018-05-14 14:08:51'),
(64, 'lorem blbla 57', 'VALID', 3, '2018-05-14 14:08:51'),
(65, 'lorem blbla 58', 'VALID', 3, '2018-05-14 14:08:51'),
(66, 'lorem blbla 59', 'VALID', 3, '2018-05-14 14:08:51'),
(67, 'lorem blbla 60', 'VALID', 3, '2018-05-14 14:08:51'),
(68, 'lorem blbla 61', 'VALID', 3, '2018-05-14 14:08:51'),
(69, 'lorem blbla 62', 'VALID', 3, '2018-05-14 14:08:51'),
(70, 'lorem blbla 63', 'VALID', 3, '2018-05-14 14:08:51'),
(71, 'lorem blbla 64', 'VALID', 3, '2018-05-14 14:08:51'),
(72, 'lorem blbla 65', 'VALID', 3, '2018-05-14 14:08:51'),
(73, 'lorem blbla 66', 'VALID', 3, '2018-05-14 14:08:51'),
(74, 'lorem blbla 67', 'VALID', 3, '2018-05-14 14:08:51'),
(75, 'lorem blbla 68', 'VALID', 3, '2018-05-14 14:08:51'),
(76, 'lorem blbla 69', 'VALID', 3, '2018-05-14 14:08:51'),
(77, 'lorem blbla 70', 'VALID', 3, '2018-05-14 14:08:51'),
(78, 'lorem blbla 71', 'VALID', 3, '2018-05-14 14:08:51'),
(79, 'lorem blbla 72', 'VALID', 3, '2018-05-14 14:08:51'),
(80, 'lorem blbla 73', 'VALID', 3, '2018-05-14 14:08:51'),
(81, 'lorem blbla 74', 'VALID', 3, '2018-05-14 14:08:51'),
(82, 'lorem blbla 75', 'VALID', 3, '2018-05-14 14:08:51'),
(83, 'lorem blbla 76', 'VALID', 3, '2018-05-14 14:08:51'),
(84, 'lorem blbla 77', 'VALID', 3, '2018-05-14 14:08:51'),
(85, 'lorem blbla 78', 'VALID', 3, '2018-05-14 14:08:51'),
(86, 'lorem blbla 79', 'VALID', 3, '2018-05-14 14:08:51'),
(87, 'lorem blbla 80', 'VALID', 3, '2018-05-14 14:08:51'),
(88, 'lorem blbla 81', 'VALID', 3, '2018-05-14 14:08:51'),
(89, 'lorem blbla 82', 'VALID', 3, '2018-05-14 14:08:51'),
(90, 'lorem blbla 83', 'VALID', 3, '2018-05-14 14:08:51'),
(91, 'lorem blbla 84', 'VALID', 3, '2018-05-14 14:08:51'),
(92, 'lorem blbla 85', 'VALID', 3, '2018-05-14 14:08:52'),
(93, 'lorem blbla 86', 'VALID', 3, '2018-05-14 14:08:52'),
(94, 'lorem blbla 87', 'VALID', 3, '2018-05-14 14:08:52'),
(95, 'lorem blbla 88', 'VALID', 3, '2018-05-14 14:08:52'),
(96, 'lorem blbla 89', 'VALID', 3, '2018-05-14 14:08:52'),
(97, 'lorem blbla 90', 'VALID', 3, '2018-05-14 14:08:52'),
(98, 'lorem blbla 91', 'VALID', 3, '2018-05-14 14:08:52'),
(99, 'lorem blbla 92', 'VALID', 3, '2018-05-14 14:08:52'),
(100, 'lorem blbla 93', 'VALID', 3, '2018-05-14 14:08:52'),
(101, 'lorem blbla 94', 'VALID', 3, '2018-05-14 14:08:52'),
(102, 'lorem blbla 95', 'VALID', 3, '2018-05-14 14:08:52'),
(103, 'lorem blbla 96', 'VALID', 3, '2018-05-14 14:08:52'),
(104, 'lorem blbla 97', 'VALID', 3, '2018-05-14 14:08:52'),
(105, 'lorem blbla 98', 'VALID', 3, '2018-05-14 14:08:52'),
(106, 'lorem blbla 99', 'VALID', 3, '2018-05-14 14:08:52');

-- --------------------------------------------------------

--
-- Déchargement des données de la table `pack`
--

INSERT INTO `pack` (`id`, `title`, `description`, `price`) VALUES
(11, 'PERMIS A', '40 heures de code\n20 heures de conduite\n8 moniteurs Ã  l\'Ã©coute\n1 diplÃ´me rÃ©ussi', '1000â‚¬'),
(12, 'PERMIS B', '40 heures de code\n20 heures de conduite\n8 moniteurs Ã  l\'Ã©coute\n1 diplome rÃ©ussi', '1500â‚¬'),
(13, 'PERMIS A (30H)', '30 heures de code\n30 heures de conduite\n8 moniteurs Ã Â  l\'Ã©coute\n1 diplÃ´me rÃ©ussi', '1300â‚¬'),
(22, 'mm', '70 heures\n30 mdmdmd\ndldldlm', '1000â‚¬'),
(23, 'PERMIS H', '40 heures de code\n20 heures de conduite\n8 moniteurs Ã  l\'Ã©coute\n1 diplome rÃ©ussi', '900â‚¬');

--
-- Déchargement des données de la table `permission`
--

INSERT INTO `permission` (`id`, `title`) VALUES
('dashboardRight', 'Visualiser le dashboard'),
('depositTimeSlotRight', 'Déposer un horaire'),
('feedbackRight', 'Valider un commentaire'),
('myFeedbackRight', 'Gérer ses commentaires'),
('myPlanningRight', 'Accéder à son planning'),
('myProgessionRight', 'Voir ses progressions'),
('pagesRight', 'Gérer les pages'),
('planningRight', 'Accéder au planning d’un élève'),
('progressionsRight', 'Gérer les progressions d’un élève'),
('rolesRight', 'Gérer un rôle'),
('settingsRight', 'Gérer les réglages'),
('statisticsRight', 'Voir les statistiques'),
('themesRight', 'Gérer les thèmes'),
('timeslotRight', 'Gérer les horaires'),
('usersRight', 'Gérer les utilisateurs');



--
-- Déchargement des données de la table `role_permission`
--

INSERT INTO `role_permission` (`id`, `id_role`, `id_permission`) VALUES
(155, 4, 'myFeedbackRight'),
(156, 4, 'myPlanningRight'),
(157, 4, 'myProgessionRight'),
(225, 1, 'dashboardRight'),
(226, 1, 'depositTimeSlotRight'),
(227, 1, 'feedbackRight'),
(228, 1, 'myFeedbackRight'),
(229, 1, 'myPlanningRight'),
(230, 1, 'myProgessionRight'),
(231, 1, 'pagesRight'),
(232, 1, 'planningRight'),
(233, 1, 'progressionsRight'),
(234, 1, 'rolesRight'),
(235, 1, 'settingsRight'),
(236, 1, 'statisticsRight'),
(237, 1, 'themesRight'),
(238, 1, 'timeslotRight'),
(239, 1, 'usersRight');

--
-- Déchargement des données de la table `theme`
--

INSERT INTO `theme` (`id`, `name`, `description`, `choosen`, `image`) VALUES
(1, 'wild-watermelon', 'Un thÃ¨me glamour et soft', 1, 'wild-watermelon.png'),
(2, 'blue-sky', 'Un thÃ¨me au couleur du ciel donnant vie Ã  votre contenu ! ', 0, 'blue-sky.png');

--
-- Déchargement des données de la table `ability`
--

INSERT INTO `ability` (`id`, `controller`, `method`, `id_permission`) VALUES
(10, 'Page', 'create', 'pagesRight'),
(11, 'Page', 'add', 'pagesRight'),
(12, 'Page', 'delete', 'pagesRight'),
(13, 'Page', 'index', 'pagesRight'),
(14, 'Page', 'show', 'pagesRight'),
(15, 'Page', 'update', 'pagesRight'),
(16, 'Statistic', 'index', 'statisticsRight'),
(17, 'Statistic', 'chart', 'statisticsRight'),
(18, 'Setting', 'index', 'settingsRight'),
(19, 'Setting', 'update', 'settingsRight'),
(20, 'Dashboard', 'index', 'dashboardRight'),
(21, 'User', 'index', 'usersRight'),
(22, 'User', 'show', 'usersRight'),
(23, 'User', 'create', 'usersRight'),
(24, 'User', 'add', 'usersRight'),
(25, 'User', 'delete', 'usersRight'),
(26, 'User', 'update', 'usersRight'),
(27, 'User', 'search', 'usersRight'),
(28, 'Role', 'index', 'rolesRight'),
(29, 'Role', 'show', 'rolesRight'),
(30, 'Role', 'add', 'rolesRight'),
(31, 'Role', 'create', 'rolesRight'),
(32, 'Role', 'delete', 'rolesRight'),
(33, 'Role', 'update', 'rolesRight'),
(34, 'Planning', 'myPlanning', 'myPlanningRight'),
(35, 'Planning', 'show', 'planningRight'),
(36, 'Planning', 'index', 'planningRight'),
(37, 'Planning', 'create', 'planningRight'),
(38, 'TimeSlot', 'index', 'timeslotRight'),
(39, 'TimeSlot', 'show', 'timeslotRight'),
(40, 'TimeSlot', 'add', 'timeslotRight'),
(41, 'TimeSlot', 'create', 'timeslotRight'),
(42, 'TimeSlot', 'delete', 'timeslotRight'),
(43, 'TimeSlot', 'update', 'timeslotRight'),
(44, 'Progress', 'index', 'progressionsRight'),
(45, 'Progress', 'show', 'progressionsRight'),
(46, 'Progress', 'add', 'progressionsRight'),
(47, 'Progress', 'create', 'progressionsRight'),
(48, 'Progress', 'delete', 'progressionsRight'),
(49, 'Progress', 'update', 'progressionsRight'),
(50, 'Progress', 'myprogress', 'progressionsRight'),
(51, 'Feedback', 'index', 'feedbackRight'),
(52, 'Feedback', 'add', 'feedbackRight'),
(53, 'Feedback', 'create', 'feedbackRight'),
(54, 'Feedback', 'delete', 'feedbackRight'),
(55, 'Feedback', 'update', 'feedbackRight'),
(56, 'Feedback', 'myfeedback', 'myFeedbackRight'),
(57, 'Theme', 'index', 'themesRight'),
(58, 'Theme', 'show', 'themesRight'),
(59, 'Theme', 'update', 'themesRight'),
(60, 'Planning', 'navigation', 'planningRight'),
(61, 'Planning', 'addtimeslot', 'planningRight'),
(62, 'Planning', 'removetimeslot', 'planningRight'),
(63, 'Planning', 'showAdmin', 'timeslotRight'),
(64, 'Progress', 'myprogress', 'myProgessionRight'),
(65, 'Feedback', 'show', 'feedbackRight'),
(66, 'Progress', 'list', 'progressionsRight'),
(67, 'Pack', 'delete', 'pagesRight'),
(68, 'Pack', 'show', 'pagesRight'),
(69, 'Pack', 'update', 'pagesRight');




INSERT INTO `setting` (`id`, `name`, `value`) VALUES
(1, 'sitename', 'Mon site'),
(2, 'mailcompany', 'contact@newdriving.fr'),
(3, 'phone', '0132323232'),
(4, 'instagram', 'https://www.instagram.com/esgiparis/?hl=fr'),
(5, 'facebook', 'https://fr-fr.facebook.com/ESGIParis/'),
(6, 'twitter', 'https://twitter.com/esgi?lang=fr');
