<?php

require "core/Router.php";



//Pages
Router::get('/pages','Page@index'); // Liste des pages
Router::get('/pages/{idPage:n}','Page@show'); //Affichage d'une page
Router::get('/pages/add','Page@add'); //Affichage d'une page
Router::post('/pages/add','Page@create'); //Ajout d'une page dans le site vitrine
Router::delete('/pages/delete/{idPage:n}','Page@delete'); //Suppression d'une page
Router::put('/pages/{idPage:n}','Page@update');//Modification d'une page

//Utilisateurs
Router::post('/login','User@login'); //Connexion d'un utilisateur
Router::get('/resetpwd','User@resetpwd'); //Connexion d'un utilisateur
Router::get('/logout','User@logout'); //Déconnexion d'un utilisateur
Router::post('/lostpwd','User@lostpwd');//Mot de passe oublié
Router::get('/profile','User@profile'); //Profil de l'utilisateur connecté
Router::put('/users/{idUser:n}','User@update'); // Modification d'un utilisateur
Router::get('/users','User@index'); // Liste des utilisateurs
Router::delete('/users/delete/{id:n}','User@delete'); // Suppression d'un utilisateur
Router::get('/users/{id:n}','User@show'); // Affichage d'un utilisateur
Router::get('/users/add','User@add'); //Formulaire de création d'un utilisateur
Router::post('/users/add','User@create'); //Création d'un utilisateur
Router::post('/users/search','User@search'); //Recherche d'un utilisateur

//Rôles
Router::get('/roles','Role@index'); // Affichage des rôles
Router::get('/roles/{idRole:n}','Role@show'); // Affichage d'un rôle
Router::get('/roles/add','Role@add'); // Formulaire de création
Router::post('/roles/add','Role@create'); //Création d'un rôle
Router::delete('/roles/delete/{idRole:n}','Role@delete'); //Suppression d'un rôle
Router::put('/roles/{idRole:n}','Role@update'); // Modification d'un rôle

//Planning
Router::get('/planning/{idUser:n}','Planning@show'); //Affichage du planning d'une personne
Router::get('/planning','Planning@index'); //Affichage du planning
Router::get('/planning/myplanning','Planning@myPlanning'); // Affichage du planning côté élève
Router::post('/planning/add','Planning@create'); //Ajout d'une séance dans le planning
Router::post('/planning/navigation','Planning@navigation'); //Navigation de semaine en semaine
Router::post('/planning/addtimeslot','Planning@addtimeslot'); //Ajoute un timeslot à partir du planning
Router::post('/planning/removetimeslot','Planning@removetimeslot'); //Supprimer un timeslot à partir du planning

//Horaires
Router::get('/timeslot','TimeSlot@index'); //Affichage des horaires
Router::get('/timeslot/{idTimeSlot:n}','TimeSlot@show'); //Affichage des horaires d’un utilisateur
Router::get('/timeslotadmin','TimeSlot@showAdmin'); //Affichage des horaires des utilisateurs
Router::get('/timeslot/add','TimeSlot@add'); //Formulaire de création d’un horaire
Router::post('/timeslot/add','TimeSlot@create'); //Création d’un horaire
Router::delete('/timeslot/delete/{idTimeSlot:n}','TimeSlot@delete'); //Suppression d’une horaire
Router::put('/timeslot/{idTimeSlot:n}','TimeSlot@update'); //Modifier d’une horaire

//Progressions
Router::get('/progress','Progress@index'); //Affichage de la liste des élèves pour accéder à leurs progressions
Router::get('/progress/{idUser:n}','Progress@list'); //Affichage des progressions d’un utilisateur (élèves)
Router::get('/progress/{idUser:n}/add','Progress@add'); //Formulaire de création d’une progression
Router::get('/progress/{idUser:n}/update/{idProgress:n}','Progress@show'); //Formulaire de modification d’une progression
Router::post('/progress/{idUser:n}/add','Progress@create'); //Ajouter une progression
Router::get('/myprogress','Progress@myprogress'); //Affichage de ses progressions
Router::delete('/progress/delete/{idProgress:n}','Progress@delete'); //Suppression d’une progression
Router::put('/progress/{idUser:n}/update/{idProgress:n}','Progress@update'); //Modification d' une progression

//Commentaires
Router::get('/feedback','Feedback@index'); //Afficher les commentaires
Router::get('/feedback/add','Feedback@add'); //Formulaire de création d’un commentaire
Router::get('/feedback/{idFeedback:n}','Feedback@show'); //Affichage d’un commentaire
Router::post('/feedback/add','Feedback@create'); //Ajouter un commentaire
Router::put('/feedback/{idFeedback:n}','Feedback@update'); //Mis à jour d'un commentaire
Router::delete('/feedback/delete/{idFeedback:n}','Feedback@delete'); //Suppression d’un commentaire
Router::get('/myfeedback','Feedback@myfeedback'); //Afficher ses commentaires

//Thèmes
Router::get('/themes','Theme@index'); // Affiche les thèmes
Router::get('/themes/{idTheme:n}','Theme@show'); // Affichage d’un thème
Router::put('/themes/{idTheme:n}','Theme@update'); // Modification d’un thème

//Règlages
Router::get('/settings','Setting@index'); //Formulaire des réglages
Router::put('/settings','setting@update'); //Modifier l’en tête du site

//Statistiques
Router::get('/statistics','Statistic@index'); //Affichage des statistiques
Router::get('/statistics/chart','Statistic@chart'); //Affichage des charts pour les statistiques

//Pack
Router::post('/packs/add','Pack@create');
Router::delete('/packs/delete/{idPack:n}','Pack@delete');
Router::get('/packs/{idPack:n}','Pack@show');
Router::put('/packs/{idPack:n}','Pack@update');



//Routes supplémentaires
/** FRONT OFFICE **/
Router::get('/','Index@index'); // Page d'accueil
Router::get('/comment','Index@comment'); // Page des commentaires
Router::post('/sendmessage','Index@sendMessage'); // Page des commentaires
Router::get('/pack','Index@pack'); // Page des forfaits
Router::get('/about','Index@description'); // Page de la description
Router::get('/error','Index@notFound');
Router::get('/dashboard','Dashboard@index'); //Affichage du dashboard
Router::get('/{pageName:an}','Index@getFrontPage'); // Page créer
Router::get('/comments/pages/{pageNumber:n}','Index@comments'); // Commentaires de la page commentaire